<?php

header("access-control-allow-origin: *");

header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

header("Access-Control-Allow-Headers: Content-Type");

defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    public $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model', '', true);
        $this->data["site_settings"] = $this->admin_model->get_cms('site_settings', '1');
    }

	// function front_view($design = null) 
	// {
	// 	$this->load->view("includes/header", $this->data);
	// 	$this->load->view($design);
	// 	$this->load->view("includes/footer", $this->data);
	// }




}
