<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">About us</h4>
        </div>
        <!--  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
           <a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>
           <ol class="breadcrumb">
             <li><a href="#">Dashboard</a></li>
             <li><a href="#">Forms</a></li>
             <li class="active">Form Layout</li>
           </ol>
         </div> -->
        <!-- /.col-lg-12 -->
    </div>

    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"> About us </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form action="<?php echo base_url(); ?>cmoon/about_us" data-toggle="validator" method="POST" enctype='multipart/form-data' class="form-horizontal" autocomplete="off" >


                            <div class="form-body">
                                <h3 class="box-title"></h3>
                                <hr class="m-t-0 m-b-40">
                                <div class="row">
                                    <!--<div class="col-md-12">-->
                                    <!--  <div class="form-group">-->
                                    <!--    <label class="control-label col-md-3">Title</label>-->
                                    <!--    <div class="col-md-9">-->
                                    <!--      <input type="text" class="form-control" name="title" placeholder="Title" data-error="Please Enter Title" value="<?php echo $row->title; ?>" required>-->
                                    <!--        <div class="help-block with-errors"></div>-->
                                          <!-- <span class="help-block"> This is inline help </span>  -->
                                    <!--    </div>-->
                                    <!--  </div>-->
                                    <!--</div>-->


                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-2">
                                                <label class="control-label">Description </label>
                                            </div>
                                            <div class="col-md-10">
                                                <textarea name="description" class="ckeditor form-control"><?php echo $row->description; ?></textarea>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--    <div class="row">-->
                                    <!--  <div class="form-group">-->
                                    <!--    <div class="col-md-2">-->
                                    <!--      <label class="control-label">Description1 </label>-->
                                    <!--    </div>-->
                                    <!--      <div class="col-md-10">-->
                                    <!--        <textarea name="description1" class="ckeditor form-control"><?php echo $row->description1; ?></textarea>-->
                                    <!--          <div class="help-block with-errors"></div>-->
                                    <!--      </div>-->
                                    <!--    </div>-->
                                    <!--</div>-->


                                    <!-- <div class="row">-->
                                    <!--  <div class="form-group">-->
                                    <!--    <div class="col-md-2">-->
                                    <!--      <label class="control-label">Quality policy description1 </label>-->
                                    <!--    </div>-->
                                    <!--      <div class="col-md-10">-->
                                    <!--        <textarea name="q_policy1" class="ckeditor form-control"><?php echo $row->q_policy1; ?></textarea>-->
                                    <!--          <div class="help-block with-errors"></div>-->
                                    <!--      </div>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                                    <!--   <div class="row">-->
                                    <!--  <div class="form-group">-->
                                    <!--    <div class="col-md-2">-->
                                    <!--      <label class="control-label">Quality policy description2 </label>-->
                                    <!--    </div>-->
                                    <!--      <div class="col-md-10">-->
                                    <!--        <textarea name="q_policy2" class="ckeditor form-control"><?php echo $row->q_policy2; ?></textarea>-->
                                    <!--          <div class="help-block with-errors"></div>-->
                                    <!--      </div>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                                    <!-- <div class="row">-->
                                    <!--  <div class="form-group">-->
                                    <!--    <div class="col-md-2">-->
                                    <!--      <label class="control-label">Quality policy description3 </label>-->
                                    <!--    </div>-->
                                    <!--      <div class="col-md-10">-->
                                    <!--        <textarea name="q_policy3" class="ckeditor form-control"><?php echo $row->q_policy3; ?></textarea>-->
                                    <!--          <div class="help-block with-errors"></div>-->
                                    <!--      </div>-->
                                    <!--    </div>-->
                                    <!--</div>-->









                                </div>



                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn btn-success">Submit</button>
                                                    <!-- <button type="button" class="btn btn-default">Cancel</button> -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"> </div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--./row-->


</div>