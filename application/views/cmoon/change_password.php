    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Change Password</h4>
        </div>
       <!--  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Form Layout</li>
          </ol>
        </div> -->
        <!-- /.col-lg-12 -->
      </div>

      <!--.row-->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-info">
            <div class="panel-heading"> Change Password </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
              <div class="panel-body">
          <form action="<?php echo base_url(); ?>cmoon/Change_password" data-toggle="validator" method="POST" enctype='multipart/form-data' class="form-horizontal" autocomplete="off" >


              <div class="form-body">
                <h3 class="box-title"></h3>
                <hr class="m-t-0 m-b-40">


                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3">Old Password</label>
                      <div class="col-md-9">
    <input type="text" class="form-control" name="old_password" placeholder="Old Password" data-error="Please enter the link" required>
                          <div class="help-block with-errors"></div>
                        <!-- <span class="help-block"> This is inline help </span>  -->
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3">New Password</label>
                      <div class="col-md-9">
        <input type="text" class="form-control" name="new_password" data-toggle="validator" data-minlength="8" class="form-control" id="inputPassword" placeholder="New Password" data-error="Please enter the link" required>
                          <div class="help-block with-errors"></div>
                          <span class="help-block">Password Should be Minimum of 8 characters</span>
                        <!-- <span class="help-block"> Phone number should be between 10-14 digits only </span>  -->
                      </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> Re-Type Password </label>
                      <div class="col-md-9">
    <input type="text" class="form-control" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Re-Type Password" data-error="Please enter the link" required>
                          <div class="help-block with-errors"></div>
                        <!-- <span class="help-block"> This is inline help </span>  -->
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <!-- <button type="button" class="btn btn-default">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"> </div>
                </div>
              </div>
            </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--./row-->
    </div>