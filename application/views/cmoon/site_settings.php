    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Contact Settings</h4>
        </div>
       <!--  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Form Layout</li>
          </ol>
        </div> -->
        <!-- /.col-lg-12 -->
      </div>

      <!--.row-->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-info">
            <div class="panel-heading"> Site Settings </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
              <div class="panel-body">
          <form action="<?php echo base_url(); ?>cmoon/site_settings" data-toggle="validator" method="POST" enctype='multipart/form-data' class="form-horizontal" autocomplete="off" >


              <div class="form-body">
                <h3 class="box-title"></h3>
                <hr class="m-t-0 m-b-40">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3">Site Name</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="site_name" placeholder="Site Name" data-error="Please Enter Site Name" value="<?php echo  $row->site_name; ?>" required>
                          <div class="help-block with-errors"></div>
                        <!-- <span class="help-block"> This is inline help </span>  -->
                      </div>
                    </div>
                  </div>
                  </div>
                 
                  <!--/span-->
                  <!-- <div class="col-md-6">
                    <div class="form-group has-error">
                      <label class="control-label col-md-3">Site Phone Number</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="12n">
                        <span class="help-block"> This field has error. </span> 
                      </div>
                    </div>
                  </div> -->
                  <!--/span-->
                </div>

                 <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> Site Phone Number</label>
                      <div class="col-md-9">
        <input type="tel" class="form-control" name="phone_number" placeholder="Phone Number" value="<?php echo  $row->phone_number; ?>" data-error="Please Enter Phone Number" data-minlength="10" data-maxlength="14" required>
                          <div class="help-block with-errors"></div>
                        <span class="help-block"> Phone number should be between 10-14 digits only </span> 
                      </div>
                    </div>
                  </div>
                </div>
                  
                
        <!--         <div class="row">-->
        <!--          <div class="col-md-6">-->
        <!--            <div class="form-group">-->
        <!--              <label class="control-label col-md-3"> Site Phone Number1</label>-->
        <!--              <div class="col-md-9">-->
        <!--<input type="tel" class="form-control" name="phone1" placeholder="Phone Number" value="<?php echo  $row->phone1; ?>" data-error="Please Enter Phone Number" data-minlength="10" data-maxlength="14" required>-->
        <!--                  <div class="help-block with-errors"></div>-->
        <!--                <span class="help-block"> Phone number should be between 10-14 digits only </span> -->
        <!--              </div>-->
        <!--            </div>-->
        <!--          </div>-->
        <!--        </div>-->
 

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> Site Email Address</label>
                      <div class="col-md-9">
                        <input type="email" class="form-control" name="contact_email" value="<?php echo  $row->contact_email; ?>" placeholder="Email Address" data-error="Please Enter Valid Email Address" required>
                          <div class="help-block with-errors"></div>
                        <!-- <span class="help-block"> This is inline help </span>  -->
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> From Email Address</label>
                      <div class="col-md-9">
                        <input type="email" class="form-control" name="from_email" value="<?php echo  $row->from_email; ?>" placeholder="Email Address" data-error="Please Enter Valid Email Address" required>
                          <div class="help-block with-errors"></div>
                        <!-- <span class="help-block"> This is inline help </span>  -->
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> Forgot Password Email Address</label>
                      <div class="col-md-9">
                        <input type="email" class="form-control" name="frogot_password_email" value="<?php echo  $row->frogot_password_email; ?>" placeholder="Email Address" data-error="Please Enter Valid Email Address" required>
                          <div class="help-block with-errors"></div>
                        <!-- <span class="help-block"> This is inline help </span>  -->
                      </div>
                    </div>
                  </div>
                </div>
                
                 

                <h3 class="box-title">Site logo & Fav-Icon</h3>
                <hr class="m-t-0 m-b-40">



            <div class="row">
              <div class="form-group">
                <div class="col-md-2"></div>
                  <div class="col-md-6">
                    <div class="image-popups">
                      <a href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->site_logo; ?>" style="width:144px;height:120px;" data-effect="mfp-3d-unfold"><img style="width: 300PX; background: black;" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->site_logo; ?>" class="img-responsive" /></a>
                    </div>
                  </div>
                </div>
              </div>


                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> Logo</label>
                      <div class="col-md-9">
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                          <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                          <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                          <input type="file" name="site_logo">
                          </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                          <div class="help-block with-errors"></div>
                        <span class="help-block">Please Upload the image of ( 282 X 83 ) pixels only </span>
                          <span class="help-block"> Note : Only jpg,jpeg,png and gif formats are allowed </span>
                      </div>
                    </div>
                  </div>
                </div>


                <!--<div class="row">-->
                <!--  <div class="col-md-6">-->
                <!--    <div class="form-group">-->
                <!--      <label class="control-label col-md-3"> Logo alt</label>-->
                <!--      <div class="col-md-9">-->
                <!--        <input type="test" class="form-control" name="site_logo_alt" value="<?php echo  $row->site_logo_alt; ?>" style="width:144px;height:120px;"  placeholder="Logo alt" data-error="Field should not be empty" required>-->
                <!--          <div class="help-block with-errors"></div>-->
                <!--      </div>-->
                <!--    </div>-->
                <!--  </div>-->
                <!--</div>-->


            <div class="row">
              <div class="form-group">
                <div class="col-md-2"></div>
                  <div class="col-md-6">
                    <div class="image-popups">
                      <a href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->site_favicon; ?>"style="width:144px;height:120px;" data-effect="mfp-3d-unfold"><img style="width: 150PX;" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->site_favicon; ?>" class="img-responsive" /></a>
                    </div>
                  </div>
                </div>
              </div>


                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> Fav-Icon</label>
                      <div class="col-md-9">
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                          <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                          <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                          <input type="file" name="site_favicon">
                          </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                          <div class="help-block with-errors"></div>
                        <span class="help-block">Please Upload the image of ( 282 X 83 ) pixels only to maintain the desing</span>
                          <span class="help-block"> Note : Only jpg,jpeg,png and gif formats are allowed </span>
                      </div>
                    </div>
                  </div>
                </div>
                
              <!--  <div class="row">-->
              <!--<div class="form-group">-->
              <!--  <div class="col-md-2"></div>-->
              <!--    <div class="col-md-6">-->
              <!--      <div class="image-popups">-->
              <!--        <a href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->footer_logo; ?>"style="width:144px;height:120px;" data-effect="mfp-3d-unfold"><img style="width: 150PX;" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->footer_logo; ?>" class="img-responsive" /></a>-->
              <!--      </div>-->
              <!--    </div>-->
              <!--  </div>-->
              <!--</div>-->


                <!--<div class="row">-->
                <!--  <div class="col-md-6">-->
                <!--    <div class="form-group">-->
                <!--      <label class="control-label col-md-3"> Header Image</label>-->
                <!--      <div class="col-md-9">-->
                <!--        <div class="fileinput fileinput-new input-group" data-provides="fileinput">-->
                <!--          <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>-->
                <!--          <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>-->
                <!--          <input type="file" name="footer_logo">-->
                <!--          </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>-->
                <!--          <div class="help-block with-errors"></div>-->
                <!--        <span class="help-block">Please Upload the image of ( 865 X 106 ) pixels only to maintain the desing</span>-->
                <!--          <span class="help-block"> Note : Only jpg,jpeg,png and gif formats are allowed </span>-->
                <!--      </div>-->
                <!--    </div>-->
                <!--  </div>-->
                <!--</div>-->

                 
          
                <!-- <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3">Email Address 2</label>
                      <div class="col-md-9">
                        <input type="email" class="form-control" name="email_2" value="<?php echo  $row->email_2; ?>" data-error="Please Enter valid Email Address" required>
                          <div class="help-block with-errors"></div>
                      </div>
                    </div>
                  </div>
                </div> -->

              </div>



              <div class="form-actions">
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <!-- <button type="button" class="btn btn-default">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"> </div>
                </div>
              </div>
            </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--./row-->


    </div>