<div class="container-fluid">

    <div class="row bg-title">

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">

            <h4 class="page-title">Add or Edit Quality tables simhadri ductile properties</h4>

        </div>



    </div>



    <?php
    if ($this->uri->segment(4) != '') {
        foreach ($values as $row)
            ;
    }
    ?>

    <!--.row-->

    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-info">

                <div class="panel-heading"> Add or Edit Quality tables simhadri ductile properties<a href="<?php echo base_url(); ?>cmoon/quality_tables_simhadri_ductile_properties/<?php echo $this->uri->segment(3) ?>?&page=<?php echo $this->uri->segment(4); ?>" class="btn  btn waves-effect waves-light btn-default" style="float:right; color: black;">Back</a></div>

                <div class="panel-wrapper collapse in" aria-expanded="true">

                    <div class="panel-body">



                        <form action="<?php echo base_url(); ?>cmoon/quality_tables_simhadri_ductile_properties_adding/<?php echo $this->uri->segment(3); ?>/<?php echo $row->id; ?>" id="form_data" method="POST" enctype='multipart/form-data' class="form-horizontal" autocomplete="off" >



                            <input type="hidden" name="products_id" value="<?php echo $this->uri->segment(3); ?>">



                            <div class="form-body">

                                <h3 class="box-title"></h3>

                                <hr class="m-t-0 m-b-40">

                                <label for="mm">Select:</label>
                                <?php
                                $mm = $this->db->get('quality_tables_simhadri_ductile_properties_size')->result();
                                ?>
                                <select name="heading" id="heading">
                                    <?php foreach ($mm as $mm1) { ?>
                                        <option value="<?= $mm1->heading ?>"><?= $mm1->heading ?></option>
                                    <?php } ?>

                                </select>


                                <div class="row">

                                    <div class="form-group">

                                        <div class="col-md-2">

                                            <label class="control-label">Yield Stress </label>

                                        </div>

                                        <div class="col-md-6">

                                            <input type="text" class="form-control" name="yield_stress" placeholder="" value="<?php echo $row->yield_stress; ?>" >

                                            <div class="help-block with-errors"></div>

                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="form-group">

                                        <div class="col-md-2">

                                            <label class="control-label">Ultimate Tensile </label>

                                        </div>

                                        <div class="col-md-6">

                                            <input type="text" class="form-control" name="ultimate_tensile" placeholder="" value="<?php echo $row->ultimate_tensile; ?>" >

                                            <div class="help-block with-errors"></div>

                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="form-group">

                                        <div class="col-md-2">

                                            <label class="control-label">%Elongation </label>

                                        </div>

                                        <div class="col-md-6">

                                            <input type="text" class="form-control" name="elongation" placeholder="" value="<?php echo $row->elongation; ?>" >

                                            <div class="help-block with-errors"></div>

                                        </div>

                                    </div>

                                </div>




                                <div class="form-actions">

                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="row">

                                                <div class="col-md-offset-4 col-md-9">

                                                    <button type="submit" class="btn btn-success">Submit</button>

                                                    <!-- <button type="button" class="btn btn-default">Cancel</button> -->

                                                </div>

                                            </div>

                                        </div>

                                        <div class="col-md-6"> </div>

                                    </div>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>







<script type="text/javascript">

    $(function () {

// Setup form validation on the #register-form element

        $("#form_data").validate({

            // Specify the validation rules

            ignore: [],

            debug: false,

            rules: {

                heading: {

                    required: true

                },

                description: {

                    required: true

                },

                offer_zone_image_alt: {

                    required: true

                },

                search_page_image: {

                    required: true

                },

                popup_image_alt: {

                    required: true

                }



            },

// Specify the validation error messages

            messages: {

                heading: {

                    required: 'Field shouldnot be empty'

                },

                description: {

                    required: 'Field shouldnot be empty'

                },

                offer_zone_image_alt: {

                    required: 'Field shouldnot be empty'

                },

                search_page_image: {

                    required: 'Field shouldnot be empty'

                },

                popup_image_alt: {

                    required: 'Field shouldnot be empty'

                }

            },

        });

    });



    function Checkfiles() {

        var fup = document.getElementById('file_type');

        var fileName = fup.value;

        var ext = fileName.substring(fileName.lastIndexOf('.') + 1);

        if (ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "gif") {

            return true;

        } else {

            alert("Upload pdf Files only");

            document.getElementById('file_type').value = "";

            fup.focus();

            return false;

        }

    }

</script>