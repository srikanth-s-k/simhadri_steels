            <footer class="footer text-center"> <?php echo date('Y'); ?> &copy; <?php echo $site_settings->site_name; ?> Admin. </footer>
        </div><!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>cmoon_assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url(); ?>cmoon_assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>cmoon_assets/js/waves.js"></script>
    <!--weather icon -->
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/skycons/skycons.js"></script>
    <!--Counter js -->
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
    <!--Morris JavaScript -->
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/raphael/raphael-min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/morrisjs/morris.js"></script> -->
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>cmoon_assets/js/custom.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>cmoon_assets/js/dashboard4.js"></script> -->
    <!-- Sparkline chart JavaScript -->
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <!--Style Switcher -->
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Validating links  -->

    <script src="<?php echo base_url(); ?>cmoon_assets/js/validator.js"></script>
    <script src="<?php echo base_url(); ?>cmoon_assets/js/jquery.validate.min.js"></script>
    <!-- For File select and change and remove-->
    <script src="<?php echo base_url(); ?>cmoon_assets/js/jasny-bootstrap.js"></script>
    <!-- desapia tostor -->
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
    <script src="<?php echo base_url(); ?>cmoon_assets/js/toastr.js"></script>
    <!-- Magnific popup JavaScript -->
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
    <!-- data tables -->
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- jQuery peity -->
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/peity/jquery.peity.min.js"></script>
    <script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/peity/jquery.peity.init.js"></script>


<!-- Date Picker Plugin JavaScript -->
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- Date range Plugin JavaScript -->
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



    <!-- Sweet-Alert  -->
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<!-- ckeditor script -->
<script src="<?php echo base_url(); ?>cmoon_assets/ckeditor/ckeditor.js"></script>




<!-- Plugin JavaScript -->
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/moment/moment.js"></script>
<!-- Clock Plugin JavaScript -->
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- Color Picker Plugin JavaScript -->
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
<script src="<?php echo base_url(); ?>cmoon_assets/js/jquery.datetimepicker.full.js"></script>



<!-- end - This is for export functionality only -->
<script>
    $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
        table.order( [ 2, 'desc' ] ).draw();
      }
      else {
        table.order( [ 2, 'asc' ] ).draw();
      }
    });
  });
    });
    $('#example23').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
  </script>


<?php if($this->uri->segment(3) == 'Success' || $this->uri->segment(4) == 'Success' || $this->uri->segment(5) == 'Success'){ ?>

  <script type="text/javascript">
           $.toast({
            heading: 'Success',
            text: 'Details Updated Successfully',
            position: 'bottom-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 5430, 
            stack: 6
          });

  </script>
<?php } ?>


<?php if($this->uri->segment(3) == 'error' || $this->uri->segment(4) == 'error' || $this->uri->segment(5) == 'error'){ ?>

  <script type="text/javascript">
           $.toast({
            heading: 'Eroor',
            text: 'Unable to Updated please tryagain refreshing the page',
            position: 'bottom-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 5430, 
            stack: 6
          });

  </script>
<?php } ?>


<?php if($this->uri->segment(3) == 'image_error'){ ?>

  <script type="text/javascript">
           $.toast({
            heading: 'Image Not uploded',
            text: 'Please upload an image',
            position: 'bottom-right',
            loaderBg:'#ff6849',
            icon: 'warning',
            hideAfter: 5430, 
            stack: 6
          });

  </script>
<?php } ?>


<?php if($this->uri->segment(3) == 'dSuccess' || $this->uri->segment(4) == 'dSuccess' || $this->uri->segment(4) == 'dSuccess'){ ?>

  <script type="text/javascript">
           $.toast({
            heading: 'Success',
            text: 'Data Deleted Successfully',
            position: 'bottom-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 5430, 
            stack: 6
          });

  </script>
<?php } ?>

<?php if($this->uri->segment(3) == 'psw_doesnot_match'){ ?>
  <script type="text/javascript">
           $.toast({
            heading: 'Error',
            text: 'Password doesnot match with the password in the database',
            position: 'bottom-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 5430, 
            stack: 6
          });

  </script>
<?php } ?>


<?php if($this->uri->segment(3) == 'psw_Success'){ ?>

  <script type="text/javascript">
           $.toast({
            heading: 'Success',
            text: 'Password changed successfully',
            position: 'bottom-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 5430, 
            stack: 6
          });

  </script>
<?php } ?>

<script>

      jQuery('#datepicker-inline').datepicker({
        format: 'MM-yyyy',
        autoclose: true
      });
      
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('.datepicker-autoclose').datepicker({
        format: 'dd-M-yyyy',
        autoclose: true,
        todayHighlight: true
      });



  $(function() {

     $('.datepicker_2').datetimepicker({
           timepicker: false,
           format: 'd-M-Y'
       });


    $( ".datepicker" ).datepicker({
        dateFormat: "M-Y"
    });
  } );
  </script>
<!--           $(".tst1").click(function(){
           $.toast({
            heading: 'Welcome to my Elite admin',
            text: 'Use the predefined ones, or specify a custom position object.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'info',
            hideAfter: 3000, 
            stack: 6
          });

     });

      $(".tst2").click(function(){
           $.toast({
            heading: 'Welcome to my Elite admin',
            text: 'Use the predefined ones, or specify a custom position object.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'warning',
            hideAfter: 3500, 
            stack: 6
          });

     });
      $(".tst3").click(function(){
           $.toast({
            heading: 'Welcome to my Elite admin',
            text: 'Use the predefined ones, or specify a custom position object.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
          });

     });

      $(".tst4").click(function(){
           $.toast({
            heading: 'Welcome to my Elite admin',
            text: 'Use the predefined ones, or specify a custom position object.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
            
          });

     }); -->



</body>
</html>