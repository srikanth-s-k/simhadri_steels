<?php
@session_start();
$session_id = $this->session->userdata('logged_in');
if (!isset($session_id['admin_id']) && $session_id['username'] == "") {
    ?>
    <script>
        document.location.href = '<?php echo base_url(); ?>cmoon_login';
    </script>
<?php } ?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>cmoon_images/<?php echo $site_settings->site_favicon; ?>">
        <title><?php echo $site_settings->site_name; ?></title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>cmoon_assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- morris CSS -->
        <link href="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="<?php echo base_url(); ?>cmoon_assets/css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>cmoon_assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>cmoon_assets/css/custom.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="<?php echo base_url(); ?>cmoon_assets/css/colors/blue.css" id="theme" rel="stylesheet">
        <link href="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
        <!-- Popup CSS -->
        <link href="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
        <!--sweet alerts CSS -->
        <link href="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

        <!-- Page plugins css -->
        <link href="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
        <!-- Color picker plugins css -->
        <link href="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
        <!-- Date picker plugins css -->
        <link href="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker plugins css -->
        <link href="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>cmoon_assets/css/jquery.datetimepicker.css" rel="stylesheet">

        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="http://www.w3schools.com/lib/w3data.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- admin validateion  -->

        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>


        <style type="text/css">
            em.invalid {
                color: red;
                font-weight: bold;
            }
        </style>

        <script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o)
                , m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-19175540-9', 'auto');
    ga('send', 'pageview');
        </script>
    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top m-b-0">
                <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                    <div class="top-left-part"><a class="logo" href="<?php echo base_url(); ?>cmoon"><b>
                        <!--<img src="<?php echo base_url(); ?>cmoon_images/<?php echo $site_settings->site_logo; ?>" style="width:100px;height:100px;" alt="<?php echo $site_settings->site_name; ?>" /></b><span class="hidden-xs">-->
                            <!-- <img src="<?php echo base_url(); ?>cmoon_assets/plugins/images/eliteadmin-text.png" alt="home" /> -->
                            </span></a></div>

                    <ul class="nav navbar-top-links navbar-right pull-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><b>Settings </b> <i class="ti-settings"></i><div class="notify"><span class="heartbit"></span><span class="point"></span></div></a>
                            <!-- <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <b class="hidden-xs">Settings</b> </a> -->
                            <ul class="dropdown-menu dropdown-user animated flipInY">
                                <li><a href="<?php echo base_url(); ?>cmoon/change_password"><i class="ti-key"></i> Change Password</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>cmoon/backup_db"><i class="ti-save"></i> Data Base Back Up</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>cmoon/seo_tags"><i class="ti-user"></i> Seo tags</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>cmoon/logs"><i class="ti-user"></i> Logs</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>cmoon/contact_details"><i class="ti-user"></i> Contact details</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>cmoon/social_links"><i class="ti-user"></i> Social Links</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>cmoon/site_settings"><i class="ti-settings"></i> Site Settings</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url(); ?>/cmoon_login/log_out"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                            <!-- /.dropdown-user -->
                        </li>

                    </ul>
                </div>
                <!-- /.navbar-header -->
                <!-- /.navbar-top-links -->
                <!-- /.navbar-static-side -->
            </nav>
            <!-- Left navbar-header -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                    <ul class="nav" id="side-menu">

                        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                            <!-- input-group -->
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn"><button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                                </span> </div>
                            <!-- /input-group -->
                        </li>

                        <li> <a href="<?php echo base_url(); ?>cmoon" class="waves-effect active"><i data-icon="v" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Dashboard</span></a> </li>


<!--                     <li> <a href="<?php echo base_url(); ?>cmoon" class="waves-effect active"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard <span class="fa arrow"></span> </span></a>
                        <!--                        <ul class="nav nav-second-level">-->
                        <!--                            <li> <a href="index.html">Minimalistic</a> </li>-->
                        <!--                            <li> <a href="index2.html">Demographical</a> </li>-->
                        <!--                            <li> <a href="index3.html">Analitical</a> </li>-->
                        <!--                            <li> <a href="index4.html">Simpler</a> </li>-->
                        <!--                        </ul>-->
                        <!--                    </li> -->
                        <li><a href="inbox.html" class="waves-effect active"><i data-icon=")" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Manage CMS Pages <span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="<?php echo base_url(); ?>cmoon/home_banners">Home banners</a></li>
                                <li><a href="<?php echo base_url(); ?>cmoon/about_us">About us </a></li>



                            </ul>
                        </li>

                        <li><a href="inbox.html" class="waves-effect active"><i data-icon=")" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Dealers <span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">

                                <li><a href="<?php echo base_url(); ?>cmoon/dealers_city"> Manage Dealers</a></li>

                            </ul>
                        </li>

                        <li><a href="inbox.html" class="waves-effect active"><i data-icon=")" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Products Title <span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">

                                <li><a href="<?php echo base_url(); ?>cmoon/products_title"> Products Title</a></li>

                            </ul>
                        </li>

                        <li><a href="inbox.html" class="waves-effect active"><i data-icon=")" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Forms <span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">

                                <li><a href="<?php echo base_url(); ?>cmoon/contact_us_form_details"> Contact and popup enquiry form details</a></li>
                                 <!--<li><a href="<?php echo base_url(); ?>cmoon/careers_form_details"> Careers form details</a></li>-->

                            </ul>
                        </li>


                        <li> <a href="<?php echo base_url(); ?>cmoon/manage_uploads" class="waves-effect active"><i data-icon="P" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Manage Uploads</span></a> </li>
                        <!-- <li><a href="login.html" class="waves-effect"><i data-icon="&#xe045;" class="linea-icon linea-aerrow fa-fw"></i> <span class="hide-menu">Log out</span></a></li> -->
                    </ul>
                </div>
            </div>
            <!-- Left navbar-header end -->
            <!-- Page wrapper -->
            <div id="page-wrapper">