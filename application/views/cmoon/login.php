<?php if($this->session->userdata('logged_in')){ 
    redirect('cmoon'); }  ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>cmoon_images/<?php echo $site_settings->site_favicon; ?>">
<title><?php echo $site_settings->site_name; ?></title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>cmoon_assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url(); ?>cmoon_assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>cmoon_assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url(); ?>cmoon_assets/css/colors/megna.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="http://www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<!-- <section id="wrapper" class="login-register" style="background: url(<?php echo base_url(); ?>cmoon_images/<?php echo $site_settings->site_favicon; ?>)  !important;"> -->
<section id="wrapper" class="login-register" style="background: url(<?php echo base_url(); ?>cmoon_assets/plugins/images/login-register.jpg) no-repeat center center / cover !important;">
  <div class="login-box">
    <div class="white-box">
      <form data-toggle="validator" class="form-horizontal form-material" id="loginform" method="POST" enctype='multipart/form-data' action="<?php echo base_url(); ?>cmoon_login">
         <?php if($this->session->flashdata('logout_success')){  ?>
            <div class="alert alert-success"> <?php echo $this->session->flashdata('logout_success'); ?> </div>
          <?php  }  ?>
          <?php if($this->session->flashdata('login_error')){  ?>
            <div class="alert alert-danger"> <?php echo $this->session->flashdata('login_error'); ?> </div>
          <?php  }  ?>
        <h3 class="box-title m-b-20">Sign In </h3>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" placeholder="username" name="username" data-error="Please Enter Username" required>
              <div class="help-block with-errors"></div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" type="password" placeholder="password" name="password" data-error="Please Enter Password" required>
              <div class="help-block with-errors"></div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <!-- <div class="checkbox checkbox-primary pull-left p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"> Remember me </label>
            </div> -->
            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
          </div>
        </div>
       <!--  <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
            <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
          </div>
        </div> -->
        <!-- <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>Don't have an account? <a href="register.html" class="text-primary m-l-5"><b>Sign Up</b></a></p>
          </div>
        </div> -->
      </form>


      <form data-toggle="validator" class="form-horizontal" id="recoverform" method="POST" action="<?php echo base_url(); ?>cmoon_login/reset_password">
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Recover Password</h3>
            <p class="text-muted">Enter your Password recover Email and default password will be sent to you! </p>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
           <input class="form-control" type="email" placeholder="Email" name="email" data-error="Invalid Email Address" required>
              <div class="help-block with-errors"></div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <a href="<?php echo base_url(); ?>cmoon_login" id="to-recover" class="text-dark pull-right"><i class="fa fa-key m-r-5"></i> SIGN IN</a> </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
          </div>
        </div>
      </form>


    </div>
  </div>
</section>
<!-- jQuery -->
<!-- <script src="<?php echo base_url(); ?>cmoon_assets/js/jquery-1.9.1.min"></script> -->
<script src="<?php echo base_url(); ?>cmoon_assets/js/jquery.validate.min"></script>
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>cmoon_assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>cmoon_assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>cmoon_assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>cmoon_assets/js/custom.min.js"></script>

<!--Style Switcher -->
<script src="<?php echo base_url(); ?>cmoon_assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>cmoon_assets/js/custom.min.js"></script>
<script src="<?php echo base_url(); ?>cmoon_assets/js/validator.js"></script>

</body>
</html>