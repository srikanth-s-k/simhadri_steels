<div class="container-fluid">

    <div class="row bg-title">

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">

            <h4 class="page-title">Add or Edit Products</h4>

        </div>



    </div>



    <?php
    if ($this->uri->segment(4) != '') {
        foreach ($values as $row)
            ;
    }
    ?>

    <!--.row-->

    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-info">

                <div class="panel-heading"> Add or Edit Products<a href="<?php echo base_url(); ?>cmoon/products/<?php echo $this->uri->segment(3) ?>?&page=<?php echo $this->uri->segment(4); ?>" class="btn  btn waves-effect waves-light btn-default" style="float:right; color: black;">Back</a></div>

                <div class="panel-wrapper collapse in" aria-expanded="true">

                    <div class="panel-body">



                        <form action="<?php echo base_url(); ?>cmoon/products_adding/<?php echo $this->uri->segment(3); ?>/<?php echo $row->id; ?>" id="form_data" method="POST" enctype='multipart/form-data' class="form-horizontal" autocomplete="off" >



                            <input type="hidden" name="products_title_id" value="<?php echo $this->uri->segment(3); ?>">



                            <div class="form-body">

                                <h3 class="box-title"></h3>

                                <hr class="m-t-0 m-b-40">




                                <div class="row">

                                    <div class="form-group">

                                        <div class="col-md-2">

                                            <label class="control-label">Title </label>

                                        </div>

                                        <div class="col-md-6">

                                            <input type="text" class="form-control" name="heading" placeholder="" value="<?php echo $row->heading; ?>" >

                                            <div class="help-block with-errors"></div>

                                        </div>

                                    </div>

                                </div>

                                <?php if ($row->image != '') { ?>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-6">
                                                <div class="image-popups">
                                                    <a href="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>" data-effect="mfp-3d-unfold"><img style="width: 300PX; background: black;" src="<?php echo base_url(); ?>cmoon_images/<?php echo $row->image; ?>" class="img-responsive" /></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                <?php } ?>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label class="control-label col-md-3"> Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image" id="file_type" onchange="Checkfiles()" />
                                                </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                            <div class="help-block with-errors"></div>
                                            <span class="help-block">Please Upload the image of ( 358 X 180 ) pixels only to maintain the desing</span>
                                            <span class="help-block"> Note : Only jpg,jpeg,png and gif formats are allowed </span>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="box-title">Quality Tables  [Grade Specifications]</h3>
                                <hr class="m-t-0 m-b-40"
                                    <div class="row">

                                <div class="form-group">

                                    <div class="col-md-2">

                                        <label class="control-label">Standard </label>

                                    </div>

                                    <div class="col-md-6">

                                        <input type="text" class="form-control" name="standard" placeholder="" value="<?php echo $row->standard; ?>" >

                                        <div class="help-block with-errors"></div>

                                    </div>

                                </div>

                            </div>


                            <div class="form-group">

                                <div class="col-md-2">

                                    <label class="control-label">Grade </label>

                                </div>

                                <div class="col-md-6">

                                    <input type="text" class="form-control" name="grade" placeholder="" value="<?php echo $row->grade; ?>" >

                                    <div class="help-block with-errors"></div>

                                </div>

                            </div>
                            <div class="form-group">

                                <div class="col-md-2">

                                    <label class="control-label">Remarks </label>

                                </div>

                                <div class="col-md-6">

                                    <input type="text" class="form-control" name="remarks" placeholder="" value="<?php echo $row->remarks; ?>" >

                                    <div class="help-block with-errors"></div>

                                </div>

                            </div>
                            <div class="form-group">

                                <div class="col-md-2">

                                    <label class="control-label">Yield Strength </label>

                                </div>

                                <div class="col-md-6">

                                    <input type="text" class="form-control" name="yield_strength" placeholder="" value="<?php echo $row->yield_strength; ?>" >

                                    <div class="help-block with-errors"></div>

                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-md-2">

                                    <label class="control-label">UTS </label>

                                </div>

                                <div class="col-md-6">

                                    <input type="text" class="form-control" name="uts" placeholder="" value="<?php echo $row->uts; ?>" >

                                    <div class="help-block with-errors"></div>

                                </div>

                            </div>
                            <div class="form-group">

                                <div class="col-md-2">

                                    <label class="control-label">% Elongation </label>

                                </div>

                                <div class="col-md-6">

                                    <input type="text" class="form-control" name="elongation" placeholder="" value="<?php echo $row->elongation; ?>" >

                                    <div class="help-block with-errors"></div>

                                </div>

                            </div>



                            <div class="form-actions">

                                <div class="row">

                                    <div class="col-md-6">

                                        <div class="row">

                                            <div class="col-md-offset-4 col-md-9">

                                                <button type="submit" class="btn btn-success">Submit</button>

                                                <!-- <button type="button" class="btn btn-default">Cancel</button> -->

                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-6"> </div>

                                </div>

                            </div>

                    </div>

                    </form>

                </div>

            </div>

        </div>

    </div>

</div>

</div>







<script type="text/javascript">

    $(function () {

// Setup form validation on the #register-form element

        $("#form_data").validate({

            // Specify the validation rules

            ignore: [],

            debug: false,

            rules: {

                heading: {

                    required: true

                },

                description: {

                    required: true

                },

                offer_zone_image_alt: {

                    required: true

                },

                search_page_image: {

                    required: true

                },

                popup_image_alt: {

                    required: true

                }



            },

// Specify the validation error messages

            messages: {

                heading: {

                    required: 'Field shouldnot be empty'

                },

                description: {

                    required: 'Field shouldnot be empty'

                },

                offer_zone_image_alt: {

                    required: 'Field shouldnot be empty'

                },

                search_page_image: {

                    required: 'Field shouldnot be empty'

                },

                popup_image_alt: {

                    required: 'Field shouldnot be empty'

                }

            },

        });

    });



    function Checkfiles() {

        var fup = document.getElementById('file_type');

        var fileName = fup.value;

        var ext = fileName.substring(fileName.lastIndexOf('.') + 1);

        if (ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "gif") {

            return true;

        } else {

            alert("Upload pdf Files only");

            document.getElementById('file_type').value = "";

            fup.focus();

            return false;

        }

    }

</script>