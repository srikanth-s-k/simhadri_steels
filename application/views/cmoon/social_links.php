    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Social Links & SEO Tags</h4>
        </div>
       <!--  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Form Layout</li>
          </ol>
        </div> -->
        <!-- /.col-lg-12 -->
      </div>

      <!--.row-->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-info">
            <div class="panel-heading"> Social Links  </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
              <div class="panel-body">
          <form action="<?php echo base_url(); ?>cmoon/social_links" data-toggle="validator" method="POST" enctype='multipart/form-data' class="form-horizontal" autocomplete="off" >


              <div class="form-body">
                <h3 class="box-title"></h3>
                <hr class="m-t-0 m-b-40">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3">Facebook</label>
                      <div class="col-md-9">
    <input type="text" class="form-control" name="facebook" placeholder="Facebook" data-error="Please enter the link" value="<?php echo  $row->facebook; ?>" required>
                          <div class="help-block with-errors"></div>
                        <!-- <span class="help-block"> This is inline help </span>  -->
                      </div>
                    </div>
                  </div>
                  <!--/span-->
                  <!-- <div class="col-md-6">
                    <div class="form-group has-error">
                      <label class="control-label col-md-3">Site Phone Number</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="12n">
                        <span class="help-block"> This field has error. </span> 
                      </div>
                    </div>
                  </div> -->
                  <!--/span-->
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3">Twitter</label>
                      <div class="col-md-9">
        <input type="text" class="form-control" name="twitter" placeholder="Twitter" value="<?php echo  $row->twitter; ?>" data-error="Please enter the link" required>
                          <div class="help-block with-errors"></div>
                         <!--<span class="help-block"> Phone number should be between 10-14 digits only </span>  -->
                      </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> Linkedin </label>
                      <div class="col-md-9">
    <input type="text" class="form-control" name="linkedin" value="<?php echo  $row->linkedin; ?>" placeholder="LinkedIn" data-error="Please enter the link" required>
                          <div class="help-block with-errors"></div>
                        <!-- <span class="help-block"> This is inline help </span>  -->
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> Youtube </label>
                      <div class="col-md-9">
  <input type="text" class="form-control" name="youtube" value="<?php echo  $row->youtube; ?>" placeholder="YouTube" data-error="Please enter the link" required>
                          <div class="help-block with-errors"></div>
                          <!-- <span class="help-block"> This is inline help </span>  -->
                      </div>
                    </div>
                  </div>
                </div>
                
  <!--               <div class="row">-->
  <!--                <div class="col-md-6">-->
  <!--                  <div class="form-group">-->
  <!--                    <label class="control-label col-md-3"> Whatsapp </label>-->
  <!--                    <div class="col-md-9">-->
  <!--<input type="text" class="form-control" name="youtube" value="<?php echo  $row->youtube; ?>" placeholder="YouTube" data-error="Please enter the link" required>-->
  <!--                        <div class="help-block with-errors"></div>-->
                          <!-- <span class="help-block"> This is inline help </span>  -->
  <!--                    </div>-->
  <!--                  </div>-->
  <!--                </div>-->
  <!--              </div>-->
                
  <!--              <div class="row">-->
  <!--                <div class="col-md-6">-->
  <!--                  <div class="form-group">-->
  <!--                    <label class="control-label col-md-3"> Whatsapp </label>-->
  <!--                    <div class="col-md-9">-->
  <!--<input type="text" class="form-control" name="whatsapp" value="<?php echo  $row->whatsapp; ?>" placeholder="Whatsapp" data-error="Please enter the link" required>-->
  <!--                        <div class="help-block with-errors"></div>-->
                           <!--<span class="help-block"> This is inline help </span>  -->
  <!--                    </div>-->
  <!--                  </div>-->
  <!--                </div>-->
  <!--              </div>-->

<!--                 <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> Google <big><sup>+</sup></big></label>
                      <div class="col-md-9">
  <input type="text" class="form-control" name="google" value="<?php echo  $row->google; ?>" placeholder="Google <big><sup>+</sup>+</big>" data-error="Please enter the link" required>
                          <div class="help-block with-errors"></div>
                      </div>
                    </div>
                  </div>
                </div> -->

                <!-- <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> Pinterest </label>
                      <div class="col-md-9">
  <input type="text" class="form-control" name="pinterest" value="<?php echo  $row->pinterest; ?>" placeholder="Pinterest " data-error="Please enter the link" required>
                          <div class="help-block with-errors"></div>
                        <!- <span class="help-block"> This is inline help </span> 
                      </div>
                    </div>
                  </div>
                </div> -->


  <!--              <div class="row">-->
  <!--                <div class="col-md-6">-->
  <!--                  <div class="form-group">-->
  <!--                    <label class="control-label col-md-3"> Instagram </label>-->
  <!--                    <div class="col-md-9">-->
  <!--<input type="text" class="form-control" name="instagram" value="<?php echo  $row->instagram; ?>" placeholder="Instagram " data-error="Please enter the link" required>-->
  <!--                        <div class="help-block with-errors"></div>-->
                        <!-- <span class="help-block"> This is inline help </span>  -->
  <!--                    </div>-->
  <!--                  </div>-->
  <!--                </div>-->
  <!--              </div>-->



                <!-- <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> Skype </label>
                      <div class="col-md-9">
  <input type="text" class="form-control" name="skype" value="<?php echo  $row->skype; ?>" placeholder="Skype " data-error="Please enter the link" required>
                          <div class="help-block with-errors"></div>
                         <span class="help-block"> This is inline help </span> 
                      </div>
                    </div>
                  </div>
                </div> -->


                <!-- <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> WhatsApp </label>
                      <div class="col-md-9">
  <input type="text" class="form-control" name="whatsapp" value="<?php echo  $row->whatsapp; ?>" placeholder="WhatsApp " data-error="Please enter the link" required>
                          <div class="help-block with-errors"></div>
                         <span class="help-block"> This is inline help </span> 
                      </div>
                    </div>
                  </div>
                </div> -->


<!--                 <h3 class="box-title">Google Map Code</h3>
                <hr class="m-t-0 m-b-40">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3">Google Map Code </label>
                      <div class="col-md-9">
                        <textarea class="form-control" rows="5" name="map" data-error="Please enter map code" placeholder="Map Code" required><?php echo  $row->map; ?></textarea>
                          <div class="help-block with-errors"></div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3"> See Google Map </label>
                      <div class="col-md-9">
                       <a class="popup-gmaps btn btn-info" href="<?php echo  $row->map; ?>" style="color: white;" >Open Google Map</a>
                      </div>
                    </div>
                  </div>
                </div> -->


               

                <!--<h3 class="box-title">Twitter Tages</h3>-->
                <!--<hr class="m-t-0 m-b-40">-->


                <!--<div class="row">-->
                <!--  <div class="col-md-6">-->
                <!--    <div class="form-group">-->
                <!--      <label class="control-label col-md-3">Twitter Title</label>-->
                <!--      <div class="col-md-9">-->
                <!--         <textarea class="form-control" rows="5" name="twitter_title" placeholder="Twitter Title"><?php echo  $row->twitter_title; ?></textarea>-->
                <!--          <div class="help-block with-errors"></div>-->
                <!--      </div>-->
                <!--    </div>-->
                <!--  </div>-->
                <!--</div>-->


                <!--<div class="row">-->
                <!--  <div class="col-md-6">-->
                <!--    <div class="form-group">-->
                <!--      <label class="control-label col-md-3">Twitter Description</label>-->
                <!--      <div class="col-md-9">-->
                <!--         <textarea class="form-control" rows="5" name="twitter_description" placeholder="Twitter Description"><?php echo  $row->twitter_description; ?></textarea>-->
                <!--          <div class="help-block with-errors"></div>-->
                <!--      </div>-->
                <!--    </div>-->
                <!--  </div>-->
                <!--</div>-->

                <!--<div class="row">-->
                <!--  <div class="col-md-6">-->
                <!--    <div class="form-group">-->
                <!--      <label class="control-label col-md-3">Twitter Card</label>-->
                <!--      <div class="col-md-9">-->
                <!--         <textarea class="form-control" rows="5" name="twitter_card" placeholder="Twitter Card"><?php echo  $row->twitter_card; ?></textarea>-->
                <!--          <div class="help-block with-errors"></div>-->
                <!--      </div>-->
                <!--    </div>-->
                <!--  </div>-->
                <!--</div>-->


                <!--/row-->
              </div>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <!-- <button type="button" class="btn btn-default">Cancel</button> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6"> </div>
                </div>
              </div>
            </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--./row-->


    </div>