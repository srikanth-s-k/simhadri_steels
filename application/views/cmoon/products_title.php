<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Products Title</h4>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /row -->
    <div class="row">

        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Products Title
                    <a href="<?php echo base_url(); ?>cmoon/products_title_adding" class="btn  btn waves-effect waves-light btn-success" style="float:right">Add Products Title</a>
                </h3>
                <div class="table-responsive">
                    <table class="table color-bordered-table dark-bordered-table">
                    <!-- <table class="table full-color-table full-dark-table hover-table"> -->
                        <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>Tittle</th>
                                <!--<th>Car Name</th>-->

                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>


                            <?php
                            if ($_GET['page'] != '') {
                                $slno = $perpage * $_GET['page'] - 1;
                            } else {
                                $slno = 1;
                            }
                            foreach ($values as $row) {
                                ?>

                                <tr>
                                    <td><?php echo $slno; ?></td>

                                    <td><?php echo $row->heading; ?></td>

                                    <td>

                                        <a href="<?php echo base_url(); ?>cmoon/products_title_adding/<?php echo $row->id; ?>" class="btn  btn waves-effect waves-light btn-primary">Edit</a> &nbsp; &nbsp;
                                        <a href="<?php echo base_url(); ?>cmoon/products/<?php echo $row->id; ?>" class="btn  btn waves-effect waves-light btn-primary">Add Products </a> &nbsp; &nbsp;
                                        <a href="JavaScript:Void(0);" onclick="deleteitem(<?php echo $row->id; ?>)"  class="btn  btn waves-effect waves-light btn-danger" >Delete</a>


                                    </td>
                                </tr>
                                <?php
                                $slno++;
                            }
                            ?>
                        </tbody>
                    </table>


                    <?php echo $pagination; ?>

                </div>
            </div>
        </div>

    </div>
    <!-- /.row -->
</div>


<script type="text/javascript">


    //Warning Message
    // $('.sa-warning-delete').click(function(){
    //     swal({
    //         title: "Are you sure?",
    //         text: "You will not be able to recover this imaginary file!",
    //         type: "warning",
    //         showCancelButton: true,
    //         confirmButtonColor: "#DD6B55",
    //         confirmButtonText: "Yes, delete it!",
    //         closeOnConfirm: false
    //     }, function(){
    //         swal("Deleted!", "Your imaginary file has been deleted.", "success");
    //     });
    // });


    function deleteitem(item) {

        //Warning Message
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            window.location.href = '<?php echo base_url(); ?>cmoon/deleate_products_title"/' + item;
        });

    }

</script>
