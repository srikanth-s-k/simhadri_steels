<div class="container-fluid">



    <div class="row bg-title">




        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Add or Edit Dealers city</h4>
        </div>
        <!--         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                  <a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>
                  <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Forms</a></li>
                    <li class="active">Form Layout</li>
                  </ol>
                </div> -->
        <!-- /.col-lg-12 -->
    </div>



    <?php
    if ($this->uri->segment(3) != '') {
        foreach ($values as $row)
            ;
    }
    ?>


    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading"> Add or Edit Dealers city <a href="<?php echo base_url(); ?>cmoon/dealers_city" class="btn  btn waves-effect waves-light btn-default" style="float:right; color: black;">Back</a></div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form action="<?php echo base_url(); ?>cmoon/dealers_city_adding/<?php echo $row->id; ?>" id="form_data" method="POST" enctype='multipart/form-data' class="form-horizontal" autocomplete="off" >

                            <div class="form-body">
                                <h3 class="box-title"></h3>
                                <hr class="m-t-0 m-b-40">

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label class="control-label">Heading </label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="heading" placeholder="Heading" value="<?php echo $row->heading; ?>">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>



                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-offset-4 col-md-9">
                                                    <button type="submit" class="btn btn-success">Submit</button>
                                                    <!-- <button type="button" class="btn btn-default">Cancel</button> -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"> </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        // Setup form validation on the #register-form element
        $("#form_data").validate({
            // Specify the validation rules
            ignore: [],
            debug: false,
            rules: {
                //   heading: {
                //       required: true
                //     },
                // image_alt: {
                //   required: true
                // }
            },
            // Specify the validation error messages
            messages: {
                //   heading: {
                //       required: 'Field shouldnot be empty'
                //     },
                // image_alt: {
                //   required: 'Field shouldnot be empty'
                // }
            },
        });
    });

    function Checkfiles() {
        var fup = document.getElementById('file_type');
        var fileName = fup.value;
        var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
        if (ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "gif" || ext == "svg") {
            return true;
        } else {
            alert("Upload jpeg, jpg, png, gif,svg Files only");
            document.getElementById('file_type').value = "";
            fup.focus();
            return false;
        }
    }
</script>