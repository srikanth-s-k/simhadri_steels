<?php

defined('BASEPATH') or exit('No direct script access allowed');

class view extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        // $this->load->model('login_model','',True);
        $this->load->model('site_model', '', true);
        $this->load->model('cmoon_model', '', true);
        $this->data["site_settings"] = $this->site_model->get_cms('*', 'site_settings', '1');
        $this->data["social_links"] = $this->site_model->get_cms('*', 'social_links', '1');
    }

    //------------------------------  pages ---------------------------------------------

    public function index() {


        if ($this->input->get('submit') == "search") {

            if ($this->input->get('date') != "") {
                $this->db->where('publishing_date', $this->input->get('date'));
            }
            if ($this->input->get('state_id') != "") {
                $this->db->where('states_id', $this->input->get('state_id'));
            }
            if ($this->input->get('districts_id') != "") {
                $this->db->where('district_id', $this->input->get('districts_id'));
            }
            $data['district_content'] = $this->site_model->get('id,image,pdf,publishing_date', 'district_content', 'id', 'ASC');
            //  echo $this->db->last_query(); die;
        }

        $data["site_settings"] = $this->site_model->get_cms('*', 'site_settings', '1');
        $data["social_links"] = $this->site_model->get_cms('*', 'social_links', '1');
        $data["cms_pages1"] = $this->site_model->get_cms('heading1,image1,description1', 'cms_pages', '1');
        $data["cms_pages2"] = $this->site_model->get_cms('heading1,image1,description1', 'cms_pages', '2');
        $data["cms_pages3"] = $this->site_model->get_cms('heading1,image1,description1,description2', 'cms_pages', '3');
        $data["cms_pages4"] = $this->site_model->get_cms('heading1,image1,description1', 'cms_pages', '4');
        $data["cms_pages5"] = $this->site_model->get_cms('heading1,image1,description1', 'cms_pages', '5');
        $data['home_banners'] = $this->site_model->get('image,heading,image_alt', 'home_banners', 'id', 'ASC');
        $data['scroll'] = $this->site_model->get('heading', 'scroll', 'id', 'ASC');
        $data['partners'] = $this->site_model->get('image,image_alt,link', 'partners', 'id', 'ASC');
        $this->load->view('index', $data);
    }

// ----------------------------------     form details    -------------------------------------------

    public function contact_us_form1() {

        mail_config();
        $name = $this->input->post('name', TRUE);

        $email = $this->input->post('email', TRUE);

        $message = $this->input->post('message', TRUE);
        $subject = $this->input->post('subject', TRUE);

        if ($name != '' && $email != '' && $message != '' && $subject != '') {
            $form_data = $this->input->post();
            //  print_r($form_data);die;

            $this->db->insert('contact_us_form_details', $form_data);
            $to_mail = $this->data["site_settings"]->contact_email;
            $from_email = $this->data["site_settings"]->from_email;
            $site_name = $this->data["site_settings"]->site_name;

            $mail_body = "<html>
                        		<head><title>" . $site_name . "</title></head>
                        		<body>
                        		   <table style='border: 2px solid #06F;padding:10px;border-radius:10px;'>
                        			  <tr>
                        				  <td width='300'>
                        					  <div class='maindiv'>

                        						 <p>Hi Admin, </p>
                        						 <p>You have a new enquiry.</p>

                        				          <p>Name     : " . $name . " </p>

                        				          <p>Message       : " . $message . " </p>
                        						  <p>Email-Id       : " . $email . " </p>

                        						  <p>subject 	    : " . $subject . " </p>

                        						 <p>From  " . $site_name . ".</p>
                        					  </div>
                        				  </td>
                        			  </tr>
                        		  </table>
                        		</body>
                        		</html>
                        ";

            $sub = "Enquiry Details from " . $site_name;
            $this->email->from($from_email, $site_name);
            $this->email->to($to_mail);
            $this->email->subject($sub);
            $this->email->message($mail_body);
            $send = $this->email->send();

            //print_r($send);die;
            if ($send) {

                // $responce=array('msg'=>'success','text'=>'Thank you for showing your intrest, We will address your request very soon.');
                // echo json_encode($responce);
                $this->session->set_flashdata('mail_success', 'Thank you for showing your interest, We will address your request very soon.');
                redirect('view/contact');
            } else {



                $this->session->set_flashdata('mail_error', 'Sorry something went wrong please try again refreshing the page');
                redirect('view/contact');
            }
        }
    }

// ---------------------------- Image upload code -----------------------------------------------------------------



    private function upload_file($file_name, $path = null) {
        $upload_path1 = "lpauto_images" . $path;
        $config1['upload_path'] = $upload_path1;
        $config1['allowed_types'] = "gif|jpg|png|jpeg";
        $config1['max_size'] = "100480000";
        $img_name1 = strtolower($_FILES[$file_name]['name']);
        $img_name1 = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name'] = date("YmdHis") . rand(0, 999) . "-" . $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }

    // ------------------------------------------------------------------------------- pdf upload code -----------------------------------------------------------------



    private function upload_file_brochure($file_name, $path = null) {


        $upload_path1 = "lpauto_images" . $path;
        $config1['upload_path'] = $upload_path1;
        $config1['allowed_types'] = "pdf|dox|docx";
        $config1['max_size'] = "100480000";
        $img_name1 = strtolower($_FILES[$file_name]['name']);
        $img_name1 = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name'] = date("YmdHis") . rand(0, 999) . "-" . $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }

}
