<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('api/Login_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

// ----------------------------------     form details    -------------------------------------------

    public function index() {

        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('type', 'type', 'required');

        if ($this->form_validation->run() == FALSE) {

            $arr = [
                "status" => "invalid",
                "message" => "Please check all your form details"
            ];
            echo json_encode($arr);
            die;
        } else {

            $data = [
                "mobile" => $this->input->post('mobile'),
                "password" => $this->input->post('password'),
                "type" => $this->input->post('type'),
            ];
        }

//check mobile unique
        $is_mobile_exists = $this->Login_model->check_mobile_exists($data['mobile']);
        if (!$is_mobile_exists) {


            $arr = [
                "status" => "invalid",
                "message" => "Invalid mobile number"
            ];
            echo json_encode($arr);
            die;
        } else if ($is_mobile_exists->id != "") {

            if ($is_mobile_exists->password != sha1($data['password'])) {
                $arr = [
                    "status" => "invalid",
                    "message" => "Invalid Password"
                ];
                echo json_encode($arr);
                die;
            } else if ($is_mobile_exists->password == sha1($data['password'])) {

                $arr = [
                    "status" => "valid",
                    "message" => "Login successfull",
                    "data" => $is_mobile_exists->id,
                ];
                echo json_encode($arr);
                die;
            }
        }
    }

}
