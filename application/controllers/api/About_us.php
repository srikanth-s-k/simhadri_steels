<?php

defined('BASEPATH') or exit('No direct script access allowed');

class About_us extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('api/About_us_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

// ----------------------------------     form details    -------------------------------------------

    public function index() {

        $about = $this->About_us_model->get_data('*', 'about_us', '1');
//        echo $this->db->last_query();
//        die;

        $arr = [
            "status" => "valid",
            "message" => "Content details",
            "data" => $about->description
        ];
        echo json_encode($arr);
        die;
    }

}
