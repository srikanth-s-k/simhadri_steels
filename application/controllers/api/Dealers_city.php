<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dealers_city extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('api/Dealers_city_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

// ----------------------------------     form details    -------------------------------------------

    public function index() {

        $dealers_city = $this->Dealers_city_model->get_result('*', 'dealers_city', 'id',);
//        echo $this->db->last_query();
//        die;

        $arr = [
            "status" => "valid",
            "message" => "Content details",
            "data" => $dealers_city
        ];
        echo json_encode($arr);
        die;
    }

}
