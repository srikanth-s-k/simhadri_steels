<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Registration extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('api/Registration_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

// ----------------------------------     form details    -------------------------------------------

    public function index() {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'email', 'required|valid_email');
        $this->form_validation->set_rules('mobile', 'mobile', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('type', 'type', 'required');

        if ('type' == "dealer") {
            $this->form_validation->set_rules('first_name', 'first_name', 'required');
            $this->form_validation->set_rules('gst_no', 'gst_no', 'required');
            $this->form_validation->set_rules('address', 'address', 'required');
        }
        if ($this->form_validation->run() == FALSE) {
            $arr = [
                "status" => "invalid",
                "message" => "Please check all your form details"
            ];
            echo json_encode($arr);
            die;
        } else {
            $data = [
                "username" => $this->input->post('username'),
                "mobile" => $this->input->post('mobile'),
                "password" => sha1($this->input->post('password')),
                "email" => $this->input->post('email'),
                "type" => $this->input->post('type'),
                "otp" => rand(1111, 9999)
            ];
        }
        if ('type' == "dealer") {
            $data["first_name"] = $this->input->post('first_name');
            $data["gst_no"] = $this->input->post('gst_no');
            $data["address"] = $this->input->post('address');
        }

//check mobile unique
        $is_mobile_exists = $this->Registration_model->check_mobile_exists($data['mobile']);

        if ($is_mobile_exists) {
            $arr = [
                "status" => "invalid",
                "message" => "Mobile number already exists"
            ];
            echo json_encode($arr);
            die;
        } else if (!$is_mobile_exists) {
            $response = $this->Registration_model->insert($data);
            echo $this->db->last_query();
            die;
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "message" => "Registration success,please verify the otp sent to your mobile number"
                ];
                echo json_encode($arr);
                die;
            } else {
                $arr = [
                    "status" => "invalid",
                    "message" => "Problem with server.Plesae try later"
                ];
                echo json_encode($arr);
                die;
            }
        } else {
            $arr = [
                "status" => "invalid",
                "message" => "Problem with server.Plesae try later"
            ];
            echo json_encode($arr);
            die;
        }
    }

}
