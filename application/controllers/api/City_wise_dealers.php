<?php

defined('BASEPATH') or exit('No direct script access allowed');

class City_wise_dealers extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('api/City_wise_dealers_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

// ----------------------------------     form details    -------------------------------------------

    public function index() {

        $this->form_validation->set_rules('city_id', 'city_id', 'required');

        if ($this->form_validation->run() == FALSE) {

            $arr = [
                "status" => "invalid",
                "message" => "Please check all your form details"
            ];
            echo json_encode($arr);
            die;
        } else {
            $data = [
                "city_id" => $this->input->post('city_id'),
            ];

            $delears_get = $this->City_wise_dealers_model->get_result('*', 'dealers', 'dealers_city_id', $data['city_id']);

//            echo $this->db->last_query();
//            die;
        }

//check result
        if ($delears_get) {
            $arr = [
                "status" => "valid",
                "message" => "Dealers details",
                "data" => $delears_get
            ];
            echo json_encode($arr);
            die;
        } else {
            $arr = [
            ];
            echo json_encode($arr);
            die;
        }
    }

}
