<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Verify_otp extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('api/Verify_otp_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

// ----------------------------------     form details    -------------------------------------------

    public function index() {

        $this->form_validation->set_rules('otp', 'otp', 'required');
        $this->form_validation->set_rules('user_id', 'user_id', 'required');


        if ($this->form_validation->run() == FALSE) {

            $arr = [
                "status" => "invalid",
                "message" => "Please check all your form details"
            ];
            echo json_encode($arr);
            die;
        } else {

            $data = [
                "otp" => $this->input->post('otp'),
                "user_id" => $this->input->post('user_id')
            ];
        }

//check Otp unique
        $otp_check = $this->Verify_otp_model->check_otp($data['otp'], $data['user_id']);
        if (!$otp_check) {


            $arr = [
                "status" => "invalid",
                "message" => "Invalid otp"
            ];
            echo json_encode($arr);
            die;
        } else if ($otp_check) {
            $arr = [
                "status" => "valid",
                "message" => "valid otp"
            ];
            echo json_encode($arr);
            die;
        }
    }

}
