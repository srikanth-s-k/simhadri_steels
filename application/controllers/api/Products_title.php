<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Products_title extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('api/Products_title_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

// ----------------------------------     form details    -------------------------------------------

    public function index() {

        $products_title = $this->Products_title_model->get_result('*', 'products_title', 'id',);
        foreach ($products_title as $products_title1) {
            $products_title1->products = $this->Products_title_model->get_products($products_title1->id);
        }
//        echo $this->db->last_query();
//        die;

        $arr = [
            "status" => "valid",
            "message" => "Products title details",
            "data" => $products_title
        ];
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

}
