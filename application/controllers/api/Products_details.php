<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Products_details extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('api/Products_details_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

// ---------------------------------- form details    -------------------------------------------

    public function index() {
        $data = [
            "products_id" => $this->input->post('products_id'),
        ];

        $products = $this->Products_details_model->get_products_details('*', 'products', 'products_title_id =', $data['products_id']);


        echo $this->db->last_query();
        die;

        $arr = [
            "status" => "valid",
            "message" => "Products  details",
            "data" => $products
        ];
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

}
