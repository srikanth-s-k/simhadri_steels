<?php

defined('BASEPATH') or exit('No direct script access allowed');

// class admin extends MY_Controller
class Cmoon extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();
// $this->load->model('login_model','',True);
        $this->load->model('cmoon_model', '', true);
        $this->data["site_settings"] = $this->cmoon_model->get_cms('site_settings', '1');
    }

// ------------------------------------------   index code    -------------------------------------------

    public function index() {
        $this->load->view('cmoon/includes/header', $this->data);
        $this->load->view('cmoon/index');
        $this->load->view('cmoon/includes/footer');
    }

// ----------------------  Dealers  code  -------------------------------------------
    public function dealers($dealers_city_id) {


        $this->load->view('cmoon/includes/header', $this->data);



        $data["dealers_city"] = $this->cmoon_model->get_cms('dealers_city', $dealers_city_id);



        $data['perpage'] = 15;

        $total_records = count($this->cmoon_model->get_result_cat('dealers', $dealers_city_id, 'dealers_city_id'));

        $pagination = my_pagination_cmoon('cmoon/dealers/' . $dealers_city_id . ' ' . '', $data['perpage'], $total_records);

        $data['pagination'] = $pagination['pagination'];



        $data['values'] = $this->cmoon_model->get_result_cat('dealers', $dealers_city_id, 'dealers_city_id', $data['perpage']);

        $this->load->view('cmoon/dealers', $data);

        $this->load->view('cmoon/includes/footer');
    }

    public function dealers_adding($dealers_city_id = '', $id = '') {
        if ($this->input->post()) {
            $form_data = $this->input->post();
            $form_data['p_link'] = $this->permalink($this->input->post('heading'));
            if ($id != '') {


                $result = $this->cmoon_model->update('dealers', $id, $form_data);
                if ($result == true) {
                    redirect('cmoon/dealers/' . $form_data['dealers_city_id'] . '/Success');
                } else {
                    redirect('cmoon/dealers_adding/' . $form_data['dealers_city_id'] . '/error');
                }
            } else {


                $result = $this->cmoon_model->insert('dealers', $form_data);
                if ($result == true) {
                    redirect('cmoon/dealers/' . $form_data['dealers_city_id'] . '/Success');
                } else {
                    redirect('cmoon/dealers_adding/' . $form_data['dealers_city_id'] . '/error');
                }
            }
        } else {
            if ($id != '') {

//   echo "phani"; die;
                $this->load->view('cmoon/includes/header', $this->data);
                $data['values'] = $this->cmoon_model->get_row('dealers', $id);
                $data['dealers_city'] = $this->db->get('dealers_city')->result();
                $data['dealers'] = $this->cmoon_model->get_result_by_id('dealers', 'dealers_city_id', $data['values'][0]->products_id);

//   echo $this->db->last_query(); die;
                $this->load->view('cmoon/dealers_adding', $data);
                $this->load->view('cmoon/includes/footer');
            } else {
                $this->load->view('cmoon/includes/header', $this->data);
                $data['dealers_city'] = $this->db->get('dealers_city')->result();
                $this->load->view('cmoon/dealers_adding', $data);
                $this->load->view('cmoon/includes/footer');
            }
        }
    }

    public function deleate_dealers($dealers_city_id, $id, $page) {
        $del_existing_img = $this->cmoon_model->get_single_row_by_id('dealers', $id);
        unlink("cmoon_images/" . $del_existing_img->pdf);
        if ($page == 'undefined') {
            $page = '';
        }
        $result = $this->cmoon_model->delete('dealers', $id);
        if ($result == true) {
            redirect('cmoon/dealers/' . $dealers_city_id . '/dSuccess?&page=' . $page);
        } else {
            redirect('cmoon/products_content_adding/' . $dealers_city_id . '/error?&page=' . $page);
        }
    }

// ----------------------  Products  code  -------------------------------------------
    public function products($products_title_id) {


        $this->load->view('cmoon/includes/header', $this->data);



        $data["products_title"] = $this->cmoon_model->get_cms('products_title', $products_title_id);



        $data['perpage'] = 15;

        $total_records = count($this->cmoon_model->get_result_cat('products', $products_title_id, 'products_title_id'));

        $pagination = my_pagination_cmoon('cmoon/products/' . $products_title_id . ' ' . '', $data['perpage'], $total_records);

        $data['pagination'] = $pagination['pagination'];



        $data['values'] = $this->cmoon_model->get_result_cat('products', $products_title_id, 'products_title_id', $data['perpage']);

        $this->load->view('cmoon/products', $data);

        $this->load->view('cmoon/includes/footer');
    }

    public function products_adding($products_title_id = '', $id = '') {
        if ($this->input->post()) {
            $form_data = $this->input->post();
            $form_data['p_link'] = $this->permalink($this->input->post('heading'));
            if ($id != '') {
                $del_existing_img = $this->cmoon_model->get_single_row_by_id('products', $id);
                if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/" . $del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }

                $result = $this->cmoon_model->update('products', $id, $form_data);
                if ($result == true) {
                    redirect('cmoon/products/' . $form_data['products_title_id'] . '/Success');
                } else {
                    redirect('cmoon/products_adding/' . $form_data['products_title_id'] . '/error');
                }
            } else {

                if ($_FILES['image']['name'] != '') {
                    $form_data['image'] = $this->upload_file('image');
                }
                $result = $this->cmoon_model->insert('products', $form_data);
                if ($result == true) {
                    redirect('cmoon/products/' . $form_data['products_title_id'] . '/Success');
                } else {
                    redirect('cmoon/products_adding/' . $form_data['products_title_id'] . '/error');
                }
            }
        } else {
            if ($id != '') {

//   echo "phani"; die;
                $this->load->view('cmoon/includes/header', $this->data);
                $data['values'] = $this->cmoon_model->get_row('products', $id);
                $data['products_title'] = $this->db->get('products_title')->result();
                $data['products'] = $this->cmoon_model->get_result_by_id('products', 'products_title_id', $data['values'][0]->products_title_id);

//   echo $this->db->last_query(); die;
                $this->load->view('cmoon/products_adding', $data);
                $this->load->view('cmoon/includes/footer');
            } else {
                $this->load->view('cmoon/includes/header', $this->data);
                $data['products_title'] = $this->db->get('products_title')->result();
                $this->load->view('cmoon/products_adding', $data);
                $this->load->view('cmoon/includes/footer');
            }
        }
    }

    public function deleate_products($products_title_id, $id, $page) {
        $del_existing_img = $this->cmoon_model->get_single_row_by_id('products', $id);
        unlink("cmoon_images/" . $del_existing_img->pdf);
        if ($page == 'undefined') {
            $page = '';
        }
        $result = $this->cmoon_model->delete('products', $id);
        if ($result == true) {
            redirect('cmoon/products/' . $products_title_id . '/dSuccess?&page=' . $page);
        } else {
            redirect('cmoon/products_adding/' . $products_title_id . '/error?&page=' . $page);
        }
    }

//----------------------Products Selection Weights code-------------------------------------------
    public function products_selection_weights($products_id) {


        $this->load->view('cmoon/includes/header', $this->data);



        $data["products"] = $this->cmoon_model->get_cms('products', $products_id);



        $data['perpage'] = 15;

        $total_records = count($this->cmoon_model->get_result_cat('products_selection_weights', $products_id, 'products_id'));

        $pagination = my_pagination_cmoon('cmoon/products_selection_weights/' . $products_id . ' ' . '', $data['perpage'], $total_records);

        $data['pagination'] = $pagination['pagination'];

        $data['values'] = $this->cmoon_model->get_result_cat('products_selection_weights', $products_id, 'products_id', $data['perpage']);

        $this->load->view('cmoon/products_selection_weights', $data);

        $this->load->view('cmoon/includes/footer');
    }

    public function products_selection_weights_adding($products_id = '', $id = '') {
        if ($this->input->post()) {
            $form_data = $this->input->post();
            $form_data['p_link'] = $this->permalink($this->input->post('min_wt'));
            if ($id != '') {
                $del_existing_img = $this->cmoon_model->get_single_row_by_id('products_selection_weights', $id);


                $result = $this->cmoon_model->update('products_selection_weights', $id, $form_data);
                if ($result == true) {
                    redirect('cmoon/products_selection_weights/' . $form_data['products_id'] . '/Success');
                } else {
                    redirect('cmoon/products_selection_weights_adding/' . $form_data['products_id'] . '/error');
                }
            } else {


                $result = $this->cmoon_model->insert('products_selection_weights', $form_data);
                if ($result == true) {
                    redirect('cmoon/products_selection_weights/' . $form_data['products_id'] . '/Success');
                } else {
                    redirect('cmoon/products_selection_weights_adding/' . $form_data['products_id'] . '/error');
                }
            }
        } else {
            if ($id != '') {

//   echo "phani"; die;
                $this->load->view('cmoon/includes/header', $this->data);
                $data['values'] = $this->cmoon_model->get_row('products_selection_weights', $id);
                $data['products'] = $this->db->get('products')->result();
                $data['products_selection_weights'] = $this->cmoon_model->get_result_by_id('products_selection_weights', 'products_id', $data['values'][0]->products_id);

//   echo $this->db->last_query(); die;
                $this->load->view('cmoon/products_selection_weights_adding', $data);
                $this->load->view('cmoon/includes/footer');
            } else {
                $this->load->view('cmoon/includes/header', $this->data);
                $data['products'] = $this->db->get('products')->result();
                $this->load->view('cmoon/products_selection_weights_adding', $data);
                $this->load->view('cmoon/includes/footer');
            }
        }
    }

    public function deleate_products_selection_weights($products_id, $id, $page) {
        $del_existing_img = $this->cmoon_model->get_single_row_by_id('products_selection_weights', $id);
        unlink("cmoon_images/" . $del_existing_img->pdf);
        if ($page == 'undefined') {
            $page = '';
        }
        $result = $this->cmoon_model->delete('products_selection_weights', $id);
        if ($result == true) {
            redirect('cmoon/products_selection_weights/' . $products_id . '/dSuccess?&page=' . $page);
        } else {
            redirect('cmoon/products_selection_weights_adding/' . $products_id . '/error?&page=' . $page);
        }
    }

    //----------------------Quality tables simhadri ductile properties  Weights code-------------------------------------------
    public function quality_tables_simhadri_ductile_properties($products_id) {


        $this->load->view('cmoon/includes/header', $this->data);



        $data["products"] = $this->cmoon_model->get_cms('products', $products_id);



        $data['perpage'] = 15;

        $total_records = count($this->cmoon_model->get_result_cat('quality_tables_simhadri_ductile_properties', $products_id, 'products_id'));

        $pagination = my_pagination_cmoon('cmoon/quality_tables_simhadri_ductile_properties/' . $products_id . ' ' . '', $data['perpage'], $total_records);

        $data['pagination'] = $pagination['pagination'];

        $data['values'] = $this->cmoon_model->get_result_cat('quality_tables_simhadri_ductile_properties', $products_id, 'products_id', $data['perpage']);

        $this->load->view('cmoon/quality_tables_simhadri_ductile_properties', $data);

        $this->load->view('cmoon/includes/footer');
    }

    public function quality_tables_simhadri_ductile_properties_adding($products_id = '', $id = '') {
        if ($this->input->post()) {
            $form_data = $this->input->post();
            $form_data['p_link'] = $this->permalink($this->input->post('min_wt'));
            if ($id != '') {
                $del_existing_img = $this->cmoon_model->get_single_row_by_id('quality_tables_simhadri_ductile_properties', $id);


                $result = $this->cmoon_model->update('quality_tables_simhadri_ductile_properties', $id, $form_data);
                if ($result == true) {
                    redirect('cmoon/quality_tables_simhadri_ductile_properties/' . $form_data['products_id'] . '/Success');
                } else {
                    redirect('cmoon/quality_tables_simhadri_ductile_properties_adding/' . $form_data['products_id'] . '/error');
                }
            } else {


                $result = $this->cmoon_model->insert('quality_tables_simhadri_ductile_properties', $form_data);
                if ($result == true) {
                    redirect('cmoon/quality_tables_simhadri_ductile_properties/' . $form_data['products_id'] . '/Success');
                } else {
                    redirect('cmoon/quality_tables_simhadri_ductile_properties_adding/' . $form_data['products_id'] . '/error');
                }
            }
        } else {
            if ($id != '') {

//   echo "phani"; die;
                $this->load->view('cmoon/includes/header', $this->data);
                $data['values'] = $this->cmoon_model->get_row('quality_tables_simhadri_ductile_properties', $id);
                $data['products'] = $this->db->get('products')->result();
                $data['quality_tables_simhadri_ductile_properties'] = $this->cmoon_model->get_result_by_id('quality_tables_simhadri_ductile_properties', 'products_id', $data['values'][0]->products_id);

//   echo $this->db->last_query(); die;
                $this->load->view('cmoon/quality_tables_simhadri_ductile_properties_adding', $data);
                $this->load->view('cmoon/includes/footer');
            } else {
                $this->load->view('cmoon/includes/header', $this->data);
                $data['products'] = $this->db->get('products')->result();
                $this->load->view('cmoon/quality_tables_simhadri_ductile_properties_adding', $data);
                $this->load->view('cmoon/includes/footer');
            }
        }
    }

    public function deleate_quality_tables_simhadri_ductile_properties($products_id, $id, $page) {
        $del_existing_img = $this->cmoon_model->get_single_row_by_id('quality_tables_simhadri_ductile_properties', $id);
        unlink("cmoon_images/" . $del_existing_img->pdf);
        if ($page == 'undefined') {
            $page = '';
        }
        $result = $this->cmoon_model->delete('quality_tables_simhadri_ductile_properties', $id);
        if ($result == true) {
            redirect('cmoon/quality_tables_simhadri_ductile_properties/' . $products_id . '/dSuccess?&page=' . $page);
        } else {
            redirect('cmoon/quality_tables_simhadri_ductile_properties_adding/' . $products_id . '/error?&page=' . $page);
        }
    }

    //----------------------Quality tables Physical properties  Weights code-------------------------------------------
    public function quality_tables_physical_properties($products_id) {


        $this->load->view('cmoon/includes/header', $this->data);



        $data["products"] = $this->cmoon_model->get_cms('products', $products_id);



        $data['perpage'] = 15;

        $total_records = count($this->cmoon_model->get_result_cat('quality_tables_physical_properties', $products_id, 'products_id'));

        $pagination = my_pagination_cmoon('cmoon/quality_tables_physical_properties/' . $products_id . ' ' . '', $data['perpage'], $total_records);

        $data['pagination'] = $pagination['pagination'];

        $data['values'] = $this->cmoon_model->get_result_cat('quality_tables_physical_properties', $products_id, 'products_id', $data['perpage']);

        $this->load->view('cmoon/quality_tables_physical_properties', $data);

        $this->load->view('cmoon/includes/footer');
    }

    public function quality_tables_physical_properties_adding($products_id = '', $id = '') {
        if ($this->input->post()) {
            $form_data = $this->input->post();
            $form_data['p_link'] = $this->permalink($this->input->post('standard'));
            if ($id != '') {
                $del_existing_img = $this->cmoon_model->get_single_row_by_id('quality_tables_physical_properties', $id);


                $result = $this->cmoon_model->update('quality_tables_physical_properties', $id, $form_data);
                if ($result == true) {
                    redirect('cmoon/quality_tables_physical_properties/' . $form_data['products_id'] . '/Success');
                } else {
                    redirect('cmoon/quality_tables_physical_properties_adding/' . $form_data['products_id'] . '/error');
                }
            } else {


                $result = $this->cmoon_model->insert('quality_tables_physical_properties', $form_data);
                if ($result == true) {
                    redirect('cmoon/quality_tables_physical_properties/' . $form_data['products_id'] . '/Success');
                } else {
                    redirect('cmoon/quality_tables_physical_properties_adding/' . $form_data['products_id'] . '/error');
                }
            }
        } else {
            if ($id != '') {

//   echo "phani"; die;
                $this->load->view('cmoon/includes/header', $this->data);
                $data['values'] = $this->cmoon_model->get_row('quality_tables_physical_properties', $id);
                $data['products'] = $this->db->get('products')->result();
                $data['quality_tables_physical_properties'] = $this->cmoon_model->get_result_by_id('quality_tables_physical_properties', 'products_id', $data['values'][0]->products_id);

//   echo $this->db->last_query(); die;
                $this->load->view('cmoon/quality_tables_physical_properties_adding', $data);
                $this->load->view('cmoon/includes/footer');
            } else {
                $this->load->view('cmoon/includes/header', $this->data);
                $data['products'] = $this->db->get('products')->result();
                $this->load->view('cmoon/quality_tables_physical_properties_adding', $data);
                $this->load->view('cmoon/includes/footer');
            }
        }
    }

    public function deleate_quality_tables_physical_properties($products_id, $id, $page) {
        $del_existing_img = $this->cmoon_model->get_single_row_by_id('quality_tables_physical_properties', $id);
        unlink("cmoon_images/" . $del_existing_img->pdf);
        if ($page == 'undefined') {
            $page = '';
        }
        $result = $this->cmoon_model->delete('quality_tables_physical_properties', $id);
        if ($result == true) {
            redirect('cmoon/quality_tables_physical_properties/' . $products_id . '/dSuccess?&page=' . $page);
        } else {
            redirect('cmoon/quality_tables_physical_properties_adding/' . $products_id . '/error?&page=' . $page);
        }
    }

    //----------------------Quality tables Chemical properties  Weights code-------------------------------------------
    public function quality_tables_chemical_properties($products_id) {


        $this->load->view('cmoon/includes/header', $this->data);



        $data["products"] = $this->cmoon_model->get_cms('products', $products_id);



        $data['perpage'] = 15;

        $total_records = count($this->cmoon_model->get_result_cat('quality_tables_chemical_properties', $products_id, 'products_id'));

        $pagination = my_pagination_cmoon('cmoon/quality_tables_chemical_properties/' . $products_id . ' ' . '', $data['perpage'], $total_records);

        $data['pagination'] = $pagination['pagination'];

        $data['values'] = $this->cmoon_model->get_result_cat('quality_tables_chemical_properties', $products_id, 'products_id', $data['perpage']);

        $this->load->view('cmoon/quality_tables_chemical_properties', $data);

        $this->load->view('cmoon/includes/footer');
    }

    public function quality_tables_chemical_properties_adding($products_id = '', $id = '') {
        if ($this->input->post()) {
            $form_data = $this->input->post();
            $form_data['p_link'] = $this->permalink($this->input->post('is'));
            if ($id != '') {
                $del_existing_img = $this->cmoon_model->get_single_row_by_id('quality_tables_chemical_properties', $id);


                $result = $this->cmoon_model->update('quality_tables_chemical_properties', $id, $form_data);
                if ($result == true) {
                    redirect('cmoon/quality_tables_chemical_properties/' . $form_data['products_id'] . '/Success');
                } else {
                    redirect('cmoon/quality_tables_chemical_properties_adding/' . $form_data['products_id'] . '/error');
                }
            } else {


                $result = $this->cmoon_model->insert('quality_tables_chemical_properties', $form_data);
                if ($result == true) {
                    redirect('cmoon/quality_tables_chemical_properties/' . $form_data['products_id'] . '/Success');
                } else {
                    redirect('cmoon/quality_tables_chemical_properties_adding/' . $form_data['products_id'] . '/error');
                }
            }
        } else {
            if ($id != '') {

//   echo "phani"; die;
                $this->load->view('cmoon/includes/header', $this->data);
                $data['values'] = $this->cmoon_model->get_row('quality_tables_chemical_properties', $id);
                $data['products'] = $this->db->get('products')->result();
                $data['quality_tables_physical_properties'] = $this->cmoon_model->get_result_by_id('quality_tables_chemical_properties', 'products_id', $data['values'][0]->products_id);

//   echo $this->db->last_query(); die;
                $this->load->view('cmoon/quality_tables_chemical_properties_adding', $data);
                $this->load->view('cmoon/includes/footer');
            } else {
                $this->load->view('cmoon/includes/header', $this->data);
                $data['products'] = $this->db->get('products')->result();
                $this->load->view('cmoon/quality_tables_chemical_properties_adding', $data);
                $this->load->view('cmoon/includes/footer');
            }
        }
    }

    public function deleate_quality_tables_chemical_properties($products_id, $id, $page) {
        $del_existing_img = $this->cmoon_model->get_single_row_by_id('quality_tables_chemical_properties', $id);
        unlink("cmoon_images/" . $del_existing_img->pdf);
        if ($page == 'undefined') {
            $page = '';
        }
        $result = $this->cmoon_model->delete('quality_tables_chemical_properties', $id);
        if ($result == true) {
            redirect('cmoon/quality_tables_chemical_properties/' . $products_id . '/dSuccess?&page=' . $page);
        } else {
            redirect('cmoon/quality_tables_chemical_properties_adding/' . $products_id . '/error?&page=' . $page);
        }
    }

// -------------------------  Dealers city code  -----------------------------------

    public function dealers_city() {
        $this->load->view('cmoon/includes/header', $this->data);

        $data['perpage'] = 15;
        $total_records = count($this->cmoon_model->get('dealers_city'));
        $pagination = my_pagination_cmoon('cmoon/dealers_city' . '', $data['perpage'], $total_records);
        $data['pagination'] = $pagination['pagination'];

        $data['values'] = $this->cmoon_model->get('dealers_city', $data['perpage']);
        $this->load->view('cmoon/dealers_city', $data);
        $this->load->view('cmoon/includes/footer');
    }

    public function dealers_city_adding($id = '') {
        if ($this->input->post()) {
            $form_data = $this->input->post();


            if ($id != '') {
                $del_existing_img = $this->cmoon_model->get_single_row_by_id('dealers_city', $id);
                if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/" . $del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }
                $result = $this->cmoon_model->update('dealers_city', $id, $form_data);
                if ($result == true) {
                    redirect('cmoon/dealers_city/Success');
                } else {
                    redirect('cmoon/dealers_city_adding/error');
                }
            } else {
                if ($_FILES['image']['name'] != '') {
                    $form_data['image'] = $this->upload_file('image');
                }
                $result = $this->cmoon_model->insert('dealers_city', $form_data);
                if ($result == true) {
                    redirect('cmoon/dealers_city/Success');
                } else {
                    redirect('cmoon/dealers_city_adding/error');
                }
            }
        } else {

            if ($id != '') {
                $this->load->view('cmoon/includes/header', $this->data);
                $data['values'] = $this->cmoon_model->get_row('dealers_city', $id);
                $this->load->view('cmoon/dealers_city_adding', $data);
                $this->load->view('cmoon/includes/footer');
            } else {
                $this->load->view('cmoon/includes/header', $this->data);
                $data['values'] = $this->cmoon_model->get('dealers_city');
                $this->load->view('cmoon/dealers_city_adding', $data);
                $this->load->view('cmoon/includes/footer');
            }
        }
    }

    public function deleate_dealers_city($id) {
        $del_existing_img = $this->cmoon_model->get_single_row_by_id('dealers_city', $id);
        unlink("cmoon_images/" . $del_existing_img->image);
        $result = $this->cmoon_model->delete('dealers_city', $id);
        if ($result == true) {
            redirect('cmoon/dealers_city/dSuccess');
        } else {
            redirect('cmoon/dealers_city_adding/error');
        }
    }

// -------------------------  Dealers city code  -----------------------------------

    public function products_title() {

        $this->load->view('cmoon/includes/header', $this->data);

        $data['perpage'] = 15;
        $total_records = count($this->cmoon_model->get('products_title'));

        $pagination = my_pagination_cmoon('cmoon/products_title' . '', $data['perpage'], $total_records);

        $data['pagination'] = $pagination['pagination'];

        $data['values'] = $this->cmoon_model->get('products_title', $data['perpage']);

        $this->load->view('cmoon/products_title', $data);

        $this->load->view('cmoon/includes/footer');
    }

    public function products_title_adding($id = '') {
        if ($this->input->post()) {
            $form_data = $this->input->post();


            if ($id != '') {
                $del_existing_img = $this->cmoon_model->get_single_row_by_id('products_title', $id);
                if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/" . $del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }
                $result = $this->cmoon_model->update('products_title', $id, $form_data);
                if ($result == true) {
                    redirect('cmoon/products_title/Success');
                } else {
                    redirect('cmoon/products_title_adding/error');
                }
            } else {
                if ($_FILES['image']['name'] != '') {
                    $form_data['image'] = $this->upload_file('image');
                }
                $result = $this->cmoon_model->insert('products_title', $form_data);
                if ($result == true) {
                    redirect('cmoon/products_title/Success');
                } else {
                    redirect('cmoon/products_title_adding/error');
                }
            }
        } else {

            if ($id != '') {
                $this->load->view('cmoon/includes/header', $this->data);
                $data['values'] = $this->cmoon_model->get_row('products_title', $id);
                $this->load->view('cmoon/products_title_adding', $data);
                $this->load->view('cmoon/includes/footer');
            } else {
                $this->load->view('cmoon/includes/header', $this->data);
                $data['values'] = $this->cmoon_model->get('products_title');
                $this->load->view('cmoon/products_title_adding', $data);
                $this->load->view('cmoon/includes/footer');
            }
        }
    }

    public function deleate_products_title($id) {
        $del_existing_img = $this->cmoon_model->get_single_row_by_id('products_title', $id);
        unlink("cmoon_images/" . $del_existing_img->image);
        $result = $this->cmoon_model->delete('products_title', $id);
        if ($result == true) {
            redirect('cmoon/products_title/dSuccess');
        } else {
            redirect('cmoon/products_title_adding/error');
        }
    }

// ------------------------------------    About us code
    public function about_us() {
        if ($this->input->post()) {

            $del_existing_img = $this->cmoon_model->get_single_row_by_id('manage_uploads', $id);
            $form_data = $this->input->post();
            if ($_FILES['image']['name'] != '') {
                unlink("cmoon_images/" . $del_existing_img->image);
                $form_data['image'] = $this->upload_file('image');
            }
            if ($_FILES['mimage1']['name'] != '') {
                unlink("cmoon_images/" . $del_existing_img->mimage1);
                $form_data['mimage1'] = $this->upload_file('mimage1');
            }
            if ($_FILES['mimage2']['name'] != '') {
                unlink("cmoon_images/" . $del_existing_img->mimage2);
                $form_data['mimage2'] = $this->upload_file('mimage2');
            }
            $result = $this->cmoon_model->update_cms('about_us', '1', $form_data);
            if ($result == true) {
                redirect('cmoon/about_us/Success');
            } else {
                redirect('cmoon/about_us/error');
            }
        } else {
            $this->load->view('cmoon/includes/header', $this->data);
            $data['row'] = $this->cmoon_model->get_cms('about_us', '1');
            $this->load->view('cmoon/about_us', $data);
            $this->load->view('cmoon/includes/footer');
        }
    }

// ------------------------------------    site settings code    -----------------------------------

    public function site_settings() {
        if ($this->input->post()) {

            $del_existing_img = $this->cmoon_model->get_single_row_by_id('manage_uploads', $id);
            $form_data = $this->input->post();
            if ($_FILES['site_logo']['name'] != '') {
                unlink("cmoon_images/" . $del_existing_img->site_logo);
                $form_data['site_logo'] = $this->upload_file('site_logo');
            }
            if ($_FILES['site_favicon']['name'] != '') {
                unlink("cmoon_images/" . $del_existing_img->site_favicon);
                $form_data['site_favicon'] = $this->upload_file('site_favicon');
            }
            if ($_FILES['footer_logo']['name'] != '') {
                unlink("cmoon_images/" . $del_existing_img->footer_logo);
                $form_data['footer_logo'] = $this->upload_file('footer_logo');
            }
            $result = $this->cmoon_model->update_cms('site_settings', '1', $form_data);
            if ($result == true) {
                redirect('cmoon/site_settings/Success');
            } else {
                redirect('cmoon/site_settings/error');
            }
        } else {
            $this->load->view('cmoon/includes/header', $this->data);
            $data['row'] = $this->cmoon_model->get_cms('site_settings', '1');
            $this->load->view('cmoon/site_settings', $data);
            $this->load->view('cmoon/includes/footer');
        }
    }

// --------------------------------------     social links code    ------------------------------------------

    public function social_links() {
        if ($this->input->post()) {
            $form_data = $this->input->post();
            $result = $this->cmoon_model->update_cms('social_links', '1', $form_data);
            if ($result == true) {
                redirect('cmoon/social_links/Success');
            } else {
                redirect('cmoon/social_links/error');
            }
        } else {
            $this->load->view('cmoon/includes/header', $this->data);
            $data['row'] = $this->cmoon_model->get_cms('social_links', '1');
            $this->load->view('cmoon/social_links', $data);
            $this->load->view('cmoon/includes/footer');
        }
    }

// ----------------------------------------------  Change password code    -------------------------------------------

    public function change_password() {
        if ($this->input->post()) {
// $form_data = $this->input->post();
            $old_password = sha1($this->input->post('old_password'));
            $row_there = $this->cmoon_model->get_psw('log_in_cr', '1', $old_password);
            if ($row_there == 1) {
                $new_password = sha1($this->input->post('new_password'));
                $result = $this->cmoon_model->update_psw('log_in_cr', '1', $new_password);
                if ($result == true) {
                    redirect('cmoon/change_password/psw_Success');
                } else {
                    redirect('cmoon/change_password/psw_error');
                }
            } else {
                redirect('cmoon/change_password/psw_doesnot_match');
            }
        } else {
            $this->load->view('cmoon/includes/header', $this->data);
            $data['row'] = $this->cmoon_model->get_cms('log_in_cr', '1');
            $this->load->view('cmoon/change_password', $data);
            $this->load->view('cmoon/includes/footer');
        }
    }

// ----------------------------------------------   manage_uploads code    ------------------------
    public function manage_uploads() {
        $this->load->view('cmoon/includes/header', $this->data);
        $data['values'] = $this->cmoon_model->get('manage_uploads');
        $this->load->view('cmoon/manage_uploads', $data);
        $this->load->view('cmoon/includes/footer');
    }

    public function manage_uploads_adding($id = '') {
        if ($_FILES['image']['name'] != '') {
            $form_data = $this->input->post();
            if ($id != '') {

                $del_existing_img = $this->cmoon_model->get_single_row_by_id('manage_uploads', $id);

                if ($_FILES['image']['name'] != '') {
                    unlink("cmoon_images/" . $del_existing_img->image);
                    $form_data['image'] = $this->upload_file('image');
                }
                $result = $this->cmoon_model->update('manage_uploads', $id, $form_data);
                if ($result == true) {
                    redirect('cmoon/manage_uploads/Success');
                } else {
                    redirect('cmoon/manage_uploads_adding/error');
                }
            } else {
                if ($_FILES['image']['name'] != '') {
                    $form_data['image'] = $this->upload_file('image');
                }
                $result = $this->cmoon_model->insert('manage_uploads', $form_data);
                if ($result == true) {
                    redirect('cmoon/manage_uploads/Success');
                } else {
                    redirect('cmoon/manage_uploads_adding/error');
                }
            }
        } else {
            if ($id != '') {
                $this->load->view('cmoon/includes/header', $this->data);
                $data['values'] = $this->cmoon_model->get_row('manage_uploads', $id);
                $this->load->view('cmoon/manage_uploads_adding', $data);
                $this->load->view('cmoon/includes/footer');
            } else {
                $this->load->view('cmoon/includes/header', $this->data);
                $data['values'] = $this->cmoon_model->get('manage_uploads');
                $this->load->view('cmoon/manage_uploads_adding', $data);
                $this->load->view('cmoon/includes/footer');
            }
        }
    }

    public function deleate_manage_uploads($id) {
        $del_existing_img = $this->cmoon_model->get_single_row_by_id('manage_uploads', $id);
        unlink("cmoon_images/" . $del_existing_img->image);

        $result = $this->cmoon_model->delete('manage_uploads', $id);
        if ($result == true) {
            redirect('cmoon/manage_uploads/dSuccess');
        } else {
            redirect('cmoon/manage_uploads_adding/error');
        }
    }

// ---------------------------------------------------------------------     Logs code    -------------------------------------------

    public function logs() {
        $this->load->view('cmoon/includes/header', $this->data);
        $this->load->view('cmoon/includes/header');
        $data['values'] = $this->cmoon_model->get('logs');
        $this->load->view('cmoon/logs', $data);
        $this->load->view('cmoon/includes/footer');
    }

// ----------------------------------------------- plink generator  -------------------------------------------------------------
// $post_title = "Welcome to php exampless";
// echo $permalink = permalink($post_title);

    private function permalink($string) {
        $string = str_replace(" ", "-", $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

// ------------------------------------ Image upload code ----------------------------------------------------



    private function upload_file($file_name, $path = null) {

        $upload_path1 = "cmoon_images" . $path;

        $config1['upload_path'] = $upload_path1;

        $config1['allowed_types'] = "gif|jpg|png|jpeg";

        $config1['max_size'] = "100480000";

        $img_name1 = strtolower($_FILES[$file_name]['name']);

        $img_name1 = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);

        $config1['file_name'] = date("YmdHis") . rand(0, 999) . "_" . $img_name1;

        $this->load->library('upload', $config1);

        $this->upload->initialize($config1);

        $this->upload->do_upload($file_name);

        $fileDetailArray1 = $this->upload->data();

        return $fileDetailArray1['file_name'];
    }

// ----------------------------------------------- pdf upload code --------------------------------------------------



    private function upload_file_brochure($file_name, $path = null) {



        $upload_path1 = "cmoon_images" . $path;

        $config1['upload_path'] = $upload_path1;

        $config1['allowed_types'] = "pdf";

        $config1['max_size'] = "100480000";

        $img_name1 = strtolower($_FILES[$file_name]['name']);

        $img_name1 = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);

        $config1['file_name'] = date("YmdHis") . rand(0, 999) . "_" . $img_name1;

        $this->load->library('upload', $config1);

        $this->upload->initialize($config1);

        $this->upload->do_upload($file_name);

        $fileDetailArray1 = $this->upload->data();

        return $fileDetailArray1['file_name'];
    }

//--------------------- Back up db--------------//

    public function backup_db() {
        $site_name = $this->data["site_settings"]->site_name;
        $this->load->dbutil();
        $prefs = array(
            'format' => 'zip',
            'filename' => $site_name,
        );
        $backup = &$this->dbutil->backup($prefs);
        $db_name = $site_name . '-' . date("Y-m-d-H-i-s") . '.zip';
        $save = 'C:/Users/cmoon/Downloads/' . $db_name;
        $this->load->helper('file');
        write_file($save, $backup);
        $this->load->helper('download');
        force_download($db_name, $backup);
    }

}
