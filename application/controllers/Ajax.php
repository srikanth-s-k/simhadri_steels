<?php  defined('BASEPATH') or exit('No direct script access allowed');

class Ajax extends CI_Controller

{

    public $data;
    public function __construct()

    {
        parent::__construct();
        // $this->load->model('login_model','',True);
        $this->load->model('site_model', '', true);
        $this->load->model('cmoon_model', '', true);
        $this->load->helper('cookie');


    }

    //------------------------------------------------------------  pages --------------------------------------------------------------

    // public function index()
    // {
    //     $this->load->view('includes/header', $this->data);
    //     $data['home_banners'] = $this->site_model->get('home_banners');
    //     $data["cms_pages1"] = $this->site_model->get_cms('cms_pages', '1');
    //     $data["cms_pages2"] = $this->site_model->get_cms('cms_pages', '2');
    //     $data["cms_pages3"] = $this->site_model->get_cms('cms_pages', '3');
    //     $data["cms_pages7"] = $this->site_model->get_cms('cms_pages', '7');
    //     $data["cms_pages8"] = $this->site_model->get_cms('cms_pages', '8');
    //     $data['testimonials_limit'] = $this->site_model->get_by_limit_desc('testimonials','6');
    //     $data['property_listings'] = $this->site_model->get_by_limit_desc('property_listings','12');
    //     $data['testimonials'] = $this->site_model->get('testimonials');
    //     $data['landlords'] = $this->site_model->get('landlords');
    //     $data['our_team'] = $this->site_model->get('our_team');
    //     $this->load->view('index', $data);
    //     $this->load->view('includes/footer');
    // }

    public function get_districts()
    {
        $states_id = $this->input->post('state_id');
        
        
        $districts = $this->cmoon_model->get_result_cat('districts', $states_id, 'states_id');
                echo "<option value=''>Select </option>";
            foreach ($districts as $districts_row) {
                echo "<option value='$districts_row->id' >$districts_row->heading</option>";
            }
            
    }


    public function setlocation()
    {
       if($this->input->post('set_location'))
        {
            
           $data=$this->session->set_userdata('current_city',$this->input->post('set_location'));         
         echo  $this->session->userdata('current_city');
        }
    }




    public function set_cookie()
    {
       if($this->input->post('project_id') != 'No')
        {


    $data=$this->input->cookie('projects').",".$this->input->post('project_id');

    $array_of_cookie=explode(",",$data);

    $array = array_unique($array_of_cookie);

    foreach($array as $key => $link) 
        { 
            if($link === '') 
            { 
                unset($array[$key]); 
            } 
        } 

    $data=implode(",", $array);

    $cookie_projects_ids = array(
       'name'   => 'projects',
       'value'  => $data,
       'expire' => '31104000'
    );

        $this->input->set_cookie($cookie_projects_ids); 

        

        $this->data['projects_cookie'] = $data; 
        $this->data['projects_cookie_array'] = explode(",", $data); 

            $array=[];
            $array["fav_projects1"] = $this->site_model->get_by_where_in_id_asc_order_by('projects',$this->data['projects_cookie_array'],'id','id');
            foreach($array['fav_projects1']  as   $row){
                $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
                $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');
                $row->price_in_short = $this->convertCurrencyforcookie($row->unit_starting_price)." to ".$this->convertCurrencyforcookie($row->unit_ending_price);  
            } 

            $this->data['fav_projects'] = $array["fav_projects1"];
            echo json_encode($this->data['fav_projects']);

        }else{



        $this->data['projects_cookie'] = $this->input->cookie('projects'); 
        $this->data['projects_cookie_array'] = explode(",", $this->input->cookie('projects')); 

            $array=[];
            $array["fav_projects1"] = $this->site_model->get_by_where_in_id_asc_order_by('projects',$this->data['projects_cookie_array'],'id','id');
            foreach($array['fav_projects1']  as   $row){
                $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
                $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');
                $row->price_in_short = $this->convertCurrencyforcookie($row->unit_starting_price)." to ".$this->convertCurrencyforcookie($row->unit_ending_price);
            } 

            $this->data['fav_projects'] = $array["fav_projects1"];

        
            echo json_encode($this->data['fav_projects']);

        }
    }
















    public function set_cookie_delete()
    {
       if($this->input->post('project_id'))
        {


    // $data=$this->input->cookie('projects').",".$this->input->post('project_id');
    $data=$this->input->cookie('projects');

    $array_of_cookie=explode(",",$data);

    // $array = array_unique($array_of_cookie);

    foreach($array_of_cookie as $key => $link) 
        { 
            if($link == $this->input->post('project_id')) 
            { 
                unset($array_of_cookie[$key]); 
            } 
        } 

    $data=implode(",", $array_of_cookie);

    $cookie_projects_ids = array(
       'name'   => 'projects',
       'value'  => $data,
       'expire' => '31104000'
    );

        $this->input->set_cookie($cookie_projects_ids); 

        $this->data['projects_cookie'] = $data; 
        $this->data['projects_cookie_array'] = explode(",", $data); 

            $array=[];
            $array["fav_projects1"] = $this->site_model->get_by_where_in_id_asc_order_by('projects',$this->data['projects_cookie_array'],'id','id');
            foreach($array['fav_projects1']  as   $row){
                $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
                $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');
                $row->price_in_short = $this->convertCurrencyforcookie($row->unit_starting_price)." to ".$this->convertCurrencyforcookie($row->unit_ending_price);  
            } 

            $this->data['fav_projects'] = $array["fav_projects1"];
            echo json_encode($this->data['fav_projects']);

        }
    }





    public function auto_complete()
    {
      
        if($this->input->post('search_words')){


            $city_result = $this->site_model->get_like('city', 'city', $this->input->post('search_words'));

            foreach($city_result as $row){
                $projects_id[]=$row->projects_id;
            }

            $location_result = $this->site_model->get_like('location', 'location', $this->input->post('search_words'));

            foreach($location_result as $row){
                $projects_id[]=$row->projects_id;
            }

            $about_builder_result = $this->site_model->get_like('about_builder', 'heading', $this->input->post('search_words'));

            foreach($about_builder_result as $row){
                $projects_id[]=$row->projects_id;
            }

            $projects_flats_details_result = $this->site_model->get_like('projects_flats_details', 'heading', $this->input->post('search_words'));//add group by heading

            foreach($projects_flats_details_result as $row){
                $projects_id[]=$row->projects_id;
            }

            $projects_floor_plans_result = $this->site_model->get_like('projects_floor_plans', 'heading', $this->input->post('search_words'));//add group by heading

            foreach($projects_floor_plans_result as $row){
                $projects_id[]=$row->projects_id;
            }
            
            $result1 = $this->site_model->auto_complete_model($this->data['current_city_id'], $this->input->post('search_words'),$projects_id);

            
            $search_array = [];
            foreach ($result1 as $row) {
                     $location =$this->site_model->get_by_id('location',$row->location_id,'id');
                     $search_array[] = array(
                        'project_name' => $row->project_name,
                        'location' => $location[0]->location,
                        'p_link' => $row->p_link ); 
                     // $search_array[]= $row->project_name;  
 
               } 
     
                echo json_encode($search_array);
            }
        }
     


function convertCurrencyforcookie($number)
{
  
    // Convert Price to Crores or Lakhs or Thousands
    $length = strlen($number);
    $currency = '';

    if($length == 4 || $length == 5)
    {
        // Thousand
        $number = $number / 1000;
        $number = round($number,2);
        $ext = "Thousand";
        $currency = $number." ".$ext;
    }
    elseif($length == 6 || $length == 7)
    {
        // Lakhs
        $number = $number / 100000;
        $number = round($number,2);
        $ext = "Lac";
        $currency = $number." ".$ext;

    }
    elseif($length == 8 || $length == 9)
    {
        // Crores
        $number = $number / 10000000;
        $number = round($number,2);
        $ext = "Cr";
        $currency = $number.' '.$ext;
    }

    return $currency;
}

// Call the function with a number input 
// echo convertCurrency(5000);



    // public function setlocationofferzone()
    // {
    //    if($this->input->post('set_location_offerzone'))
    //     {

    //         echo $this->input->post('set_location_offerzone');
    //        $data=$this->session->set_userdata('current_city',$this->input->post('set_location_offerzone'));         
    //       // echo $this->session->userdata('current_city');
    //     }
    // }





        function cookie_status()
        {
            $data=$this->input->cookie('projects');
            $array_of_cookie=explode(",",$data);


           $projects = $this->site_model->get('projects');

                $get_cookie_status=[];
                foreach($projects as $projects_row){

                        if(in_array($projects_row->id,$array_of_cookie)){                     
                            $get_cookie_status[] = array(
                            'all_project_id' => $projects_row->id,
                            'cookie_status' => 'true' ); 
                        }else{ 
                            $get_cookie_status[] = array(
                            'all_project_id' => $projects_row->id,
                            'cookie_status' => 'false' );            
                        } 

            }

            echo json_encode($get_cookie_status);

        }


        function autodetect_location()
        {
            $getloc = json_decode(file_get_contents("http://ipinfo.io/$ip_address"));

                    $geo_city = strtolower($getloc->city);

                    if(count($this->site_model->get_by_p_link('city',$geo_city)) > 0){
                         $this->session->set_userdata('current_city',$geo_city);
                         echo "true";
                    }
                

        }


    // public function recently_viewed()
    // {
    //    if($this->input->post('project_id'))
    //     {

    //     $data=$this->input->cookie('recently_viewed').",".$this->input->post('project_id');

    //     $array_of_cookie=explode(",",$data);

    //     $array = array_unique($array_of_cookie);

    //     foreach($array as $key => $link) 
    //         { 
    //             if($link === '') 
    //             { 
    //                 unset($array[$key]); 
    //             } 
    //         } 

    //     $last_10_ofarray = array_slice($array, -10, 10, true); 
        
    //     $data=implode(",", $last_10_ofarray);

    //     $cookie_recently_viewed_project_ids = array(
    //        'name'   => 'recently_viewed_projects',
    //        'value'  => $data,
    //        'expire' => '31104000'
    //     );
    //         $this->input->set_cookie($cookie_recently_viewed_project_ids);         
    //     } 
    // }


}