<?php

defined('BASEPATH') or exit('No direct script access allowed');
class view extends CI_Controller

{
    public $data;

    public function __construct()

    {
        parent::__construct();

        $this->load->helper('cookie');
        // $this->load->model('login_model','',True);
        $this->load->model('site_model', '', true);
        $this->load->model('cmoon_model', '', true);

        $this->data["site_settings"] = $this->site_model->get_cms('site_settings', '1');
        $this->data["social_links"]  = $this->site_model->get_cms('social_links', '1');
        $this->data["city_index_dropdown"]  = $this->site_model->get('city');
        $this->data["city_index"]  = $this->site_model->get_by_col('city','yes','show_in_main_popup');
        $this->data["cms_pages17"] = $this->site_model->get_cms('cms_pages', '17');
        $this->data["industry_news"]  = $this->site_model->get('industry_news');
        $this->data["cms_pages11"] = $this->site_model->get_cms('cms_pages', '11');
        $this->data["cms_pages13"] = $this->site_model->get_cms('cms_pages', '13');


        $this->data["cms_pages20"] = $this->site_model->get_cms('cms_pages', '20');
        $this->data["cms_pages21"] = $this->site_model->get_cms('cms_pages', '21');
        $this->data["cms_pages22"] = $this->site_model->get_cms('cms_pages', '22');
        $this->data["cms_pages23"] = $this->site_model->get_cms('cms_pages', '23');


        $this->data["real_estate_type_index"]  = $this->site_model->get('real_estate_type');
        $this->data['current_city'] = $this->session->userdata('current_city');

        $data["city"] = $this->site_model->get_by_p_link('city',$this->data['current_city']);
        $this->data['current_city_id'] = $data["city"]->id;

    $this->data['footer_locations'] = $this->site_model->get_by_id('location',$data["city"]->id,'city_id');
    $this->data['footer_zone'] = $this->site_model->get_by_id_asc_order_by('location',$data["city"]->id,'city_id','zone_link');

    
    $builders_ids = explode(",", $data["city"]->builders_id);
        foreach($builders_ids  as   $builders_id){
            $builders_in_current_city[] = $this->site_model->get_row_by_id('about_builder',$builders_id,'id');  
        }
        $this->data['footer_builders'] = $builders_in_current_city;


    $this->data['footer_projects_apartments'] = $this->site_model->get_by_id_desc_order_by_col2('projects',$data["city"]->id,'city_id','apartment','project_type','id');

    // echo "<pre>";
    // print_r($this->data['footer_projects_apartments']);
    // die();

    $this->data['footer_projects_villa'] = $this->site_model->get_by_id_desc_order_by_col2('projects',$data["city"]->id,'city_id','villa','project_type','id');

    // $this->data['footer_zone'] = $this->site_model->get_by_id_asc_order_by('location',$data["city"]->id,'city_id','zone_link');
        // $this->data['current_city'] = $this->session->userdata('current_city');


        $this->data['search_property_type'] = $this->site_model->projects_by_property_type('projects','property_type');
        $this->data['search_projects_flats_details'] = $this->site_model->projects_by_property_type('projects_flats_details','heading');
        $this->data['search_possession_year'] = $this->site_model->projects_by_property_type('projects','possession_year');
        $this->data['search_name_array'] = $this->site_model->projects_by_property_type('projects','possession_year');


        $this->data["country_istd_codes"] = $this->site_model->get_orderby_asc('country_istd_codes', 'heading');

    $currentURL = current_url(); //for simple URL
    $params = $_SERVER['QUERY_STRING']; //for parameters
    $this->data['fullURL'] = $currentURL . '?' . $params; //full URL with parameter


    if($this->input->cookie('projects') != ''){

        $this->data['projects_cookie'] = $this->input->cookie('projects'); //full URL with parameter
        $this->data['projects_cookie_array'] = explode(",", $this->input->cookie('projects')); //full URL with parameter
        // $this->data['projects_cookie_array_count'] = count($this->data['projects_cookie_array']);

            // $array=[];
            // $array["fav_projects1"] = $this->site_model->get_by_where_in_id_asc_order_by('projects',$this->data['projects_cookie_array'],'id','id');
            // foreach($array['fav_projects1']  as   $row){
            //     $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
            //     $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');  
            // } 

            // $this->data['fav_projects'] = $array["fav_projects1"];

    }
        // $this->data["education_services_index"]  = $this->site_model->get('education_services');
        // $this->data["medical_certified_products_index"]  = $this->site_model->get('medical_certified_products');
        // $this->data["community_products_index"]  = $this->site_model->get('community_products');
        // $this->data["import_export_services_index"]  = $this->site_model->get('import_export_services');
        // $this->data["electrical_automation_index"]  = $this->site_model->get('electrical_automation');
        // $this->data["smart_home_appliance_index"]  = $this->site_model->get('smart_home_appliance');
        // $this->data["testimonials"]  = $this->site_model->get('testimonials');
        // $this->data["applications_header"]  = $this->site_model->get('applications');
        // $this->data["branches_footer"]  = $this->site_model->get_by_limit('branches','2');
    }

    //------------------------------  pages ---------------------------------------------


    public function error()
    {
        
        $this->load->view('includes/header', $this->data);
        $this->load->view('error_page', $data);
        $this->load->view('includes/footer');
    }


    public function index()
    {
        if($this->data['current_city'] == ''){
                redirect('latest_projects');
            }

        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','1');
        $this->load->view('includes/main_header', $this->data);
        // $data['home_banners'] = $this->site_model->get('home_banners');
        $data["budget_form_field"] = $this->site_model->get_orderby_asc('budget_form_field', 'priority');

        $data["home_banners1"] = $this->site_model->get_cms('home_banners', '1');
        $data["home_banners2"] = $this->site_model->get_cms('home_banners', '6');
        $data["home_banners3"] = $this->site_model->get_cms('home_banners', '7');
        $data["home_banners4"] = $this->site_model->get_cms('home_banners', '8');
        $data["home_banners5"] = $this->site_model->get_cms('home_banners', '9');
        $data["home_banners6"] = $this->site_model->get_cms('home_banners', '10');
        $data["cms_pages12"] = $this->site_model->get_cms('cms_pages', '12');
        $data['testimonials'] = $this->site_model->get('testimonials');

        $data["city_id_fetch"] = $this->site_model->get_by_p_link('city',$this->data['current_city']);
        $city_id_fetch = $data["city_id_fetch"]->id;

$data['projects_apartment'] = $this->site_model->projects_by_type('projects','apartment','project_type',$city_id_fetch);
$data['projects_apartment_count'] = count($this->site_model->projects_by_type('projects','apartment','project_type',$city_id_fetch));
$data['projects_villa'] = $this->site_model->projects_by_type('projects','villa','project_type',$city_id_fetch);
$data['projects_villa_count'] = count($this->site_model->projects_by_type('projects','villa','project_type',$city_id_fetch));
        // $data["cms_pages7"] = $this->site_model->get_cms('cms_pages', '7');
        // $data["cms_pages8"] = $this->site_model->get_cms('cms_pages', '8');
        // $data['property_listings'] = $this->site_model->get_by_limit_desc('property_listings','12');
        // $data['testimonials'] = $this->site_model->get('testimonials');
        // $data['landlords'] = $this->site_model->get('landlords');
        // $data['our_team'] = $this->site_model->get('our_team');


        $recently_viewed_ids=$this->input->cookie('recently_viewed_projects');
        $recently_viewed_ids_array=explode(",",$recently_viewed_ids);

            $array=[];
            $array["recently_viewed1"] = $this->site_model->get_by_where_in_id_asc_order_by('projects',$recently_viewed_ids_array,'id','id');
            foreach($array['recently_viewed1']  as   $row){
                $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
                $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');
                $row->price_in_short = $this->convertCurrencyforcookie($row->unit_starting_price)." to ".$this->convertCurrencyforcookie($row->unit_ending_price);  
            } 

            $data['recently_viewed_projects_count'] = count($array["recently_viewed1"]);

            $data['recently_viewed_projects'] = $array["recently_viewed1"];


        $this->load->view('index', $data);
        $this->load->view('includes/main_footer');
    }
    
    
     public function error_page()
    {
        $this->load->view('includes/header', $this->data);
        $data["cms_pages1"] = $this->site_model->get_cms('cms_pages', '1');
        $data["cms_pages2"] = $this->site_model->get_cms('cms_pages', '2');
        $data["cms_pages3"] = $this->site_model->get_cms('cms_pages', '3');
        $data["cms_pages4"] = $this->site_model->get_cms('cms_pages', '4');
        $data["cms_pages5"] = $this->site_model->get_cms('cms_pages', '5');
        $this->load->view('error_page', $data);
        $this->load->view('includes/footer');
    }
    


    public function about()
    {

        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','6');
        $this->load->view('includes/header', $this->data);
        $data["budget_form_field"] = $this->site_model->get_orderby_asc('budget_form_field', 'priority');
        $data["cms_pages1"] = $this->site_model->get_cms('cms_pages', '1');
        $data["cms_pages2"] = $this->site_model->get_cms('cms_pages', '2');
        $data["cms_pages3"] = $this->site_model->get_cms('cms_pages', '3');
        $data["cms_pages4"] = $this->site_model->get_cms('cms_pages', '4');
        $data["cms_pages5"] = $this->site_model->get_cms('cms_pages', '5');
        $this->load->view('about', $data);
        $this->load->view('includes/footer');
    }


    public function terms_and_conditions()
    {
        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','10');
        $this->load->view('includes/header', $this->data);
        $data["cms_pages10"]  = $this->site_model->get_cms('cms_pages', '10');
        $this->load->view('terms_and_conditions', $data);
        $this->load->view('includes/footer');
    }

    public function privacy_policy()
    {
        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','9');
        $this->load->view('includes/header', $this->data);
        $data["cms_pages9"]  = $this->site_model->get_cms('cms_pages', '9');
        $this->load->view('privacy_policy', $data);
        $this->load->view('includes/footer');
    }

        public function home_loans()
    {


       //  echo "phani";
       //  echo "<br>";

       // echo  $this->input->cookie('projects_ids');

       //  die();
        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','3');
        $this->load->view('includes/header', $this->data);
        $data["cms_pages8"]  = $this->site_model->get_cms('cms_pages', '8');
        $data["bank_logos"]  = $this->site_model->get('bank_logos');
        $data["budget_form_field"] = $this->site_model->get_orderby_asc('budget_form_field', 'priority');
        $this->load->view('home_loans', $data);
        $this->load->view('includes/footer');
    }


        public function contact_us()
    {
        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','8');
        $this->load->view('includes/header', $this->data);
        $data["cms_pages7"]  = $this->site_model->get_cms('cms_pages', '7');
        $this->load->view('contact_us', $data);
        $this->load->view('includes/footer');
    }


        public function careers()
    {
        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','7');
        $this->load->view('includes/header', $this->data);
        $data["cms_pages6"]  = $this->site_model->get_cms('cms_pages', '6');
        $this->load->view('careers', $data);
        $this->load->view('includes/footer');
    }


        public function news()
    {
        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','4');
        $this->load->view('includes/header', $this->data);
        $data["cms_pages14"]  = $this->site_model->get_cms('cms_pages', '14');

        $data['perpage']       = 12;
        $total_records = count($this->site_model->get_pagination_desc('news'));
        $pagination = my_pagination('view/news' , $data['perpage'], $total_records);
        $data['pagination']  = $pagination['pagination'];

        $data["news"]  = $this->site_model->get_pagination_desc('news',$data['perpage']);

        $this->load->view('news', $data);
        $this->load->view('includes/footer');
    }



        public function news_details($p_link)
    {
        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','5');
        $this->load->view('includes/header', $this->data);
        $data["cms_pages14"]  = $this->site_model->get_cms('cms_pages', '14');
        $data["news"]  = $this->site_model->get_by_p_link('news',$p_link);
        $data["related_news"]  = $this->site_model->get_by_limit_desc('news','9');
        $this->load->view('news_details', $data);
        $this->load->view('includes/footer');
    }


        public function disclaimer()
    {
        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','12');
        $this->load->view('includes/header', $this->data);
        $data["cms_pages15"]  = $this->site_model->get_cms('cms_pages', '15');
        $this->load->view('disclaimer', $data);
        $this->load->view('includes/footer');
    }

        public function sub_page($p_link)
    {


        $this->data['page_meta_tags'] = $this->site_model->get_by_p_link('projects',$p_link);

        $recently_viewed_data=$this->data['page_meta_tags']->id.",".$this->input->cookie('recently_viewed_projects');
        $array_of_cookie=explode(",",$recently_viewed_data);
        $recently_viewed_cookies = array_unique($array_of_cookie);
        foreach($recently_viewed_cookies as $key => $link) 
            { 
                if($link === '') 
                { 
                    unset($recently_viewed_cookies[$key]); 
                } 
            } 
        $last_10_ofarray = array_slice($recently_viewed_cookies, -10, 10, true); 
        $recently_viewed_data=implode(",", $last_10_ofarray);

        $cookie_recently_viewed_project_ids = array(
           'name'   => 'recently_viewed_projects',
           'value'  => $recently_viewed_data,
           'expire' => '31104000'
        );
        
        $this->input->set_cookie($cookie_recently_viewed_project_ids);  



        $this->load->view('includes/header', $this->data);
        $data['projects'] = $this->site_model->get_by_p_link('projects',$p_link);
        $data['builder'] = $this->site_model->get_row_by_id('about_builder',$data['projects']->builder_id,'id');
        $data['city'] = $this->site_model->get_row_by_id('city',$data['projects']->city_id,'id');
        $data['location'] = $this->site_model->get_row_by_id('location',$data['projects']->location_id,'id');
        $data['projects_banners'] = $this->site_model->get_by_id('projects_banners',$data['projects']->id,'projects_id');
        $data['projects_floor_plans'] = $this->site_model->get_by_id('projects_floor_plans',$data['projects']->id,'projects_id');
        $data["amenities"]  = $this->site_model->get('amenities');
        $data['projects_amenities_count'] = count($this->site_model->get_by_id('projects_amenities',$data['projects']->id,'projects_id'));
        $data['projects_amenities'] = $this->site_model->get_by_id('projects_amenities',$data['projects']->id,'projects_id');

        $data['projects_specifications'] = $this->site_model->get_by_id('projects_specifications',$data['projects']->id,'projects_id');
        $data['projects_approved_banks'] = $this->site_model->get_by_id('projects_approved_banks',$data['projects']->id,'projects_id');

         $array= [];
         $array['projects_gallery'] = $this->site_model->get_by_id('projects_gallery',$data['projects']->id,'projects_id');
         foreach($array['projects_gallery']  as   $row){
            $row->images = $this->site_model->get_by_id('projects_gallery_photos',$row->id,'projects_gallery_id');  
        }
        $data['projects_gallery']=$array;
       
        $data['projects_videos'] = $this->site_model->get_by_id('projects_videos',$data['projects']->id,'projects_id');
        $data['projects_reasons_gallery'] = $this->site_model->get_by_id('projects_reasons_gallery',$data['projects']->id,'projects_id');



        // $data['similer_projects'] = $this->site_model->get_id_notequalto('projects',$data['projects']->id,'id',$data['projects']->project_type,'project_type',$data['projects']->city_id,$data['projects']->location_id);

    $similer_projects_location_count = count($this->site_model->get_id_notequalto('projects',$data['projects']->id,'id',$data['projects']->location_id,'location_id'));
    $similer_projects_city_count = count($this->site_model->get_id_notequalto('projects',$data['projects']->id,'id',$data['projects']->city_id,'city_id'));


    $final_less_amount=$data['projects']->unit_starting_price-((30/100)*$data['projects']->unit_starting_price);
    $final_more_amount=$data['projects']->unit_ending_price+((30/100)*$data['projects']->unit_ending_price);


    $similer_projects_price_count = count($this->site_model->get_id_notequalto_price('projects',$data['projects']->id,'id',$final_less_amount,$final_more_amount));

    if($similer_projects_condition_count > 0){
        $data['similer_projects'] = $this->site_model->get_id_notequalto('projects',$data['projects']->id,'id',$data['projects']->location_id,'location_id');
    }elseif($similer_projects_city_count > 0){
        $data['similer_projects'] = $this->site_model->get_id_notequalto('projects',$data['projects']->id,'id',$data['projects']->city_id,'city_id');
    }elseif($similer_projects_price_count > 0){
        $data['similer_projects']=$this->site_model->get_id_notequalto_price('projects',$data['projects']->id,'id',$final_less_amount,$final_more_amount);
    }else{
        $data['similer_projects']=$this->site_model->get_by_limit_desc('projects',10);
    }


        $this->load->view('sub_page', $data);
        $this->load->view('includes/footer');
    }


        public function micro_site($p_link)
    {
                // header codes
    $this->data['page_meta_tags'] = $this->site_model->get_by_p_link('projects',$p_link);
    $data["current_city"] = $this->data['current_city'];

    $data["city"] = $this->site_model->get_by_p_link('city',$this->data['current_city']);

    $data['footer_locations'] = $this->site_model->get_by_id('location',$data["city"]->id,'city_id');
    $data['footer_zone'] = $this->site_model->get_by_id_asc_order_by('location',$data["city"]->id,'city_id','zone_link');
    $builders_ids = explode(",", $data["city"]->builders_id);
        foreach($builders_ids  as   $builders_id){
            $builders_in_current_city[] = $this->site_model->get_row_by_id('about_builder',$builders_id,'id');  
        }
    $data['footer_builders'] = $builders_in_current_city;
    $data['footer_projects_apartments'] = $this->site_model->get_by_id_desc_order_by_col2('projects',$data["city"]->id,'city_id','apartment','project_type','id');
    $data['footer_projects_villa'] = $this->site_model->get_by_id_desc_order_by_col2('projects',$data["city"]->id,'city_id','villa','project_type','id');

        $data["cms_pages20"] = $this->site_model->get_cms('cms_pages', '20');
        $data["cms_pages21"] = $this->site_model->get_cms('cms_pages', '21');
        $data["cms_pages22"] = $this->site_model->get_cms('cms_pages', '22');
        $data["cms_pages23"] = $this->site_model->get_cms('cms_pages', '23');

        $data['micro_sub_meta_tags'] = $this->site_model->get_by_p_link('projects',$p_link);
        $data["site_settings"] = $this->site_model->get_cms('site_settings', '1');
        $data["social_links"]  = $this->site_model->get_cms('social_links', '1');
        $data["city_index"]  = $this->site_model->get('city');
        $data["cms_pages17"] = $this->site_model->get_cms('cms_pages', '17');
        $data["industry_news"]  = $this->site_model->get('industry_news');
        $data["cms_pages11"] = $this->site_model->get_cms('cms_pages', '11');
        $data["cms_pages13"] = $this->site_model->get_cms('cms_pages', '13');
        $data["real_estate_type_index"]  = $this->site_model->get('real_estate_type');
        $data['city_index'] = $this->site_model->get('city');
        $data['projects_flats_details_index'] = $this->site_model->get_orderby('projects_flats_details','heading');

        // page codes
        $data['projects'] = $this->site_model->get_by_p_link('projects',$p_link);
        $data['builder'] = $this->site_model->get_row_by_id('about_builder',$data['projects']->builder_id,'id');
        $data['city'] = $this->site_model->get_row_by_id('city',$data['projects']->city_id,'id');
        $data['location'] = $this->site_model->get_row_by_id('location',$data['projects']->location_id,'id');
        $data['projects_banners'] = $this->site_model->get_by_id('projects_banners',$data['projects']->id,'projects_id');
        $data['projects_floor_plans'] = $this->site_model->get_by_id('projects_floor_plans',$data['projects']->id,'projects_id');
        $data["amenities"]  = $this->site_model->get('amenities');
        // $data['projects_amenities'] = $this->site_model->get_by_id('projects_amenities',$data['projects']->id,'projects_id');
    $data['projects_micro_site_amenities_count'] = count($this->site_model->get_by_id('projects_micro_site_amenities',$data['projects']->id,'projects_id'));

        $data['projects_micro_site_amenities'] = $this->site_model->get_by_id('projects_micro_site_amenities',$data['projects']->id,'projects_id');
        $data['projects_specifications'] = $this->site_model->get_by_id('projects_specifications',$data['projects']->id,'projects_id');
        $data['projects_approved_banks'] = $this->site_model->get_by_id('projects_approved_banks',$data['projects']->id,'projects_id');

        $array= [];
         $array['projects_gallery'] = $this->site_model->get_by_id('projects_gallery',$data['projects']->id,'projects_id');
         foreach($array['projects_gallery']  as   $row){
            $row->images = $this->site_model->get_by_id('projects_gallery_photos',$row->id,'projects_gallery_id');  
        }
        $data['projects_gallery']=$array;
       
        $data['projects_videos'] = $this->site_model->get_by_id('projects_videos',$data['projects']->id,'projects_id');

        $data['projects_reasons_gallery'] = $this->site_model->get_by_id('projects_reasons_gallery',$data['projects']->id,'projects_id');



        // $data['similer_projects'] = $this->site_model->get_id_notequalto('projects',$data['projects']->id,'id',$data['projects']->project_type,'project_type',$data['projects']->city_id,$data['projects']->location_id);



        $similer_projects_location_count = count($this->site_model->get_id_notequalto('projects',$data['projects']->id,'id',$data['projects']->location_id,'location_id'));
    $similer_projects_city_count = count($this->site_model->get_id_notequalto('projects',$data['projects']->id,'id',$data['projects']->city_id,'city_id'));


    $final_less_amount=$data['projects']->unit_starting_price-((30/100)*$data['projects']->unit_starting_price);
    $final_more_amount=$data['projects']->unit_ending_price+((30/100)*$data['projects']->unit_ending_price);


    $similer_projects_price_count = count($this->site_model->get_id_notequalto_price('projects',$data['projects']->id,'id',$final_less_amount,$final_more_amount));

    if($similer_projects_condition_count > 0){
        $data['similer_projects'] = $this->site_model->get_id_notequalto('projects',$data['projects']->id,'id',$data['projects']->location_id,'location_id');
    }elseif($similer_projects_city_count > 0){
        $data['similer_projects'] = $this->site_model->get_id_notequalto('projects',$data['projects']->id,'id',$data['projects']->city_id,'city_id');
    }elseif($similer_projects_price_count > 0){
        $data['similer_projects']=$this->site_model->get_id_notequalto_price('projects',$data['projects']->id,'id',$final_less_amount,$final_more_amount);
    }else{
        $data['similer_projects']=$this->site_model->get_by_limit_desc('projects',10);
    }



        // footer codes
        $this->load->view('micro_site', $data);
    }





        public function offer_zone($p_link)
    {
        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','2');
        $this->load->view('includes/header', $this->data);
        $data["budget_form_field"] = $this->site_model->get_orderby_asc('budget_form_field', 'priority');

        $data['city'] = $this->site_model->get_by_p_link('city',$p_link);

        $city_id = $data['city']->id;

        $data['projects_apartment_count'] = count($this->site_model->get_offer_zone('projects','apartment','project_type',$city_id));
        

        $array=[];
        $array['projects_apartment'] = $this->site_model->get_offer_zone('projects','apartment','project_type',$city_id);
         foreach($array['projects_apartment']  as   $row){
            $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
            $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');  
        } 

        $data['projects_apartment']=$array;
           
        $data['projects_villa_count'] = count($this->site_model->get_offer_zone('projects','villa','project_type',$city_id));


        $array=[];
        $array['projects_villa'] = $this->site_model->get_offer_zone('projects','villa','project_type',$city_id);
        foreach($array['projects_villa']  as   $row){
            $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
            $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');  
        } 
        $data['projects_villa']=$array;

        $this->load->view('offer_zone', $data);
        $this->load->view('includes/footer');
    }



    public function recently_add_projects()
    {
        $this->data["budget_form_field"] = $this->site_model->get_orderby_asc('budget_form_field', 'priority');
        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','13');
        $this->load->view('includes/main_header', $this->data);

        // $data['home_banners'] = $this->site_model->get('home_banners');

        $data["home_banners1"] = $this->site_model->get_cms('home_banners', '1');
        $data["home_banners2"] = $this->site_model->get_cms('home_banners', '6');
        $data["home_banners3"] = $this->site_model->get_cms('home_banners', '7');
        $data["home_banners4"] = $this->site_model->get_cms('home_banners', '8');
        $data["home_banners5"] = $this->site_model->get_cms('home_banners', '9');
        $data["home_banners6"] = $this->site_model->get_cms('home_banners', '10');
        $data["cms_pages12"] = $this->site_model->get_cms('cms_pages', '12');
        $data["cms_pages16"] = $this->site_model->get_cms('cms_pages', '16');
        $data['testimonials'] = $this->site_model->get('testimonials');

        // $data["city_id_fetch"] = $this->site_model->get_by_p_link('city',$this->data['current_city']);
        // $city_id_fetch = $data["city_id_fetch"]->id;

        $array=[];
        $array['projects'] = $this->site_model->get_by_limit_desc('projects','10');
        foreach($array['projects']  as   $row){
            $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
            $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');  
            $row->projects_flats_details = $this->site_model->get_by_id_asc('projects_flats_details',$row->id,'projects_id',2);  
            $row->original_projects_flats_details_coun = count($this->site_model->get_by_id('projects_flats_details',$row->id,'projects_id')); 
        } 
        $data['projects']=$array;

        // $data['projects_apartment_count'] = count($this->site_model->projects_by_type('projects','apartment','project_type',$city_id_fetch));
        // $data['projects_villa'] = $this->site_model->projects_by_type('projects','villa','project_type',$city_id_fetch);
        // $data['projects_villa_count'] = count($this->site_model->projects_by_type('projects','villa','project_type',$city_id_fetch));

        $this->load->view('recently_add_projects', $data);
        $this->load->view('includes/main_footer');
    }



        public function builder_landing($p_link)
    {

        $this->data['page_meta_tags'] = $this->site_model->get_by_p_link('about_builder',$p_link);
        $this->load->view('includes/header', $this->data);
        $data["budget_form_field"] = $this->site_model->get_orderby_asc('budget_form_field', 'priority');
        $data["cms_pages19"] = $this->site_model->get_cms('cms_pages', '19');

        $data['about_builder'] = $this->site_model->get_by_p_link('about_builder',$p_link);
        $builder_id=$data['about_builder']->id;

        $data['ongoing_projects_count'] = count($this->site_model->get_by_id_col2('projects',$builder_id,'builder_id','Ongoing Project','project_status'));  
        $array=[];
        $array['ongoing_projects'] = $this->site_model->get_by_id_col2('projects',$builder_id,'builder_id','Ongoing Project','project_status');
        foreach($array['ongoing_projects']  as   $row){
            $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
            $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');  
        } 
        $data['ongoing_projects']=$array;



        $data['upcoming_projects_count'] = count($this->site_model->get_by_id_col2('projects',$builder_id,'builder_id','Upcoming Project','project_status'));   
        $array=[];
        $array['upcoming_projects'] = $this->site_model->get_by_id_col2('projects',$builder_id,'builder_id','Upcoming Project','project_status');
        foreach($array['upcoming_projects']  as   $row){
            $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
            $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');  
        } 
        $data['upcoming_projects']=$array;


        $data['completed_projects_count'] = count($this->site_model->get_by_id_col2('projects',$builder_id,'builder_id','Completed Project','project_status'));    
        $array=[];
        $array['completed_projects'] = $this->site_model->get_by_id_col2('projects',$builder_id,'builder_id','Completed Project','project_status');
        foreach($array['completed_projects']  as   $row){
            $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
            $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');  
        } 
        $data['completed_projects']=$array;

        $this->load->view('builder_landing', $data);
        $this->load->view('includes/footer');
    }








        public function search_landing()
    {
        $this->data['page_meta_tags'] = $this->site_model->get_cms('page_wise_meta_tags','11');
        $this->load->view('includes/header', $this->data);

        $data["cms_pages18"] = $this->site_model->get_cms('cms_pages', '18');
        $data["budget_form_field"] = $this->site_model->get_orderby_asc('budget_form_field', 'priority');


        $city_s = $this->input->get('city_s');
        $property_type_s = $this->input->get('property_type_s');
        $bhk_type_s = $this->input->get('bhk_type_s');
        $from_budget_s = $this->input->get('from_budget_s');
        $to_budget_s = $this->input->get('to_budget_s');
        $possession_year_s = $this->input->get('possession_year_s');
        $search = $this->input->get('search');
        $arrange_by = $this->input->get('arrange_by');
        $location = $this->input->get('location');
        $zone = $this->input->get('zone');


        $data["city"] = $this->site_model->get_by_p_link('city',$city_s);
        $city_id[]=$data["city"]->id;

        if($search != ''){
            $search_city = $this->site_model->get_by_coloum_like('city',$search,'city');
            foreach($search_city as $row){
                $city_projects=$this->site_model->get_by_id('projects',$row->id,'city_id');
                    foreach($city_projects as $row){
                       $projects_id[]=$row->id;
                    }
            }         
        }

        if($search != ''){
            $search_location = $this->site_model->get_by_coloum_like('location',$search,'location');
            foreach($search_location as $row){
                $location_projects=$this->site_model->get_by_id('projects',$row->id,'location_id');
                    foreach($location_projects as $row){
                       $projects_id[]=$row->id;
                    }
            }       
        }


        // if($location != ''){
        //     $search_location_by_name = $this->site_model->get_by_coloum_like('location',$location,'location');
        //     foreach($search_location_by_name as $row){
        //         $location_projects=$this->site_model->get_by_id('projects',$row->id,'location_id');
        //             foreach($location_projects as $row){
        //                $projects_id[]=$row->id;
        //             }
        //     }         
        // }

        if($zone != ''){
            $search_zone = $this->site_model->get_by_coloum_like('location',$zone,'zone_link');
            foreach($search_zone as $row){
                $location_projects=$this->site_model->get_by_id('projects',$row->id,'location_id');
                    foreach($location_projects as $row){
                       $projects_id[]=$row->id;
                    }
            }         
        }

        if($bhk_type_s != ''){
            $bhk_type_s = $this->site_model->get_by_coloum_like('projects_flats_details',$bhk_type_s,'heading');
            foreach($bhk_type_s as $row){
                $projects_id[]=$row->projects_id;
            }         
        }


        if($from_budget_s  != 0 || $to_budget_s != 0){
            $budget_search = $this->site_model->get_by_value_between('projects_flats_details',$from_budget_s,$to_budget_s);

            foreach($budget_search as $row){
                $projects_id[]=$row->projects_id;
            }
        }


        // $projects_id = array_unique($projects_id);
        // foreach($projects_id as $key => $link) 
        //     { 
        //         if($link === '') 
        //         { 
        //             unset($projects_id[$key]); 
        //         } 
        //     } 
    

        $data['perpage']       = 12;
        $total_records = count($this->site_model->get_search_projects('projects',$city_id,$property_type_s,$possession_year_s,$search,$projects_id,$arrange_by));
        $pagination = my_pagination('view/search_landing' , $data['perpage'], $total_records);
        $data['pagination']  = $pagination['pagination'];

        $array=[];
        $array["projects"] = $this->site_model->get_search_projects('projects',$city_id,$property_type_s,$possession_year_s,$search,$projects_id,$arrange_by, $data['perpage']);
        foreach($array['projects']  as   $row){
            $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
            $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');  
        } 



        if(count($array["projects"]) > 0){
            $data['projects']=$array;

        }else{

            $array=[];
            $array["projects"] = $this->site_model->get_by_id_limit_desc('projects','city_id',$data["city"]->id,12);
                foreach($array['projects']  as   $row){
                    $row->location = $this->site_model->get_row_by_id('location',$row->location_id,'id');
                    $row->city = $this->site_model->get_row_by_id('city',$row->city_id,'id');  
                } 
                $data['projects']=$array;
                $data['no_search_resuts']="no_results_found";
        }
    

        $this->load->view('search_landing', $data);
        $this->load->view('includes/footer');
    }





// ----------------------------------     form details    -------------------------------------------

public function sub_page_enquiry_form(){

if($this->input->post('g-recaptcha-response', TRUE) == ''){
        $this->session->set_flashdata('mail_error','Invalid capcha please try after refresing the page');  ?>        
          <script> window.location.href = "<?php echo $this->input->post('sub_page_link', TRUE); ?>"; </script>
  <?php  }else{

$ip_address=$_SERVER['REMOTE_ADDR'];

$geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
$addrDetailsArr = unserialize(file_get_contents($geopluginURL)); 


$city = $addrDetailsArr['geoplugin_city']; 
$state = $addrDetailsArr['geoplugin_region']; 
$country = $addrDetailsArr['geoplugin_countryName']; 

$this->load->library('user_agent');


if ($this->agent->is_browser())
{
        $agent = $this->agent->browser().' '.$this->agent->version();

        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "DESKTOP";
}
elseif ($this->agent->is_robot())
{
        $agent = $this->agent->robot();

        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "ROBOT";

}
elseif ($this->agent->is_mobile())
{
        $agent = $this->agent->mobile();

        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "MOBILE";
}
else
{
        $agent = 'Unidentified User';

        $os_browser = $agent;
        $device = $agent;
}

// $form_data = $this->input->post();   

  $name = $this->input->post('name', TRUE);
  $email = $this->input->post('email', TRUE);
  $phone_number = $this->input->post('phone_number', TRUE);
  $country_code = $this->input->post('country_code', TRUE);
  $message = $this->input->post('message', TRUE);
  $home_loan = $this->input->post('home_loan', TRUE);
  $project_name = $this->input->post('project_name', TRUE);
  $to_email_1 = $this->input->post('to_email_1', TRUE);
  $to_email_2 = $this->input->post('to_email_2', TRUE);
  $to_email_3 = $this->input->post('to_email_3', TRUE);
  $sub_page_link = $this->input->post('sub_page_link', TRUE);

  $phone_number=$country_code." ".$phone_number;

  $enquiry_type = $this->input->post('enquiry_type', TRUE);
  $enquiry_to_company = $this->input->post('enquiry_to_company', TRUE);



  if($home_loan == 'Yes'){
    $home_loan_required = 'Yes'; 
  }else{
     $home_loan_required = 'No'; 
  }

  $client_city = $city;
  $client_state = $state;
  $client_country = $country;

  $client_os_browser = $os_browser;
  $client_device = $device;


  $to_mail = $this->data["site_settings"]->contact_email;
  $from_email = $this->data["site_settings"]->from_email;
  $site_name = $this->data["site_settings"]->site_name;

  $enquired_date = date('d-M-Y h:i A');

$site_url=base_url()."".$sub_page_link;

  $subject="$enquiry_type for the project $project_name, from the site LP Property";
  $email_message = "Hi Sir/Madam, <br>
                        Below are the client details<br><br>
                    Name: $name <br>
                    Email: $email <br>
                    Phone: $phone_number <br>
                    Required Home Loan: $home_loan_required <br>
                    Date : $enquired_date <br>
                    Type: $enquiry_type <br>
                    IP-Address: $ip_address <br>
                    Device: $device <br>
                    OS & Browser: $os_browser <br>
                    City: $client_city <br>
                    State: $client_state <br>
                    Country: $client_country <br>
                    Site URL: $site_url <br>
                    Enquiry to Company: $enquiry_to_company <br>
                    Enquiry for Project: $project_name";


       $this->load->library('email');
       // $this->email->from($email);
       // $this->email->to($to_mail);
      // $this->email->subject("Customer enquiry mail of site $site");
       // $this->email->message($email_message);
       //$send = $this->email->send();


require_once (APPPATH.'libraries/vendor/autoload.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/Exception.php');

//PHPMailer Object  
$mail = new PHPMailer\PHPMailer\PHPMailer();




    if($to_email_1 != ''){
        $mail->From = $from_email;
        $mail->FromName = $site_name;
        $mail->addAddress($to_email_1); //Recipient name is optional
        $mail->addReplyTo($from_email, "Reply");
        $mail->isHTML(true);
        $mail->Sender = $from_email;
        $mail->Subject = $subject;
        $mail->Body = $email_message;
        $sucess = $mail->send();
    }

    if($to_email_2 != ''){
        $mail->From = $from_email;
        $mail->FromName = $site_name;
        $mail->addAddress($to_email_2); //Recipient name is optional
        $mail->addReplyTo($from_email, "Reply");
        $mail->isHTML(true);
        $mail->Sender = $from_email;
        $mail->Subject = $subject;
        $mail->Body = $email_message;
        $sucess = $mail->send();
    }

    if($to_email_3 != ''){
        $mail->From = $from_email;
        $mail->FromName = $site_name;
        $mail->addAddress($to_email_3); //Recipient name is optional
        $mail->addReplyTo($from_email, "Reply");
        $mail->isHTML(true);
        $mail->Sender = $from_email;
        $mail->Subject = $subject;
        $mail->Body = $email_message;
        $sucess = $mail->send();
    }
//-------------------- END SMTP --------------------

//From email address and name
$mail->From = $from_email;
$mail->FromName = $site_name;

//To address and name
// $mail->addAddress("recepient1@example.com", "Recepient Name");
$mail->addAddress($to_mail); //Recipient name is optional

//Address to which recipient will reply
$mail->addReplyTo($from_email, "Reply");

//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);
$mail->Sender = $from_email;
$mail->Subject = $subject;
$mail->Body = $email_message;
// $mail->AltBody = "This is the plain text version of the email content";
$sucess = $mail->send();

       if ($sucess) { 

  $form_data['name'] = $this->input->post('name', TRUE);
  $form_data['email'] = $this->input->post('email', TRUE);
  $form_data['phone_number'] = $this->input->post('phone_number', TRUE)." ".$this->input->post('country_code', TRUE);
  $form_data['message'] = $this->input->post('message', TRUE);
  $form_data['project_name'] = $this->input->post('project_name', TRUE);

  if($home_loan == 'Yes'){
    $form_data['home_loan_required'] = 'Yes'; 
  }else{
     $form_data['home_loan_required'] = 'No'; 
  }


  $form_data['client_city'] = $city;
  $form_data['client_state'] = $state;
  $form_data['client_country'] = $country;
  $form_data['client_os_browser'] = $os_browser;
  $form_data['client_device'] = $device;
  $form_data['enquired_date'] = date('d-M-Y h:i A');
  $form_data['enquired_string_date'] = strtotime(date('d-M-Y h:i A'));


  $form_data['enquiry_type'] = $enquiry_type;
  $form_data['ip_address'] = $ip_address;
  $form_data['site_url'] = $site_url;
  $form_data['enquiry_to_company'] = $enquiry_to_company;


 $result = $this->cmoon_model->insert('sub_page_enquiry_form', $form_data);

        $this->session->set_flashdata('mail_success','Thank you for enquiring '.$project_name.' project. Our representative will get in touch with you shortly.'); 
        redirect($sub_page_link);
          } else { 

      $this->session->set_flashdata('mail_error','Sorry something went wrong please try again refreshing the page'); 
      redirect($sub_page_link);
        }  }  }  




// ----------------------------------     form details    -------------------------------------------

public function offer_zone_enquiry_form(){


if($this->input->post('g-recaptcha-response', TRUE) == ''){
        $this->session->set_flashdata('mail_error','Invalid capcha please try after refresing the page');  ?>        
          <script> window.location.href = "<?php echo $this->input->post('page_link', TRUE); ?>"; </script>
  <?php  }else{

$ip_address=$_SERVER['REMOTE_ADDR'];

$geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
$addrDetailsArr = unserialize(file_get_contents($geopluginURL)); 


$city = $addrDetailsArr['geoplugin_city']; 
$state = $addrDetailsArr['geoplugin_region']; 
$country = $addrDetailsArr['geoplugin_countryName']; 

$this->load->library('user_agent');

if ($this->agent->is_browser()){
        $agent = $this->agent->browser().' '.$this->agent->version();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "DESKTOP";
}
elseif ($this->agent->is_robot()){
        $agent = $this->agent->robot();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "ROBOT";
}
elseif ($this->agent->is_mobile()){
        $agent = $this->agent->mobile();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "MOBILE";
}
else{
        $agent = 'Unidentified User';
        $os_browser = $agent;
        $device = $agent;
}

// $form_data = $this->input->post();   

  $name = $this->input->post('name', TRUE);
  $email = $this->input->post('email', TRUE);
  $phone_number = $this->input->post('phone_number', TRUE);
  $country_code = $this->input->post('country_code', TRUE);
  $page_link = $this->input->post('page_link', TRUE);
  $enquiry_type = $this->input->post('enquiry_type', TRUE);
  $enquired_for_city = $this->input->post('enquired_for_city', TRUE);
  $property_type = $this->input->post('property_type', TRUE);
  $budget_range = $this->input->post('budget_range', TRUE);

  $phone_number=$country_code." ".$phone_number;

  $home_loan = $this->input->post('home_loan', TRUE);
  if($home_loan == 'Yes'){
    $home_loan_required = 'Yes'; 
  }else{
     $home_loan_required = 'No'; 
  }


  $client_city = $city;
  $client_state = $state;
  $client_country = $country;

  $client_os_browser = $os_browser;
  $client_device = $device;


  $to_mail = $this->data["site_settings"]->contact_email;
  $from_email = $this->data["site_settings"]->from_email;
  $site_name = $this->data["site_settings"]->site_name;

  $enquired_date = date('d-M-Y h:i A');

$site_url=base_url()."".$page_link;


  $subject="$enquiry_type, Enquiry form the site LP Prperty";
  $email_message = "Hi Sir/Madam, <br>
                        Below are the client details<br><br>
                    Name: $name <br>
                    Email: $email <br>
                    Phone: $phone_number <br>
                    Enquired For City: $enquired_for_city <br>
                    Property Type: $property_type <br>
                    Budget Range: $budget_range <br>
                    Required Home Loan: $home_loan_required <br>
                    Date : $enquired_date <br>
                    Type: $enquiry_type <br>
                    IP-Address: $ip_address <br>
                    Device: $device <br>
                    OS & Browser: $os_browser <br>
                    City: $client_city <br>
                    State: $client_state <br>
                    Country: $client_country <br>
                    Site URL: $site_url";


       $this->load->library('email');
       // $this->email->from($email);
       // $this->email->to($to_mail);
      // $this->email->subject("Customer enquiry mail of site $site");
       // $this->email->message($email_message);
       //$send = $this->email->send();


require_once (APPPATH.'libraries/vendor/autoload.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/Exception.php');

//PHPMailer Object  
$mail = new PHPMailer\PHPMailer\PHPMailer();

//-------------------- END SMTP --------------------

//From email address and name
$mail->From = $from_email;
$mail->FromName = $site_name;

//To address and name
// $mail->addAddress("recepient1@example.com", "Recepient Name");
$mail->addAddress($to_mail); //Recipient name is optional

//Address to which recipient will reply
$mail->addReplyTo($from_email, "Reply");

//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);
$mail->Sender = $from_email;
$mail->Subject = $subject;
$mail->Body = $email_message;
// $mail->AltBody = "This is the plain text version of the email content";
$sucess = $mail->send();

       if ($sucess) { 

  $form_data['name'] = $this->input->post('name', TRUE);
  $form_data['email'] = $this->input->post('email', TRUE);
  $form_data['phone_number'] = $this->input->post('phone_number', TRUE)." ".$this->input->post('country_code', TRUE);
  $form_data['enquired_for_city'] = $this->input->post('enquired_for_city', TRUE);
  $form_data['property_type'] = $this->input->post('property_type', TRUE);
  $form_data['budget_range'] = $this->input->post('budget_range', TRUE);
  $form_data['enquiry_type'] = $this->input->post('enquiry_type', TRUE);



  if($home_loan == 'Yes'){
    $form_data['home_loan_required'] = 'Yes'; 
  }else{
     $form_data['home_loan_required'] = 'No'; 
  }


  $form_data['client_city'] = $city;
  $form_data['client_state'] = $state;
  $form_data['client_country'] = $country;
  $form_data['client_os_browser'] = $os_browser;
  $form_data['client_device'] = $device;
  $form_data['enquired_date'] = date('d-M-Y h:i A');
  $form_data['enquired_string_date'] = strtotime(date('d-M-Y h:i A'));


  $form_data['ip_address'] = $ip_address;
  $form_data['site_url'] = $site_url;


 $result = $this->cmoon_model->insert('offer_zone_enquiry_form', $form_data);

        $this->session->set_flashdata('mail_success','Thank you for showing your intrest, We will address your request very soon.');  
        redirect($page_link);
           } else { 

      $this->session->set_flashdata('mail_error','Sorry something went wrong please try again refreshing the page');  
      redirect($page_link);  }  }  }  




// ----------------------------------     form details    -------------------------------------------

public function home_loan_know_more_form(){



if($this->input->post('g-recaptcha-response', TRUE) == ''){
        $this->session->set_flashdata('mail_error','Invalid capcha please try after refresing the page');  ?>        
          <script> window.location.href = "<?php echo $this->input->post('page_link', TRUE); ?>"; </script>
  <?php  }else{

$ip_address=$_SERVER['REMOTE_ADDR'];

$geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
$addrDetailsArr = unserialize(file_get_contents($geopluginURL)); 


$city = $addrDetailsArr['geoplugin_city']; 
$state = $addrDetailsArr['geoplugin_region']; 
$country = $addrDetailsArr['geoplugin_countryName']; 

$this->load->library('user_agent');

if ($this->agent->is_browser()){
        $agent = $this->agent->browser().' '.$this->agent->version();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "DESKTOP";
}
elseif ($this->agent->is_robot()){
        $agent = $this->agent->robot();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "ROBOT";
}
elseif ($this->agent->is_mobile()){
        $agent = $this->agent->mobile();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "MOBILE";
}
else{
        $agent = 'Unidentified User';
        $os_browser = $agent;
        $device = $agent;
}

// $form_data = $this->input->post();   

  $name = $this->input->post('name', TRUE);
  $email = $this->input->post('email', TRUE);
  $phone_number = $this->input->post('phone_number', TRUE);
  $country_code = $this->input->post('country_code', TRUE);
  $page_link = $this->input->post('page_link', TRUE);
  $enquiry_type = $this->input->post('enquiry_type', TRUE);
  $enquired_for_city = $this->input->post('enquired_for_city', TRUE);
  $property_type = $this->input->post('property_type', TRUE);
  $budget_range = $this->input->post('budget_range', TRUE);

  $phone_number=$country_code." ".$phone_number;

  $accepted_for_call = $this->input->post('accepted_for_call', TRUE);
  if($accepted_for_call == 'Yes'){
    $accepted_for_call = 'Yes'; 
  }else{
     $accepted_for_call = 'No'; 
  }


  $client_city = $city;
  $client_state = $state;
  $client_country = $country;

  $client_os_browser = $os_browser;
  $client_device = $device;


  $to_mail = $this->data["site_settings"]->contact_email;
  $from_email = $this->data["site_settings"]->from_email;
  $site_name = $this->data["site_settings"]->site_name;

  $enquired_date = date('d-M-Y h:i A');

$site_url=base_url()."".$page_link;


  $subject="$enquiry_type, from the site LP Property";
  $email_message = "Hi Sir/Madam, <br>
                        Below are the client details<br><br>
                    Name: $name <br>
                    Email: $email <br>
                    Phone: $phone_number <br>
                    Enquired For City: $enquired_for_city <br>
                    Property Type: $property_type <br>
                    Budget Range: $budget_range <br>
                    Accepted For Call: $accepted_for_call <br>
                    Date : $enquired_date <br>
                    Type: $enquiry_type <br>
                    IP-Address: $ip_address <br>
                    Device: $device <br>
                    OS & Browser: $os_browser <br>
                    City: $client_city <br>
                    State: $client_state <br>
                    Country: $client_country <br>
                    Site URL: $site_url";

       $this->load->library('email');
       // $this->email->from($email);
       // $this->email->to($to_mail);
      // $this->email->subject("Customer enquiry mail of site $site");
       // $this->email->message($email_message);
       //$send = $this->email->send();


require_once (APPPATH.'libraries/vendor/autoload.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/Exception.php');

//PHPMailer Object  
$mail = new PHPMailer\PHPMailer\PHPMailer();

//-------------------- END SMTP --------------------

//From email address and name
$mail->From = $from_email;
$mail->FromName = $site_name;

//To address and name
// $mail->addAddress("recepient1@example.com", "Recepient Name");
$mail->addAddress($to_mail); //Recipient name is optional

//Address to which recipient will reply
$mail->addReplyTo($from_email, "Reply");

//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);
$mail->Sender = $from_email;
$mail->Subject = $subject;
$mail->Body = $email_message;
// $mail->AltBody = "This is the plain text version of the email content";
$sucess = $mail->send();

       if ($sucess) { 

  $form_data['name'] = $this->input->post('name', TRUE);
  $form_data['email'] = $this->input->post('email', TRUE);
  $form_data['phone_number'] = $this->input->post('phone_number', TRUE)." ".$this->input->post('country_code', TRUE);
  $form_data['enquired_for_city'] = $this->input->post('enquired_for_city', TRUE);
  $form_data['property_type'] = $this->input->post('property_type', TRUE);
  $form_data['budget_range'] = $this->input->post('budget_range', TRUE);
  $form_data['enquiry_type'] = $this->input->post('enquiry_type', TRUE);



  if($accepted_for_call == 'Yes'){
    $form_data['accepted_for_call'] = 'Yes'; 
  }else{
     $form_data['accepted_for_call'] = 'No'; 
  }


  $form_data['client_city'] = $city;
  $form_data['client_state'] = $state;
  $form_data['client_country'] = $country;
  $form_data['client_os_browser'] = $os_browser;
  $form_data['client_device'] = $device;
  $form_data['enquired_date'] = date('d-M-Y h:i A');
  $form_data['enquired_string_date'] = strtotime(date('d-M-Y h:i A'));


  $form_data['ip_address'] = $ip_address;
  $form_data['site_url'] = $site_url;


 $result = $this->cmoon_model->insert('home_loan_know_more_form', $form_data);

        $this->session->set_flashdata('mail_success','Thank you for showing your intrest, We will address your request very soon.'); 
        redirect($page_link);
  } else { 

      $this->session->set_flashdata('mail_error','Sorry something went wrong please try again refreshing the page'); 
      redirect($page_link);
        }  }  } 







// ----------------------------------     form details    -------------------------------------------

public function business_enquiry_form(){


if($this->input->post('g-recaptcha-response', TRUE) == ''){
        $this->session->set_flashdata('mail_error','Invalid capcha please try after refresing the page');  ?>        
          <script> window.location.href = "<?php echo $this->input->post('page_link', TRUE); ?>"; </script>
  <?php  }else{

$ip_address=$_SERVER['REMOTE_ADDR'];

$geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
$addrDetailsArr = unserialize(file_get_contents($geopluginURL)); 


$city = $addrDetailsArr['geoplugin_city']; 
$state = $addrDetailsArr['geoplugin_region']; 
$country = $addrDetailsArr['geoplugin_countryName']; 

$this->load->library('user_agent');

if ($this->agent->is_browser()){
        $agent = $this->agent->browser().' '.$this->agent->version();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "DESKTOP";
}
elseif ($this->agent->is_robot()){
        $agent = $this->agent->robot();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "ROBOT";
}
elseif ($this->agent->is_mobile()){
        $agent = $this->agent->mobile();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "MOBILE";
}
else{
        $agent = 'Unidentified User';
        $os_browser = $agent;
        $device = $agent;
}

// $form_data = $this->input->post();   

  $name = $this->input->post('name', TRUE);
  $email = $this->input->post('email', TRUE);
  $phone_number = $this->input->post('phone_number', TRUE);
  $page_link = $this->input->post('page_link', TRUE);
  $enquiry_type = $this->input->post('enquiry_type', TRUE);
  $enquired_for_city = $this->input->post('enquired_for_city', TRUE);
  $message = $this->input->post('message', TRUE);

  $country_code = $this->input->post('country_code', TRUE);

if($country_code != ''){
$phone_number = $country_code." ".$phone_number;
}


  $client_city = $city;
  $client_state = $state;
  $client_country = $country;

  $client_os_browser = $os_browser;
  $client_device = $device;


  $to_mail = $this->data["site_settings"]->contact_email;
  $from_email = $this->data["site_settings"]->from_email;
  $site_name = $this->data["site_settings"]->site_name;

  $enquired_date = date('d-M-Y h:i A');

$site_url=base_url()."".$page_link;


  $subject="$enquiry_type, from the site LP Property";
  $email_message = "Hi Sir/Madam, <br>
                        Below are the client details<br><br>
                    Name: $name <br>
                    Email: $email <br>
                    Phone: $phone_number <br>
                    Enquired For City: $enquired_for_city <br>
                    Message: $message <br>
                    Date : $enquired_date <br>
                    Type: $enquiry_type <br>
                    IP-Address: $ip_address <br>
                    Device: $device <br>
                    OS & Browser: $os_browser <br>
                    City: $client_city <br>
                    State: $client_state <br>
                    Country: $client_country <br>
                    Site URL: $site_url";


       $this->load->library('email');
       // $this->email->from($email);
       // $this->email->to($to_mail);
      // $this->email->subject("Customer enquiry mail of site $site");
       // $this->email->message($email_message);
       //$send = $this->email->send();


require_once (APPPATH.'libraries/vendor/autoload.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/Exception.php');

//PHPMailer Object  
$mail = new PHPMailer\PHPMailer\PHPMailer();

//-------------------- END SMTP --------------------

//From email address and name
$mail->From = $from_email;
$mail->FromName = $site_name;

//To address and name
// $mail->addAddress("recepient1@example.com", "Recepient Name");
$mail->addAddress($to_mail); //Recipient name is optional

//Address to which recipient will reply
$mail->addReplyTo($from_email, "Reply");

//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);
$mail->Sender = $from_email;
$mail->Subject = $subject;
$mail->Body = $email_message;
// $mail->AltBody = "This is the plain text version of the email content";
$sucess = $mail->send();

       if ($sucess) { 

  $form_data['name'] = $this->input->post('name', TRUE);
  $form_data['email'] = $this->input->post('email', TRUE);
  $form_data['phone_number'] = $this->input->post('phone_number', TRUE);
  $form_data['enquired_for_city'] = $this->input->post('enquired_for_city', TRUE);
  $form_data['message'] = $this->input->post('message', TRUE);
  $form_data['enquiry_type'] = $this->input->post('enquiry_type', TRUE);


  if($this->input->post('country_code', TRUE) != ''){
      $form_data['phone_number'] = $this->input->post('phone_number', TRUE)." ".$this->input->post('country_code', TRUE);
  }

  $form_data['client_city'] = $city;
  $form_data['client_state'] = $state;
  $form_data['client_country'] = $country;
  $form_data['client_os_browser'] = $os_browser;
  $form_data['client_device'] = $device;
  $form_data['enquired_date'] = date('d-M-Y h:i A');
  $form_data['enquired_string_date'] = strtotime(date('d-M-Y h:i A'));

  $form_data['ip_address'] = $ip_address;
  $form_data['site_url'] = $site_url;


 $result = $this->cmoon_model->insert('business_enquiry_form', $form_data);

        $this->session->set_flashdata('mail_success','Thank you for showing your intrest, We will address your request very soon.'); 
        redirect($page_link);
  } else { 

      $this->session->set_flashdata('mail_error','Sorry something went wrong please try again refreshing the page'); 
      redirect($page_link);
        }  }  } 





// ----------------------------------     form details    -------------------------------------------

public function customer_enquiry_form(){



if($this->input->post('g-recaptcha-response', TRUE) == ''){
        $this->session->set_flashdata('mail_error','Invalid capcha please try after refresing the page');  ?>        
          <script> window.location.href = "<?php echo $this->input->post('page_link', TRUE); ?>"; </script>
  <?php  }else{

$ip_address=$_SERVER['REMOTE_ADDR'];

$geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
$addrDetailsArr = unserialize(file_get_contents($geopluginURL)); 


$city = $addrDetailsArr['geoplugin_city']; 
$state = $addrDetailsArr['geoplugin_region']; 
$country = $addrDetailsArr['geoplugin_countryName']; 

$this->load->library('user_agent');

if ($this->agent->is_browser()){
        $agent = $this->agent->browser().' '.$this->agent->version();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "DESKTOP";
}
elseif ($this->agent->is_robot()){
        $agent = $this->agent->robot();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "ROBOT";
}
elseif ($this->agent->is_mobile()){
        $agent = $this->agent->mobile();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "MOBILE";
}
else{
        $agent = 'Unidentified User';
        $os_browser = $agent;
        $device = $agent;
}

// $form_data = $this->input->post();   

  $name = $this->input->post('name', TRUE);
  $email = $this->input->post('email', TRUE);
  $phone_number = $this->input->post('phone_number', TRUE);
  $page_link = $this->input->post('page_link', TRUE);
  $enquiry_type = $this->input->post('enquiry_type', TRUE);
  $enquired_for_city = $this->input->post('enquired_for_city', TRUE);
  $property_type = $this->input->post('property_type', TRUE);
  $budget_range = $this->input->post('budget_range', TRUE);


  $client_city = $city;
  $client_state = $state;
  $client_country = $country;

  $client_os_browser = $os_browser;
  $client_device = $device;


  $to_mail = $this->data["site_settings"]->contact_email;
  $from_email = $this->data["site_settings"]->from_email;
  $site_name = $this->data["site_settings"]->site_name;

  $enquired_date = date('d-M-Y h:i A');

$site_url=base_url()."".$page_link;


  $subject="$enquiry_type, from the site LP Property";
  $email_message = "Hi Sir/Madam, <br>
                        Below are the client details<br><br>
                    Name: $name <br>
                    Email: $email <br>
                    Phone: $phone_number <br>
                    Enquired For City: $enquired_for_city <br>
                    Property Type: $property_type <br>
                    Budget Range: $budget_range <br>
                    Date : $enquired_date <br>
                    Type: $enquiry_type <br>
                    IP-Address: $ip_address <br>
                    Device: $device <br>
                    OS & Browser: $os_browser <br>
                    City: $client_city <br>
                    State: $client_state <br>
                    Country: $client_country <br>
                    Site URL: $site_url";

       $this->load->library('email');
       // $this->email->from($email);
       // $this->email->to($to_mail);
      // $this->email->subject("Customer enquiry mail of site $site");
       // $this->email->message($email_message);
       //$send = $this->email->send();


require_once (APPPATH.'libraries/vendor/autoload.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/Exception.php');

//PHPMailer Object  
$mail = new PHPMailer\PHPMailer\PHPMailer();

//-------------------- END SMTP --------------------

//From email address and name
$mail->From = $from_email;
$mail->FromName = $site_name;

//To address and name
// $mail->addAddress("recepient1@example.com", "Recepient Name");
$mail->addAddress($to_mail); //Recipient name is optional

//Address to which recipient will reply
$mail->addReplyTo($from_email, "Reply");

//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);
$mail->Sender = $from_email;
$mail->Subject = $subject;
$mail->Body = $email_message;
// $mail->AltBody = "This is the plain text version of the email content";
$sucess = $mail->send();

       if ($sucess) { 

  $form_data['name'] = $this->input->post('name', TRUE);
  $form_data['email'] = $this->input->post('email', TRUE);
  $form_data['phone_number'] = $this->input->post('phone_number', TRUE);
  $form_data['enquired_for_city'] = $this->input->post('enquired_for_city', TRUE);
  $form_data['property_type'] = $this->input->post('property_type', TRUE);
  $form_data['budget_range'] = $this->input->post('budget_range', TRUE);
  $form_data['enquiry_type'] = $this->input->post('enquiry_type', TRUE);


  $form_data['client_city'] = $city;
  $form_data['client_state'] = $state;
  $form_data['client_country'] = $country;
  $form_data['client_os_browser'] = $os_browser;
  $form_data['client_device'] = $device;
  $form_data['enquired_date'] = date('d-M-Y h:i A');
  $form_data['enquired_string_date'] = strtotime(date('d-M-Y h:i A'));


  $form_data['ip_address'] = $ip_address;
  $form_data['site_url'] = $site_url;


 $result = $this->cmoon_model->insert('customer_enquiry_form', $form_data);

        $this->session->set_flashdata('mail_success','Thank you for showing your intrest, We will address your request very soon.'); 
        redirect($page_link);
  } else { 

      $this->session->set_flashdata('mail_error','Sorry something went wrong please try again refreshing the page'); 
      redirect($page_link);
        }  }  } 




// ----------------------------------     form details    -------------------------------------------

public function contact_us_form(){



if($this->input->post('g-recaptcha-response', TRUE) == ''){
        $this->session->set_flashdata('mail_error','Invalid capcha please try after refresing the page');  ?>        
          <script> window.location.href = "<?php echo $this->input->post('page_link', TRUE); ?>"; </script>
  <?php  }else{

$ip_address=$_SERVER['REMOTE_ADDR'];

$geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
$addrDetailsArr = unserialize(file_get_contents($geopluginURL)); 


$city = $addrDetailsArr['geoplugin_city']; 
$state = $addrDetailsArr['geoplugin_region']; 
$country = $addrDetailsArr['geoplugin_countryName']; 

$this->load->library('user_agent');

if ($this->agent->is_browser()){
        $agent = $this->agent->browser().' '.$this->agent->version();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "DESKTOP";
}
elseif ($this->agent->is_robot()){
        $agent = $this->agent->robot();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "ROBOT";
}
elseif ($this->agent->is_mobile()){
        $agent = $this->agent->mobile();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "MOBILE";
}
else{
        $agent = 'Unidentified User';
        $os_browser = $agent;
        $device = $agent;
}

// $form_data = $this->input->post();   

  $first_name = $this->input->post('first_name', TRUE);
  $last_name = $this->input->post('last_name', TRUE);
  $email = $this->input->post('email', TRUE);
  $phone_number = $this->input->post('phone_number', TRUE);
  $page_link = $this->input->post('page_link', TRUE);
  $enquiry_type = $this->input->post('enquiry_type', TRUE);
  $message = $this->input->post('message', TRUE);


  $client_city = $city;
  $client_state = $state;
  $client_country = $country;

  $client_os_browser = $os_browser;
  $client_device = $device;


  $to_mail = $this->data["site_settings"]->contact_email;
  $from_email = $this->data["site_settings"]->from_email;
  $site_name = $this->data["site_settings"]->site_name;

  $enquired_date = date('d-M-Y h:i A');

$site_url=base_url()."".$page_link;


  $subject="$enquiry_type, from the site LP Property";
  $email_message = "Hi Sir/Madam, <br>
                        Below are the client details<br><br>
                    First Name: $first_name <br>
                    Last Name: $last_name <br>
                    Email: $email <br>
                    Phone: $phone_number <br>
                    Message: $message <br>
                    Date : $enquired_date <br>
                    Type: $enquiry_type <br>
                    IP-Address: $ip_address <br>
                    Device: $device <br>
                    OS & Browser: $os_browser <br>
                    City: $client_city <br>
                    State: $client_state <br>
                    Country: $client_country <br>
                    Site URL: $site_url";

       $this->load->library('email');
       // $this->email->from($email);
       // $this->email->to($to_mail);
      // $this->email->subject("Customer enquiry mail of site $site");
       // $this->email->message($email_message);
       //$send = $this->email->send();


require_once (APPPATH.'libraries/vendor/autoload.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/Exception.php');

//PHPMailer Object  
$mail = new PHPMailer\PHPMailer\PHPMailer();

//-------------------- END SMTP --------------------

//From email address and name
$mail->From = $from_email;
$mail->FromName = $site_name;

//To address and name
// $mail->addAddress("recepient1@example.com", "Recepient Name");
$mail->addAddress($to_mail); //Recipient name is optional

//Address to which recipient will reply
$mail->addReplyTo($from_email, "Reply");

//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);
$mail->Sender = $from_email;
$mail->Subject = $subject;
$mail->Body = $email_message;
// $mail->AltBody = "This is the plain text version of the email content";
$sucess = $mail->send();

       if ($sucess) { 

  $form_data['first_name'] = $this->input->post('first_name', TRUE);
  $form_data['last_name'] = $this->input->post('last_name', TRUE);
  $form_data['email'] = $this->input->post('email', TRUE);
  $form_data['phone_number'] = $this->input->post('phone_number', TRUE);
  $form_data['enquired_for_city'] = $this->input->post('enquired_for_city', TRUE);
  $form_data['message'] = $this->input->post('message', TRUE);
  $form_data['enquiry_type'] = $this->input->post('enquiry_type', TRUE);

  $form_data['client_city'] = $city;
  $form_data['client_state'] = $state;
  $form_data['client_country'] = $country;
  $form_data['client_os_browser'] = $os_browser;
  $form_data['client_device'] = $device;
  $form_data['enquired_date'] = date('d-M-Y h:i A');
  $form_data['enquired_string_date'] = strtotime(date('d-M-Y h:i A'));


  $form_data['ip_address'] = $ip_address;
  $form_data['site_url'] = $site_url;


 $result = $this->cmoon_model->insert('contact_us_form', $form_data);

        $this->session->set_flashdata('mail_success','Thank you for showing your intrest, We will address your request very soon.'); 
        redirect($page_link);
  } else { 

      $this->session->set_flashdata('mail_error','Sorry something went wrong please try again refreshing the page'); 
      redirect($page_link);
        }  }  } 





// ----------------------------------     form details    -------------------------------------------

public function careers_form(){

if($this->input->post('g-recaptcha-response', TRUE) == ''){
        $this->session->set_flashdata('mail_error','Invalid capcha please try after refresing the page');  ?>        
          <script> window.location.href = "<?php echo $this->input->post('page_link', TRUE); ?>"; </script>
  <?php  }else{

$ip_address=$_SERVER['REMOTE_ADDR'];

$geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
$addrDetailsArr = unserialize(file_get_contents($geopluginURL)); 


$city = $addrDetailsArr['geoplugin_city']; 
$state = $addrDetailsArr['geoplugin_region']; 
$country = $addrDetailsArr['geoplugin_countryName']; 

$this->load->library('user_agent');

if ($this->agent->is_browser()){
        $agent = $this->agent->browser().' '.$this->agent->version();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "DESKTOP";
}
elseif ($this->agent->is_robot()){
        $agent = $this->agent->robot();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "ROBOT";
}
elseif ($this->agent->is_mobile()){
        $agent = $this->agent->mobile();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "MOBILE";
}
else{
        $agent = 'Unidentified User';
        $os_browser = $agent;
        $device = $agent;
}

// $form_data = $this->input->post();   

    if ($_FILES['resume']['name'] != '') {
        $form_data['resume'] = $this->upload_file_brochure('resume');
    }


  $first_name = $this->input->post('first_name', TRUE);
  $last_name = $this->input->post('last_name', TRUE);
  $email = $this->input->post('email', TRUE);
  $phone_number = $this->input->post('phone_number', TRUE);
  $designation = $this->input->post('designation', TRUE);
  $experience = $this->input->post('experience', TRUE);
  $page_link = $this->input->post('page_link', TRUE);
  $enquiry_type = $this->input->post('enquiry_type', TRUE);
  $message = $this->input->post('message', TRUE);

  $resume = $form_data['resume'];




  $client_city = $city;
  $client_state = $state;
  $client_country = $country;

  $client_os_browser = $os_browser;
  $client_device = $device;


  $to_mail = $this->data["site_settings"]->contact_email;
  $from_email = $this->data["site_settings"]->from_email;
  $site_name = $this->data["site_settings"]->site_name;

  $enquired_date = date('d-M-Y h:i A');

$site_url=base_url()."".$page_link;

$resume_full_path = base_url()."cmoon_images/".$resume;

  $subject="$enquiry_type, biodata and resume of candidate";
  $email_message = "Hi Sir/Madam, <br>
                        Below are the client details<br><br>
                    First Name: $first_name <br>
                    Last Name: $last_name <br>
                    Email: $email <br>
                    Phone: $phone_number <br>
                    Designation: $designation <br>
                    Experience: $experience <br>
                    Message: $message <br>
                    Date : $enquired_date <br>
                    Type: $enquiry_type <br>
                    IP-Address: $ip_address <br>
                    Device: $device <br>
                    OS & Browser: $os_browser <br>
                    City: $client_city <br>
                    State: $client_state <br>
                    Country: $client_country <br>
                    Site URL: $site_url <br>
                    Resume: <a href='$resume_full_path' download> click here </a> to download the resume.";

       $this->load->library('email');
       // $this->email->from($email);
       // $this->email->to($to_mail);
      // $this->email->subject("Customer enquiry mail of site $site");
       // $this->email->message($email_message);
       //$send = $this->email->send();


require_once (APPPATH.'libraries/vendor/autoload.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/Exception.php');

//PHPMailer Object  
$mail = new PHPMailer\PHPMailer\PHPMailer();

//-------------------- END SMTP --------------------



//From email address and name
$mail->From = $from_email;
$mail->FromName = $site_name;

//To address and name
// $mail->addAddress("recepient1@example.com", "Recepient Name");
$mail->addAddress($to_mail); //Recipient name is optional

//Address to which recipient will reply
$mail->addReplyTo($from_email, "Reply");

//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);
$mail->Sender = $from_email;
$mail->Subject = $subject;
$mail->Body = $email_message;
$mail->addAttachment( $resume_full_path );
// $mail->AltBody = "This is the plain text version of the email content";
$sucess = $mail->send();

       if ($sucess) { 

  $form_data['first_name'] = $this->input->post('first_name', TRUE);
  $form_data['last_name'] = $this->input->post('last_name', TRUE);
  $form_data['email'] = $this->input->post('email', TRUE);
  $form_data['phone_number'] = $this->input->post('phone_number', TRUE);
  $form_data['designation'] = $this->input->post('designation', TRUE);
  $form_data['experience'] = $this->input->post('experience', TRUE);
  $form_data['message'] = $this->input->post('message', TRUE);
  $form_data['enquiry_type'] = $this->input->post('enquiry_type', TRUE);

  $form_data['client_city'] = $city;
  $form_data['client_state'] = $state;
  $form_data['client_country'] = $country;
  $form_data['client_os_browser'] = $os_browser;
  $form_data['client_device'] = $device;
  $form_data['enquired_date'] = date('d-M-Y h:i A');
  $form_data['enquired_string_date'] = strtotime(date('d-M-Y h:i A'));

  $form_data['ip_address'] = $ip_address;
  $form_data['site_url'] = $site_url;

 $result = $this->cmoon_model->insert('careers_form', $form_data);

        $this->session->set_flashdata('mail_success','Thank you for showing your intrest, We will address your request very soon.'); 
        redirect($page_link);
  } else { 

      $this->session->set_flashdata('mail_error','Sorry something went wrong please try again refreshing the page'); 
      redirect($page_link);
        }  }  } 






// ----------------------------------     form details    -------------------------------------------

public function sub_page_contactus_enquiry_form(){

if($this->input->post('g-recaptcha-response', TRUE) == ''){
        $this->session->set_flashdata('mail_error','Invalid capcha please try after refresing the page');  ?>        
          <script> window.location.href = "<?php echo $this->input->post('page_link', TRUE); ?>"; </script>
  <?php  }else{

$ip_address=$_SERVER['REMOTE_ADDR'];

$geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
$addrDetailsArr = unserialize(file_get_contents($geopluginURL)); 


$city = $addrDetailsArr['geoplugin_city']; 
$state = $addrDetailsArr['geoplugin_region']; 
$country = $addrDetailsArr['geoplugin_countryName']; 

$this->load->library('user_agent');

if ($this->agent->is_browser()){
        $agent = $this->agent->browser().' '.$this->agent->version();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "DESKTOP";
}
elseif ($this->agent->is_robot()){
        $agent = $this->agent->robot();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "ROBOT";
}
elseif ($this->agent->is_mobile()){
        $agent = $this->agent->mobile();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "MOBILE";
}
else{
        $agent = 'Unidentified User';
        $os_browser = $agent;
        $device = $agent;
}


  $name = $this->input->post('name', TRUE);
  $email = $this->input->post('email', TRUE);
  $phone_number = $this->input->post('phone_number', TRUE);
  $country_code = $this->input->post('country_code', TRUE);
  $enquired_for_city = $this->input->post('enquired_for_city', TRUE);
  $page_link = $this->input->post('page_link', TRUE);
  $enquiry_type = $this->input->post('enquiry_type', TRUE);
  $project_name = $this->input->post('project_name', TRUE);
  $enquiry_to_company = $this->input->post('enquiry_to_company', TRUE);
  $message = $this->input->post('message', TRUE);

  $phone_number=$country_code." ".$phone_number;

  $client_city = $city;
  $client_state = $state;
  $client_country = $country;

  $client_os_browser = $os_browser;
  $client_device = $device;


  $to_mail = $this->data["site_settings"]->contact_email;
  $from_email = $this->data["site_settings"]->from_email;
  $site_name = $this->data["site_settings"]->site_name;

  $enquired_date = date('d-M-Y h:i A');

$site_url=base_url()."".$page_link;

$resume_full_path = base_url()."cmoon_images/".$resume;

  $subject="$enquiry_type for the project $project_name";
  $email_message = "Hi Sir/Madam, <br>
                        Below are the client details<br><br>
                    Name: $name <br>
                    Email: $email <br>
                    Phone: $phone_number <br>
                    Enquired For City: $enquired_for_city <br>
                    Message: $message <br>
                    Date : $enquired_date <br>
                    Type: $enquiry_type <br>
                    IP-Address: $ip_address <br>
                    Device: $device <br>
                    OS & Browser: $os_browser <br>
                    City: $client_city <br>
                    State: $client_state <br>
                    Country: $client_country <br>
                    Site URL: $site_url <br>
                    Enquired For Project: $project_name <br>
                    Enquiry For Company : $enquiry_to_company";

       $this->load->library('email');
       // $this->email->from($email);
       // $this->email->to($to_mail);
      // $this->email->subject("Customer enquiry mail of site $site");
       // $this->email->message($email_message);
       //$send = $this->email->send();


require_once (APPPATH.'libraries/vendor/autoload.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/Exception.php');

//PHPMailer Object  
$mail = new PHPMailer\PHPMailer\PHPMailer();

//-------------------- END SMTP --------------------



//From email address and name
$mail->From = $from_email;
$mail->FromName = $site_name;

//To address and name
// $mail->addAddress("recepient1@example.com", "Recepient Name");
$mail->addAddress($to_mail); //Recipient name is optional

//Address to which recipient will reply
$mail->addReplyTo($from_email, "Reply");

//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);
$mail->Sender = $from_email;
$mail->Subject = $subject;
$mail->Body = $email_message;
$mail->addAttachment( $resume_full_path );
// $mail->AltBody = "This is the plain text version of the email content";
$sucess = $mail->send();

       if ($sucess) { 

  $form_data['name'] = $this->input->post('name', TRUE);
  $form_data['email'] = $this->input->post('email', TRUE);
  $form_data['phone_number'] = $this->input->post('phone_number', TRUE)." ".$this->input->post('country_code', TRUE);
  $form_data['enquired_for_city'] = $this->input->post('enquired_for_city', TRUE);
  $form_data['message'] = $this->input->post('message', TRUE);
  $form_data['enquiry_type'] = $this->input->post('enquiry_type', TRUE);
  $form_data['project_name'] = $this->input->post('project_name', TRUE);
  $form_data['enquiry_to_company'] = $this->input->post('enquiry_to_company', TRUE);

  $form_data['client_city'] = $city;
  $form_data['client_state'] = $state;
  $form_data['client_country'] = $country;
  $form_data['client_os_browser'] = $os_browser;
  $form_data['client_device'] = $device;
  $form_data['enquired_date'] = date('d-M-Y h:i A');
  $form_data['enquired_string_date'] = strtotime(date('d-M-Y h:i A'));

  $form_data['ip_address'] = $ip_address;
  $form_data['site_url'] = $site_url;

 $result = $this->cmoon_model->insert('sub_page_enquiry_form', $form_data);

        $this->session->set_flashdata('mail_success','Thank you for showing your intrest, We will address your request very soon.'); 
        redirect($page_link);
  } else { 

      $this->session->set_flashdata('mail_error','Sorry something went wrong please try again refreshing the page'); 
      redirect($page_link);
        }  }  } 







// ----------------------------------     form details    -------------------------------------------

public function brochure_enquiry_form(){

if($this->input->post('g-recaptcha-response', TRUE) == ''){
        $this->session->set_flashdata('mail_error','Invalid capcha please try after refresing the page');  ?>        
          <script> window.location.href = "<?php echo $this->input->post('page_link', TRUE); ?>"; </script>
  <?php  }else{

$ip_address=$_SERVER['REMOTE_ADDR'];

$geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
$addrDetailsArr = unserialize(file_get_contents($geopluginURL)); 


$city = $addrDetailsArr['geoplugin_city']; 
$state = $addrDetailsArr['geoplugin_region']; 
$country = $addrDetailsArr['geoplugin_countryName']; 

$this->load->library('user_agent');

if ($this->agent->is_browser()){
        $agent = $this->agent->browser().' '.$this->agent->version();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "DESKTOP";
}
elseif ($this->agent->is_robot()){
        $agent = $this->agent->robot();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "ROBOT";
}
elseif ($this->agent->is_mobile()){
        $agent = $this->agent->mobile();
        $os_browser = $this->agent->platform()." - ".$agent;
        $device = "MOBILE";
}
else{
        $agent = 'Unidentified User';
        $os_browser = $agent;
        $device = $agent;
}


  $name = $this->input->post('name', TRUE);
  $email = $this->input->post('email', TRUE);
  $phone_number = $this->input->post('phone_number', TRUE);
  $page_link = $this->input->post('page_link', TRUE);
  $enquiry_type = $this->input->post('enquiry_type', TRUE);
  $project_name = $this->input->post('project_name', TRUE);
  $enquiry_to_company = $this->input->post('enquiry_to_company', TRUE);


  $client_city = $city;
  $client_state = $state;
  $client_country = $country;

  $client_os_browser = $os_browser;
  $client_device = $device;


  $to_mail = $this->data["site_settings"]->contact_email;
  $from_email = $this->data["site_settings"]->from_email;
  $site_name = $this->data["site_settings"]->site_name;

  $enquired_date = date('d-M-Y h:i A');

$site_url=base_url()."".$page_link;

$resume_full_path = base_url()."cmoon_images/".$resume;

  $subject="$enquiry_type for the project $project_name";
  $email_message = "Hi Sir/Madam, <br>
                        Below are the client details<br><br>
                    Name: $name <br>
                    Email: $email <br>
                    Phone: $phone_number <br>
                    Date : $enquired_date <br>
                    Type: $enquiry_type <br>
                    IP-Address: $ip_address <br>
                    Device: $device <br>
                    OS & Browser: $os_browser <br>
                    City: $client_city <br>
                    State: $client_state <br>
                    Country: $client_country <br>
                    Site URL: $site_url <br>
                    Enquired For Project: $project_name <br>
                    Enquiry For Company : $enquiry_to_company";

       $this->load->library('email');
       // $this->email->from($email);
       // $this->email->to($to_mail);
      // $this->email->subject("Customer enquiry mail of site $site");
       // $this->email->message($email_message);
       //$send = $this->email->send();


require_once (APPPATH.'libraries/vendor/autoload.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/Exception.php');

//PHPMailer Object  
$mail = new PHPMailer\PHPMailer\PHPMailer();

//-------------------- END SMTP --------------------



//From email address and name
$mail->From = $from_email;
$mail->FromName = $site_name;

//To address and name
// $mail->addAddress("recepient1@example.com", "Recepient Name");
$mail->addAddress($to_mail); //Recipient name is optional

//Address to which recipient will reply
$mail->addReplyTo($from_email, "Reply");

//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);
$mail->Sender = $from_email;
$mail->Subject = $subject;
$mail->Body = $email_message;
$mail->addAttachment( $resume_full_path );
// $mail->AltBody = "This is the plain text version of the email content";
$sucess = $mail->send();

       if ($sucess) { 

  $form_data['name'] = $this->input->post('name', TRUE);
  $form_data['email'] = $this->input->post('email', TRUE);
  $form_data['phone_number'] = $this->input->post('phone_number', TRUE);
  $form_data['enquiry_type'] = $this->input->post('enquiry_type', TRUE);
  $form_data['project_name'] = $this->input->post('project_name', TRUE);
  $form_data['enquiry_to_company'] = $this->input->post('enquiry_to_company', TRUE);

  $form_data['client_city'] = $city;
  $form_data['client_state'] = $state;
  $form_data['client_country'] = $country;
  $form_data['client_os_browser'] = $os_browser;
  $form_data['client_device'] = $device;
  $form_data['enquired_date'] = date('d-M-Y h:i A');
  $form_data['enquired_string_date'] = strtotime(date('d-M-Y h:i A'));

  $form_data['ip_address'] = $ip_address;
  $form_data['site_url'] = $site_url;


 $result = $this->cmoon_model->insert('brochure_enquiry_form', $form_data);

        $this->session->set_flashdata('broucher_mail_success','Thank you for showing your intrest, We will address your request very soon.'); 
        redirect($page_link);
  } else { 

      $this->session->set_flashdata('mail_error','Sorry something went wrong please try again refreshing the page'); 
      redirect($page_link);
        }  }  } 



















  




function convertCurrencyforcookie($number)
{
  
    // Convert Price to Crores or Lakhs or Thousands
    $length = strlen($number);
    $currency = '';

    if($length == 4 || $length == 5)
    {
        // Thousand
        $number = $number / 1000;
        $number = round($number,2);
        $ext = "Thousand";
        $currency = $number." ".$ext;
    }
    elseif($length == 6 || $length == 7)
    {
        // Lakhs
        $number = $number / 100000;
        $number = round($number,2);
        $ext = "Lac";
        $currency = $number." ".$ext;

    }
    elseif($length == 8 || $length == 9)
    {
        // Crores
        $number = $number / 10000000;
        $number = round($number,2);
        $ext = "Cr";
        $currency = $number.' '.$ext;
    }

    return $currency;
}






















































































































    public function management()
    {
        $this->data["meta_tags"]  = $this->site_model->get_cms('social_links', '1');
        $this->load->view('includes/header', $this->data);
        $data["cms_pages5"]  = $this->site_model->get_cms('cms_pages', '5');
        $this->load->view('management', $data);
        $this->load->view('includes/footer');
    }

    public function testimonial()
    {
        $this->data["meta_tags"]  = $this->site_model->get_cms('social_links', '1');
        $this->load->view('includes/header', $this->data);
        $data["cms_pages6"]  = $this->site_model->get_cms('cms_pages', '6');
        $data['testimonials'] = $this->site_model->get('testimonials');
        $this->load->view('testimonial', $data);
        $this->load->view('includes/footer');
    }


    public function useful_information($p_link)
    {
        $this->data["meta_tags"]  = $this->site_model->get_plink('useful_information',$p_link);
        $this->load->view('includes/header', $this->data);
        $data["cms_pages9"]  = $this->site_model->get_cms('cms_pages', '9');
        $data['useful_information'] = $this->site_model->get_plink('useful_information',$p_link);
        $this->load->view('useful_information', $data);
        $this->load->view('includes/footer');
    }


    public function student_rent()
    {
        $this->data["meta_tags"]  = $this->site_model->get_cms('social_links', '1');
        $this->load->view('includes/header', $this->data);
        $data["cms_pages11"]  = $this->site_model->get_cms('cms_pages', '11');

        $perpage       = 12;
        $total_records = count($this->site_model->get_student_rent('property_listings'));
        $pagination = my_pagination('view/student_rent' . '', $perpage, $total_records);
        $data['pagination']  = $pagination['pagination'];

        $data['property_listings'] = $this->site_model->get_student_rent('property_listings',$perpage);
        // $data['useful_information'] = $this->site_model->get_plink('useful_information',$p_link);
        $this->load->view('student_rent', $data);
        $this->load->view('includes/footer');
    }


    public function property_detail($p_link)
    {
        $this->data["meta_tags"]  = $this->site_model->get_plink('property_listings',$p_link);

        $this->load->view('includes/header', $this->data);
        $data["cms_pages12"]  = $this->site_model->get_cms('cms_pages', '12');
        $data["cms_pages13"]  = $this->site_model->get_cms('cms_pages', '13');

        $data["property_listings"]  = $this->site_model->get_plink('property_listings',$p_link);
        $property_listings_id=$data["property_listings"]->id;

        $data["property_listings_images"]  = $this->site_model->getby_cat_id('property_listings_images', $property_listings_id, 'property_listings_id');
        $data["property_listings_quick_summary"]  = $this->site_model->getby_cat_id('property_listings_quick_summary', $property_listings_id, 'property_listings_id');
        $data['property_listings_quick_summary_count']=count($this->site_model->getby_cat_id('property_listings_quick_summary', $property_listings_id, 'property_listings_id'));

        $data["property_listings_features"]  = $this->site_model->getby_cat_id('property_listings_features', $property_listings_id, 'property_listings_id');
        $data['property_listings_features_count']=count($this->site_model->getby_cat_id('property_listings_features', $property_listings_id, 'property_listings_id'));

        $data['property_listings_similar'] = $this->site_model->get_where_notequalto('property_listings', $property_listings_id);

        $this->load->view('property_detail', $data);
        $this->load->view('includes/footer');
    }


    public function contact()
    {
        $this->data["meta_tags"]  = $this->site_model->get_cms('social_links', '1');

        $this->load->view('includes/header', $this->data);
        $data["cms_pages10"]   = $this->site_model->get_cms('cms_pages', '10');
        $this->load->view('contact', $data);
        $this->load->view('includes/footer');
    }

    public function user_login()
    {
        $this->data["meta_tags"]  = $this->site_model->get_cms('social_links', '1');

        $this->load->view('includes/header', $this->data);
        $this->load->view('login', $data);
        $this->load->view('includes/footer');
    }

    public function user_form_data()
    {
       
        $form_data = $this->input->post();


    $email_count=count($this->site_model->get_by_data('user_data',$form_data['email'],'email'));

    if($email_count == 0){

$mobile_count=count($this->site_model->get_by_data('user_data',$form_data['mobile_number'],'mobile_number'));

    if($mobile_count == 0){

        $user_data['first_name']= $form_data['first_name'];
        $user_data['last_name']= $form_data['last_name'];
        $user_data['email']= $form_data['email'];
        $user_data['phone_number']= $form_data['phone_number'];
        $user_data['mobile_number']= $form_data['mobile_number'];
        $user_data['password']= sha1($form_data['password']);

        if($form_data['check-box'] == 'yes'){
            $user_data['receive_newsletter']='yes';
        }else{
            $user_data['receive_newsletter']='No';
        }   

         $seed = str_split('abcdefghijklmnopqrstuvwxyz'
                 .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                 .'0123456789'); // and any other characters
        shuffle($seed); // probably optional since array_is randomized; this may be redundant
        $rand = '';
        foreach (array_rand($seed, 7) as $k) 
          $rand .= $seed[$k]; 

        $user_data['user_token']=$rand;
        
        $result = $this->cmoon_model->insert('user_data', $user_data);

         if ($result == true) {
                 $this->session->set_flashdata('login_success','You have successfully registered, you can login now');

                redirect('view/user_login');
            } else {
                 $this->session->set_flashdata('login_error','Something went wrong please refresh the page and try again');
                redirect('user_login');
            }

        }else{
            $this->session->set_flashdata('login_error','User with same Mobile Number exists, please register with different mobile number');
            redirect('user_login');
        }

      }else{
            $this->session->set_flashdata('login_error','User with same Email exists, please register with different Email-Id');
            redirect('view/user_login');
        }

    }



    public function user_login_form()
    {
        if($this->input->post()){
        $email=$this->input->post('email');
        $password=sha1($this->input->post('password'));
        $row=$this->site_model->user_login('user_data',$email,$password);
            if($row != ""){
                $sess_arr=array(
                'user_id'=>$row->id,
                'user_token'=>$row->user_token,
                'user_name'=>$row->first_name,
                'user_email'=>$row->email,
                );

                $data=$this->session->set_userdata('user_session_data',$sess_arr);
                $form_data['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $form_data['user_id'] = $row->id;
                $form_data['user_name'] = $row->first_name;
                $form_data['login_time'] = date('d-M-Y h:i A');
                $result = $this->cmoon_model->insert('user_logs', $form_data);
                redirect('dashboard');
        }else{
            $this->session->set_flashdata('login_error','Email or Password is not correct');
            redirect('user_login');
        }

        }else{
            $this->load->view('index',$this->data);
        }
    }



    public function logout()
    {
        if($this->session->userdata('user_session_data')){
                $this->session->unset_userdata('user_session_data');
                $this->session->set_flashdata('login_success','You Have loged our successfully');
                redirect('user_login');
        }else{
            redirect('index');
        }
    }


    public function dashboard()
    {
        $this->data["meta_tags"]  = $this->site_model->get_cms('social_links', '1');
        if($this->session->userdata('user_session_data')){
            $this->load->view('includes/header', $this->data);
            $session_data=$this->session->userdata('user_session_data');
            $data["user_data"] = $this->site_model->get_by_id('user_data', $session_data['user_id']);
            $this->load->view('profile', $data);
            $this->load->view('includes/footer');

        }else{
            redirect('index');
        }
    }




    public function user_profile_details_form()
    {
       
    $form_data = $this->input->post();

     $session_data=$this->session->userdata('user_session_data');

    $email_count=count($this->site_model->get_by_data_where_not_id('user_data',$form_data['email'],'email',  $session_data['user_id']));

    if($email_count == 0){

        $mobile_count=count($this->site_model->get_by_data_where_not_id('user_data',$form_data['mobile_number'],'mobile_number', $session_data['user_id']));

    if($mobile_count == 0){

        $user_data['first_name']= $form_data['first_name'];
        $user_data['last_name']= $form_data['last_name'];
        $user_data['email']= $form_data['email'];
        $user_data['phone_number']= $form_data['phone_number'];
        $user_data['mobile_number']= $form_data['mobile_number'];
        $user_data['skype']= $form_data['skype'];
        $user_data['about']= $form_data['about'];

        $result = $this->cmoon_model->update('user_data',$session_data['user_id'],$user_data);

         if ($result == true) {
                $this->session->set_flashdata('login_success','Your details have been updated successfully');
                redirect('dashboard');
            } else {
                 $this->session->set_flashdata('login_error','Something went wrong please refresh the page and try again');
                redirect('dashboard');
            }

        }else{
            $this->session->set_flashdata('login_error','User with same Mobile Number exists, please enter a different mobile number');
            redirect('dashboard');
        }

      }else{
            $this->session->set_flashdata('login_error','User with same Email exists, please enter with different Email-Id');
            redirect('dashboard');
        }

    }



    public function user_profile_social_links_form()
    {
       
    $form_data = $this->input->post();

     $session_data=$this->session->userdata('user_session_data');

        $user_data['facebook']= $form_data['facebook'];
        $user_data['twitter']= $form_data['twitter'];
        $user_data['instagram']= $form_data['instagram'];
        $user_data['linkedin']= $form_data['linkedin'];

        $result = $this->cmoon_model->update('user_data',$session_data['user_id'],$user_data);

         if ($result == true) {
                $this->session->set_flashdata('login_success','Your social links have been updated successfully');
                redirect('dashboard');
            } else {
                 $this->session->set_flashdata('login_error','Something went wrong please refresh the page and try again');
                redirect('dashboard');
            }
    }



    public function user_profile_password_form()
    {

    $form_data = $this->input->post();

     $session_data=$this->session->userdata('user_session_data');

    $check_data['old_password']= sha1($form_data['old_password']);

    $row_count=count($this->site_model->get_by_data_where_id('user_data',$check_data['old_password'],'password',$session_data['user_id']));

    if($row_count > 0){

        $user_data['password']= sha1($form_data['password']);

        $result = $this->cmoon_model->update('user_data',$session_data['user_id'],$user_data);

         if ($result == true) {
                $this->session->set_flashdata('login_success','Your password have been updated successfully');
                redirect('dashboard');
            } else {
                 $this->session->set_flashdata('login_error','Something went wrong please refresh the page and try again');
                redirect('dashboard');
            }
        }else{
            $this->session->set_flashdata('login_error','Current password doesnot match');
            redirect('dashboard');
        }
    }


    


    public function user_profile_image_form()
    {

         if ($_FILES['image']['name'] != '') {
    
     $session_data=$this->session->userdata('user_session_data');

        if ($_FILES['image']['name'] != '') {
            $user_data['image'] = $this->upload_file('image');
        }
        $result = $this->cmoon_model->update('user_data',$session_data['user_id'],$user_data);

         if ($result == true) {
                $this->session->set_flashdata('login_success','Your image have been updated successfully');
                redirect('dashboard');
            } else {
                 $this->session->set_flashdata('login_error','Something went wrong please refresh the page and try again');
                redirect('dashboard');
            }
        }else{
             $this->session->set_flashdata('login_error','Please select an image');
                redirect('dashboard');
        }
    }


    public function saved_properties_inner()
    {
        $this->data["meta_tags"]  = $this->site_model->get_cms('social_links', '1');

        if($this->session->userdata('user_session_data')){
            $this->load->view('includes/header', $this->data);
            $session_data=$this->session->userdata('user_session_data');
            $data["saved_properties"] = $this->site_model->get_by_userid('saved_properties', $session_data['user_id']);
            foreach ($data["saved_properties"] as $rows) {
                $data["properties"][] = $this->site_model->get_by_propertyid('property_listings', $rows->property_listings_id);
            }

            $this->load->view('saved_properties_inner', $data);
            $this->load->view('includes/footer');
        }else{
            redirect('index');
        }
    }


    public function saved_searches()
    {
        $this->data["meta_tags"]  = $this->site_model->get_cms('social_links', '1');

        if($this->session->userdata('user_session_data')){
            $this->load->view('includes/header', $this->data);
             $session_data=$this->session->userdata('user_session_data');

        $perpage = 12;
        $total_records = count($this->site_model->get_by_userid('saved_searches', $session_data['user_id']));
        $pagination = my_pagination('view/saved_searches' . '', $perpage, $total_records);
        $data['pagination']  = $pagination['pagination'];

            $data["saved_properties"] = $this->site_model->get_by_userid('saved_searches', $session_data['user_id'], $perpage);
            foreach ($data["saved_properties"] as $rows) {
                $data["properties"][] = $this->site_model->get_by_propertyid('property_listings', $rows->property_listings_id);
            }


            $this->load->view('saved_searches', $data);
            $this->load->view('includes/footer');
        }else{
            redirect('index');
        }
    }


    public function student_rent_search()
    {
        $this->load->view('includes/header', $this->data);
        $data["cms_pages11"]  = $this->site_model->get_cms('cms_pages', '11');

        $perpage = 12;
        $total_records = count($this->site_model->get_student_rent_search('property_listings'));
        $pagination = my_pagination('view/student_rent' . '', $perpage, $total_records);
        $data['pagination']  = $pagination['pagination'];

        $data['property_listings'] = $this->site_model->get_student_rent_search('property_listings',$perpage);
        $session_data=$this->session->userdata('user_session_data');

        if($this->session->userdata('user_session_data')){

            foreach ($data['property_listings'] as $listings) {
        
              $search_count = count($this->site_model->get_where_property_listings_id('saved_searches',$listings->id,$session_data['user_id']));
                if($search_count == 0){
                        $saved_data['property_listings_id']=$listings->id;
                        $saved_data['user_id']=$session_data['user_id'];
                        $saved_data['saved_time']=date('d-M-Y h:i A');
                        $saved_data['string_date']=strtotime(date('d-M-Y h:i A'));
                        $result = $this->site_model->insert('saved_searches', $saved_data);
                }   
            }
        }
        // $data['useful_information'] = $this->site_model->get_plink('useful_information',$p_link);
        $this->load->view('student_rent_search', $data);
        $this->load->view('includes/footer');
    }





    public function user_forgot_password()
    {
        $this->data["meta_tags"]  = $this->site_model->get_cms('social_links', '1');
        $this->load->view('includes/header', $this->data);
        $this->load->view('user_forgot_password', $data);
        $this->load->view('includes/footer');
    }




    function user_forgot_password_submit(){
        $form_data=$this->input->post();
        $result = $this->login_model->get_user_email('user_data',$form_data['email']);
        if($result > 0){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr(str_shuffle($chars), 0, 10);
        $password1 = sha1($password);

        $result_psw = $this->login_model->update_password('user_data',$form_data['email'], $password1);
        if($result_psw == true){

            $data['user_details'] = $this->login_model->get_user_details('user_data',$form_data['email']);

$client_email =$data['user_details']->email;

       $to_mail = $this->data["site_settings"]->email;
       $from_email = $this->data["site_settings"]->from_email;
       $site_name = $this->data["site_settings"]->site_name;

       $email_message = "
      Hi ,<br>
      Your Password of the site $site_name has been reset as per your request  <br>
      Your reset password is : $password <br>
      ";
       //echo $message;exit;


    $this->load->library('email');
       // $this->email->from($email);
       // $this->email->to($to_mail);
      // $this->email->subject("Customer enquiry mail of site $site");
       // $this->email->message($email_message);
       //$send = $this->email->send();
require_once (APPPATH.'libraries/vendor/autoload.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/PHPMailer.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/SMTP.php');
require_once (APPPATH.'libraries/vendor/phpmailer/phpmailer/src/Exception.php');
//PHPMailer Object  
$mail = new PHPMailer\PHPMailer\PHPMailer();
//-----------Enable SMTP debugging. ---------------
// $mail->SMTPDebug = 3;                               
// //Set PHPMailer to use SMTP.
// $mail->isSMTP();            
// //Set SMTP host name                          
// $mail->Host = "cmdemo.in";
// //Set this to true if SMTP host requires authentication to send email
// $mail->SMTPAuth = true;                          
// //Provide username and password     
// $mail->Username = "hyd@cmdemo.in";                 
// $mail->Password = "hyd@123";                           
// //If SMTP requires TLS encryption then set it
// $mail->SMTPSecure = "tls";                           
// //Set TCP port to connect to 
// $mail->Port = 465; 
// $client_email=$email;
//-------------------- END SMTP --------------------
//From email address and name
$mail->From = $from_email;
$mail->FromName = $site_name;
//To address and name
// $mail->addAddress("recepient1@example.com", "Recepient Name");
$mail->addAddress($client_email); //Recipient name is optional
//Address to which recipient will reply
$mail->addReplyTo($from_email, "Reply");
//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");
//Send HTML or Plain Text email
$mail->isHTML(true);
$mail->Sender = $from_email;
$mail->Subject = "Rest password of the site $site";
$mail->Body = $email_message;
// $mail->AltBody = "This is the plain text version of the email content";
$sucess = $mail->send();



       // $this->load->library('email');
       // $this->email->from($from_email);
       // $this->email->to($to_mail);
       // $this->email->subject("Rest password of the site $site");
       // $this->email->message($email_message);
       //  $email_from = $from_email;
       //  $headers = "MIME-Version: 1.0" . "\r\n";
       //  $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
       //  $headers .= 'From: '.$email_from. "\r\n";
       //  $headers .= 'Reply-To: '.$email_from. "\r\n";
       //  $mail=mail($to_mail, $site ,$message, $headers);
       //$send = $this->email->send();
       if ($sucess) {
            $this->session->set_flashdata('login_success','Your default password has been sent to the registered Mail-Id');
            redirect('user_forgot_password');
       } else {
            $this->session->set_flashdata('login_error','Unable to reset password please try after refresing the page');
            redirect('user_login');
       }
             }else{
               $this->session->set_flashdata('login_error','unable to reset password please try after some time');
                redirect('user_login');
             }
        }else{
            $this->session->set_flashdata('login_error','Email-Id does not match with the registered Email-Id');
           redirect('user_login');
       }
    }






















    public function gallery_products()
    {
        $this->load->view('includes/header', $this->data);
        $data["cms_pages4"]   = $this->site_model->get_cms('cms_pages', '4');
        $data['products_images'] = $this->site_model->get('products_images');
        $this->load->view('gallery_products', $data);
        $this->load->view('includes/footer');
    }

    public function gallery_soft_skills()
    {
        $this->load->view('includes/header', $this->data);
        $data["cms_pages5"]   = $this->site_model->get_cms('cms_pages', '5');
        $data['soft_skills_images'] = $this->site_model->get('soft_skills_images');
        $this->load->view('gallery_soft_skills', $data);
        $this->load->view('includes/footer');
    }

    public function corporate_support($p_link)
    {
        $this->load->view('includes/header', $this->data);
        $data["cms_pages6"]   = $this->site_model->get_cms('cms_pages', '6');
        $data["corporate_support"]  = $this->site_model->get_plink('corporate_support',$p_link);
        $corporate_support_id=$data["corporate_support"]->id;
        $data["corporate_support_images"]  = $this->site_model->getbyid_corporate_support_images('corporate_support_images', $corporate_support_id);
        $data["corporate_support_pdf"]  = $this->site_model->getbyid_corporate_support_pdf('corporate_support_pdf', $corporate_support_id);
        $this->load->view('corporate_support', $data);
        $this->load->view('includes/footer');
    }

    public function education_services($p_link)
    {
        $this->load->view('includes/header', $this->data);
        $data["cms_pages7"]   = $this->site_model->get_cms('cms_pages', '7');
        $data["education_services"]  = $this->site_model->get_plink('education_services',$p_link);
        $education_services_id=$data["education_services"]->id;
        $data["education_services_images"]  = $this->site_model->getbyid_education_services_images('education_services_images', $education_services_id);
        $data["education_services_pdf"]  = $this->site_model->getbyid_education_services_pdf('education_services_pdf', $education_services_id);
        $this->load->view('education_services', $data);
        $this->load->view('includes/footer');
    }

    public function medical_certified_products($p_link)
    {
        $this->load->view('includes/header', $this->data);
        $data["cms_pages8"]   = $this->site_model->get_cms('cms_pages', '8');
        $data["medical_certified_products"]  = $this->site_model->get_plink('medical_certified_products',$p_link);
        $medical_certified_products_id=$data["medical_certified_products"]->id;
        $data["medical_certified_products_images"]  = $this->site_model->getbyid_medical_certified_products_images('medical_certified_products_images', $medical_certified_products_id);
        $data["medical_certified_products_pdf"]  = $this->site_model->getbyid_medical_certified_products_pdf('medical_certified_products_pdf', $medical_certified_products_id);
        $this->load->view('medical_certified_products', $data);
        $this->load->view('includes/footer');
    }

    public function community_products($p_link)
    {
        $this->load->view('includes/header', $this->data);
        $data["cms_pages9"]   = $this->site_model->get_cms('cms_pages', '9');
        $data["community_products"]  = $this->site_model->get_plink('community_products',$p_link);
        $community_products_id=$data["community_products"]->id;
        $data["community_products_images"]  = $this->site_model->getbyid_community_products_images('community_products_images', $community_products_id);
        $data["community_products_pdf"]  = $this->site_model->getbyid_community_products_pdf('community_products_pdf', $community_products_id);
        $this->load->view('community_products', $data);
        $this->load->view('includes/footer');
    }

    public function import_export_services($p_link)
    {
        $this->load->view('includes/header', $this->data);
        $data["cms_pages10"]   = $this->site_model->get_cms('cms_pages', '10');
        $data["import_export_services"]  = $this->site_model->get_plink('import_export_services',$p_link);
        $import_export_services_id=$data["import_export_services"]->id;
        $data["import_export_services_images"]  = $this->site_model->getbyid_import_export_services_images('import_export_services_images', $import_export_services_id);
        $data["import_export_services_pdf"]  = $this->site_model->getbyid_import_export_services_pdf('import_export_services_pdf', $import_export_services_id);
        $this->load->view('import_export_services', $data);
        $this->load->view('includes/footer');
    }

    public function electrical_automation($p_link)
    {
        $this->load->view('includes/header', $this->data);
        $data["cms_pages11"]   = $this->site_model->get_cms('cms_pages', '11');
        $data["electrical_automation"]  = $this->site_model->get_plink('electrical_automation',$p_link);
        $electrical_automation_id=$data["electrical_automation"]->id;
        $data["electrical_automation_images"]  = $this->site_model->getbyid_electrical_automation_images('electrical_automation_images', $electrical_automation_id);
        $data["electrical_automation_pdf"]  = $this->site_model->getbyid_electrical_automation_pdf('electrical_automation_pdf', $electrical_automation_id);
        $this->load->view('electrical_automation', $data);
        $this->load->view('includes/footer');
    }

    public function smart_home_appliance($p_link)
    {
        $this->load->view('includes/header', $this->data);
        $data["cms_pages12"]   = $this->site_model->get_cms('cms_pages', '12');
        $data["smart_home_appliance"]  = $this->site_model->get_plink('smart_home_appliance',$p_link);
        $smart_home_appliance_id=$data["smart_home_appliance"]->id;
        $data["smart_home_appliance_images"]  = $this->site_model->getbyid_smart_home_appliance_images('smart_home_appliance_images', $smart_home_appliance_id);
        $data["smart_home_appliance_pdf"]  = $this->site_model->getbyid_smart_home_appliance_pdf('smart_home_appliance_pdf', $smart_home_appliance_id);
        $this->load->view('smart_home_appliance', $data);
        $this->load->view('includes/footer');
    }




// ---------------------------------------------------------------------   Testimonials code    -------------------------------------------

    public function testimonials()
    {
        if ($this->input->post()) {
            $form_data = $this->input->post();
        $phone_count = count($this->site_model->get_testimonials_byphone('testimonials', $form_data['phone']));
        if($phone_count > 0){
            redirect('index/t_phone_error');
         }else{
             $email_count = count($this->site_model->get_testimonials_byemail('testimonials', $form_data['email']));
             if($email_count > 0){
                    redirect('index/t_email_error');
                }else{
                if ($_FILES['image']['name'] != '') {
                    $form_data['image'] = $this->upload_file('image');
                }
                $result = $this->cmoon_model->insert('testimonials', $form_data);
                if ($result == true) {
                    redirect('view/index/t_Success');
                } else {
                    redirect('index/t_error');
                }
            }
        }
        } else {
            redirect('index/error');
        }
    }








// ---------------------------- Image upload code -----------------------------------------------------------------



    private function upload_file($file_name, $path = null)

    {
        $upload_path1             = "cmoon_images".$path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "gif|jpg|png|jpeg";
        $config1['max_size']      = "100480000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     = date("YmdHis") . rand(0, 999) . "_" . $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }

    // ------------------------------------------------------------------------------- pdf upload code -----------------------------------------------------------------



    private function upload_file_brochure($file_name, $path = null)

    {


        $upload_path1             = "cmoon_images" . $path;
        $config1['upload_path']   = $upload_path1;
        $config1['allowed_types'] = "pdf|dox|docx";
        $config1['max_size']      = "100480000";
        $img_name1                = strtolower($_FILES[$file_name]['name']);
        $img_name1                = preg_replace('/[^a-zA-Z0-9\.]/', "_", $img_name1);
        $config1['file_name']     = date("YmdHis") . rand(0, 999) . "_" . $img_name1;
        $this->load->library('upload', $config1);
        $this->upload->initialize($config1);
        $this->upload->do_upload($file_name);
        $fileDetailArray1 = $this->upload->data();
        return $fileDetailArray1['file_name'];
    }








}

