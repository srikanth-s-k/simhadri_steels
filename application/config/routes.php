<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'view';
$route['index'] = 'view/index';
$route['contact'] = 'view/contact';








$route['sub-page/(:any)'] = 'view/sub_page/$1';
















$route['home_loans'] = 'view/home_loans';
$route['news'] = 'view/news';
$route['news_details/(:any)'] = 'view/news_details/$1';

$route['careers'] = 'view/careers';
$route['error_page'] = 'view/error_page';
$route['contact_us'] = 'view/contact_us';
$route['search_landing'] = 'view/search_landing';
$route['privacy_policy'] = 'view/privacy_policy';
$route['terms_and_conditions'] = 'view/terms_and_conditions';
$route['disclaimer'] = 'view/disclaimer';
$route['sub_page/(:any)'] = 'view/sub_page/$1';
$route['micro_site/(:any)'] = 'view/micro_site/$1';
$route['builder_landing/(:any)'] = 'view/builder_landing/$1';
$route['offer_zone/(:any)'] = 'view/offer_zone/$1';
$route['latest_projects'] = 'view/recently_add_projects';



$route['sub_page_enquiry_form'] = 'view/sub_page_enquiry_form';
$route['offer_zone_enquiry_form'] = 'view/offer_zone_enquiry_form';
$route['home_loan_know_more_form'] = 'view/home_loan_know_more_form';
$route['business_enquiry_form'] = 'view/business_enquiry_form';
$route['customer_enquiry_form'] = 'view/customer_enquiry_form';
$route['contact_us_form'] = 'view/contact_us_form';
$route['careers_form'] = 'view/careers_form';
$route['sub_page_contactus_enquiry_form'] = 'view/sub_page_contactus_enquiry_form';
$route['brochure_enquiry_form'] = 'view/brochure_enquiry_form';





$route['management'] = 'view/management';
$route['testimonial'] = 'view/testimonial';
$route['useful_information/(:any)'] = 'view/useful_information/$1';
$route['student_rent'] = 'view/student_rent';
$route['property_detail/(:any)'] = 'view/property_detail/$1';
$route['user_login'] = 'view/user_login';
$route['profile'] = 'view/profile';
$route['dashboard'] = 'view/dashboard';
$route['saved_properties_inner'] = 'view/saved_properties_inner';
$route['saved_searches'] = 'view/saved_searches';
$route['student_rent_search'] = 'view/student_rent_search';
$route['user_forgot_password'] = 'view/user_forgot_password';

// $route['contactus/(:any)/(:any)'] = 'view/contactus/$1/$2';

$route['404_override'] = 'view/error';
$route['translate_uri_dashes'] = FALSE;