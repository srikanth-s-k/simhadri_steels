<?php  class Cmoon_model extends CI_Model

{
    // echo $this->db->last_query(); exit;     to see the qurey executing
  function get_cms($table,$id)
  {
   $this->db->where('id',$id);
  $query=$this->db->get($table);
  return $query->row();
  }

  function update_cms($table,$id,$data)
  {
    $this->db->update($table, $data, array('id' => $id));
     return true;
  }

    function get($table, $perpage = null)
  {

    $limit = "";
        if (isset($_GET['page'])) {
            if ($_GET['page'] != '' || $_GET['page'] > 0) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $this->db->limit($perpage, (($page - 1) * $perpage));
        } else {
            $page = 1;
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }


        $query=$this->db->get($table);
        return $query->result();
  }



      function get_order_by($table,$coloumn,$sequence)
    {
        $this->db->order_by($coloumn, $sequence);
        $query=$this->db->get($table);
        return $query->result();
    }





  function insert($table,$data)
  {
  $query=$this->db->insert($table,$data);
  return true;
   
  }

  function get_row($table,$id)
  {
    $this->db->where('id',$id);
    $query=$this->db->get($table);
    return $query->result();
  }

  function get_single_row_by_id($table,$id)
  {
    $this->db->where('id',$id);
    $query=$this->db->get($table);
    return $query->row();
    echo $this->db->last_query();  die();
  }


    function update($table,$id,$data)
  {
    $this->db->update($table, $data, array('id' => $id));
      // echo $this->db->last_query();  die();
     return true;
  }

 function delete($table,$id)
  {
  $result=$this->db->delete($table, array('id' => $id));
     return true;
  }




  function get_subcat($table,$sub_cat_id,$sub_cat_id_name,$perpage = null)
  {

        $limit = "";
        if (isset($_GET['page'])) {
            if ($_GET['page'] != '' || $_GET['page'] > 0) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $this->db->limit($perpage, (($page - 1) * $perpage));
        } else {
            $page = 1;
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }

    $this->db->where($sub_cat_id_name,$sub_cat_id);
    $query=$this->db->get($table);
    return $query->result();
  }



    function get_result_cat($table,$cat_id,$cat_name,$perpage = null){
      $limit = "";
        if (isset($_GET['page'])) {
            if ($_GET['page'] != '' || $_GET['page'] > 0) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $this->db->limit($perpage, (($page - 1) * $perpage));
        } else {
            $page = 1;
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }

        if($table == 'cars_on_road_prices'){
              $this->db->order_by('city_id', 'ASC');
        }

    $this->db->where($cat_name,$cat_id);
    $query=$this->db->get($table);
    return $query->result();
  }


  public function get_row_by_id($table, $id, $coloum)
    {
        $this->db->where($coloum, $id);
        $query = $this->db->get($table);
        return $query->row();
  // echo $this->db->last_query(); exit;
    }




  public function get_result_by_id($table, $coloum_name,$coumn_value)
    {
    
        $this->db->where($coloum_name, $coumn_value);
        $query = $this->db->get($table);
       return  $query->result();
//   echo $this->db->last_query(); exit;
    }

















    function get_gallery_photos($table,$gallery_id)
  {
    $this->db->where('gallery_id',$gallery_id);
    $query=$this->db->get($table);
    return $query->result();
  }


// ------------------------------------------------------------ change password code --------------------------------------------------

    function get_psw($table,$id,$data)
  {
    $this->db->where('id',$id);
    $this->db->where('admin_psw',$data);
    $query=$this->db->get($table);
    return $query->num_rows();
  }


    function update_psw($table,$id,$data)
  {
    $this->db->set('admin_psw', $data);
    $this->db->update($table, array('id' => $id));
     return true;
  }
}