<?php class site_model extends CI_Model

{

        // echo $this->db->last_query(); exit;


    public function get_cms($table, $id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        return $query->row();
    }



    public function get($table, $perpage = null)
    {
    	$limit = "";
        if (isset($_GET['page'])) {
            if ($_GET['page'] != '' || $_GET['page'] > 0) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $this->db->limit($perpage, (($page - 1) * $perpage));
        } else {
            $page = 1;
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $query = $this->db->get($table);
        return $query->result();
    }


    public function get_id_notequalto($table,$col_id ,$col_value,$col_id2,$col_value2)
    {

        $this->db->where($col_value." !=", $col_id);
        $this->db->where($col_value2, $col_id2);
        $query = $this->db->get($table);
        return $query->result();
         // echo $this->db->last_query(); exit;
    }

    public function get_id_notequalto_price($table,$col_id ,$col_value,$colm2,$colm3)
    {

        $this->db->where($col_value." !=", $col_id);
        $this->db->where('unit_starting_price >', $colm2);
        $this->db->where('unit_ending_price <', $colm3);
        $query = $this->db->get($table);
        return $query->result();
         // echo $this->db->last_query(); exit;
    }

    public function get_orderby($table,$col)
    {
        $this->db->order_by($col, 'DESC');
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_orderby_asc($table,$col)
    {
        $this->db->order_by($col, 'ASC');
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_pagination($table, $perpage = null)
    {
        $limit = "";
        if (isset($_GET['page'])) {
            if ($_GET['page'] != '' || $_GET['page'] > 0) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $this->db->limit($perpage, (($page - 1) * $perpage));
        } else {
            $page = 1;
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $query = $this->db->get($table);
        return $query->result();
    }

    public function get_pagination_desc($table, $perpage = null)
    {
        $limit = "";
        if (isset($_GET['page'])) {
            if ($_GET['page'] != '' || $_GET['page'] > 0) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $this->db->limit($perpage, (($page - 1) * $perpage));
        } else {
            $page = 1;
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($table);
        return $query->result();
    }


    public function get_by_limit_desc($table, $limit)
    {
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit);
        $query = $this->db->get($table);
        return $query->result();
    }


    public function get_by_id_limit_desc($table,$colm, $value, $limit)
    {
        $this->db->where($colm, $value);
        $this->db->order_by('unit_starting_price', 'DESC');
        $this->db->limit($limit);
        $query = $this->db->get($table);
        return $query->result();
    }



    function insert($table,$data)

      {

      $query=$this->db->insert($table,$data);

      return true;

      }



    public function get_by_id($table, $id, $coloum)
    {
        $this->db->where($coloum, $id);
        $query = $this->db->get($table);
        return $query->result();
  // echo $this->db->last_query(); exit;
    }


    public function get_by_id_by_colum($table, $id, $coloum)
    {   
        $this->db->select('id');
        $this->db->where($coloum, $id);
        $query = $this->db->get($table);
        return $query->result();
  // echo $this->db->last_query(); exit;
    }




    public function get_row_by_id($table, $id, $coloum)
    {
        $this->db->where($coloum, $id);
        $query = $this->db->get($table);
        return $query->row();
  // echo $this->db->last_query(); exit;
    }

    public function get_by_p_link($table, $p_link)

    {
        $this->db->where('p_link', $p_link);
        $query = $this->db->get($table);
        return $query->row();
    }


    public function projects_by_type($table,$col_value,$col_name,$city_id)
    {

        $this->db->where('city_id', $city_id);
        $this->db->where($col_name, $col_value);

        $this->db->order_by('home_page_priority', 'ASC');
        $this->db->limit('10');

        $query = $this->db->get($table);
        return $query->result();

    }


    public function get_offer_zone($table,$col_value,$col_name,$city_id)
    {
        $this->db->where('city_id', $city_id);
        $this->db->where('offer_zone', 'Yes');
        $this->db->where($col_name, $col_value);
        $query = $this->db->get($table);
        return $query->result();
    }




    public function get_by_id_col2($table, $id, $coloum1,$value,$coloum2)
    {
        $this->db->where($coloum1, $id);
        $this->db->where($coloum2, $value);
        $query = $this->db->get($table);
        return $query->result();
        // echo $this->db->last_query(); exit;
    }



    public function projects_by_property_type($table, $group_by_col)
    {
        $this->db->group_by($group_by_col);
        $query = $this->db->get($table);
        return $query->result();
        // echo $this->db->last_query(); exit;
    }




    public function get_by_coloum_like($table, $value,  $coloumn)
    {
        $this->db->like($coloumn, $value);
        $query = $this->db->get($table);
        return $query->result();
        // echo $this->db->last_query(); exit;
    }



    public function get_search_projects($table, $city_id ='',$property_type = '',$possession_year = '',$search = '',$projects_id = '',$arrange_by = '',$perpage = null)
    {

        $this->db->group_start();

        if(isset($city_id) && $city_id != ''){
            $this->db->where_in('city_id', $city_id);

        }
        if(isset($property_type) && $property_type != ''){
            $this->db->where('project_type', $property_type);
        }



    // if(isset($from_budget) && $from_budget != '' && isset($to_budget) && $to_budget != ''){
    //         if($from_budget == 0){ $from_budget =1; }
    //         if($to_budget == 0){ $to_budget =1; }
    //             $this->db->where("(unit_starting_price BETWEEN ".$from_budget." AND ". $to_budget.")");

    // }else{
    //      if(isset($from_budget) && $from_budget != ''){
    //             if($from_budget == 0){ $from_budget =1; }
    //             if($to_budget == 0){ $to_budget =1; }
    //             $this->db->where("(unit_starting_price BETWEEN ".$from_budget." AND ". $to_budget." )");
    //         }
    //         if(isset($to_budget) && $to_budget != ''){
    //             if($from_budget == 0){ $from_budget =1; }
    //             if($to_budget == 0){ $to_budget =1; }
    //             $this->db->where("(unit_starting_price BETWEEN ". $from_budget." AND ". $to_budget." )");
    //         }
    // }

       
        if(isset($possession_year) && $possession_year != ''){
            $this->db->where('possession_year', $possession_year);
        }
        // if(isset($location_id) && $location_id != ''){
        //     $this->db->where_in('location_id', $location_id);
        // }
        


        if(isset($projects_id) && $projects_id != ''){
           $this->db->where_in('id', $projects_id);
        }
        $this->db->group_end();
        if(isset($search) && $search != ''){

            $this->db->where("(project_name LIKE '%$search%' OR header_meta_tags LIKE '%$search%' OR footer_meta_tags LIKE '%$search%' OR property_type LIKE '%$search%')");

            // $this->db->like('project_name', $search);
            // $this->db->or_like('header_meta_tags', $search);
            // $this->db->or_like('footer_meta_tags', $search);
            // $this->db->or_like('property_type', $search);
        }

        if($arrange_by == 'price_low'){
            $this->db->order_by('unit_starting_price', 'ASC');
        }elseif ($arrange_by == 'price_high') {
            $this->db->order_by('unit_starting_price', 'DESC');
        }elseif($arrange_by == 'possession_date'){
            $this->db->order_by('possession_year', 'ASC');
        }



        if (isset($_GET['page'])) {
            if ($_GET['page'] != '' || $_GET['page'] > 0) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $this->db->limit($perpage, (($page - 1) * $perpage));
        } else {
            $page = 1;
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }
        
        $this->db->order_by('unit_starting_price', 'DESC');

        $query = $this->db->get($table);

        return $query->result();
        // echo $this->db->last_query(); exit;
    }



    public function get_by_id_asc($table, $id, $colum, $limit)
    {
        $this->db->where($colum, $id);
        $this->db->order_by('id', 'ASC');
        $this->db->limit($limit);
        $query = $this->db->get($table);
        return $query->result();
    } 



    public function get_by_id_asc_order_by($table, $id, $colum, $order_by)
    {
        $this->db->where($colum, $id);
        $this->db->order_by($order_by, 'ASC');
        $query = $this->db->get($table);
        return $query->result();
    } 



    public function get_by_id_desc_order_by_col2($table, $id, $colum,$value,$col2, $order_by)
    {
        $this->db->where($colum, $id);
        $this->db->where($col2, $value);
        $this->db->order_by($order_by, 'DESC');
        $query = $this->db->get($table);
        return $query->result();
    } 



    public function search_suggesions()
    {
        $this->db->where($colum, $id);
        $this->db->where($col2, $value);
        $this->db->order_by($order_by, 'DESC');
        $query = $this->db->get($table);
        return $query->result();
    } 



    public function get_like($table,$col,$value)
    {

        if($table == 'projects_floor_plans' || $table == 'projects_flats_details'){
            $this->db->group_by('projects_id');
        }
        $this->db->like($col, $value);
        $query = $this->db->get($table);
        return $query->result();
    } 





    public function auto_complete_model($city_id,$search,$projects_id)
    {

        $this->db->group_start();
        if(isset($projects_id) && $projects_id != ''){
           $this->db->where_in('id', $projects_id);
        }

        if(isset($search) && $search != ''){

            $this->db->or_where("(project_name LIKE '%$search%' OR header_meta_tags LIKE '%$search%' OR footer_meta_tags LIKE '%$search%' OR property_type LIKE '%$search%')");

            // $this->db->like('project_name', $search);
            // $this->db->or_like('header_meta_tags', $search);
            // $this->db->or_like('footer_meta_tags', $search);
            // $this->db->or_like('property_type', $search);
        }
        $this->db->group_end();

        $this->db->where('city_id', $city_id);
        $this->db->order_by('project_name', 'ASC');
        $this->db->limit('20');
        $query = $this->db->get('projects');
        return $query->result();
        // echo $this->db->last_query(); die();
    } 



    public function get_by_col($table,$value,$col)
    {
        $this->db->where($col, $value);
        $query = $this->db->get($table);
        return $query->result();
    }





    public function get_by_where_in_id_asc_order_by($table, $id, $colum, $order_by)
    {
        $this->db->where_in($colum, $id);
        $this->db->order_by($order_by, 'ASC');
        $query = $this->db->get($table);
        return $query->result();
    } 



    public function get_by_value_between($table, $colum1, $colum2)
    {
        if($colum1 != '' && $colum2 != ''){

            if($colum1 == 0){ $colum1 = 1; }
            if($colum2 == 0){ $colum2 = 1; }

            $this->db->where("(starting_price BETWEEN ". $colum1." AND ". $colum2." )");
        }elseif($colum1 != '' && $colum2 == ''){
            if($colum2 == ''){ $colum2 = 1; }
            // $this->db->where('starting_price >=',$colum1);
            $this->db->where("(starting_price BETWEEN ". $colum1." AND ". $colum2." )");
        }elseif($colum1 == '' && $colum2 != ''){
            if($colum1 == ''){ $colum1 = 1; }
            // $this->db->where('starting_price <=',$colum2);
            $this->db->where("(starting_price BETWEEN ". $colum1." AND ". $colum2." )");
        }
        $query = $this->db->get($table);
        return $query->result();
        // echo $this->db->last_query(); die();
    } 













































    public function get_where_notequalto($table, $property_listings_id)
    {
        $this->db->where('id !=', $property_listings_id);
        $query = $this->db->get($table);
        return $query->result();
    }




    public function getbyid_corporate_support_images($table, $corporate_support_id)

    {

        $this->db->where('corporate_support_id', $corporate_support_id);

        $query = $this->db->get($table);

        return $query->result();

    } 

      

    public function getbyid_corporate_support_pdf($table, $corporate_support_id)

    {

        $this->db->where('corporate_support_id', $corporate_support_id);

        $query = $this->db->get($table);

        return $query->result();

    }   









    public function getbyid_education_services_images($table, $education_services_id)

    {

        $this->db->where('education_services_id', $education_services_id);

        $query = $this->db->get($table);

        return $query->result();

    } 

      

    public function getbyid_education_services_pdf($table, $education_services_id)

    {

        $this->db->where('education_services_id', $education_services_id);

        $query = $this->db->get($table);

        return $query->result();

    }  







    public function getbyid_medical_certified_products_images($table, $medical_certified_products_id)

    {

        $this->db->where('medical_certified_products_id', $medical_certified_products_id);

        $query = $this->db->get($table);

        return $query->result();

    } 

      

    public function getbyid_medical_certified_products_pdf($table, $medical_certified_products_id)

    {

        $this->db->where('medical_certified_products_id', $medical_certified_products_id);

        $query = $this->db->get($table);

        return $query->result();

    }  









    public function getbyid_community_products_images($table, $community_products_id)

    {

        $this->db->where('community_products_id', $community_products_id);

        $query = $this->db->get($table);

        return $query->result();

    } 

      

    public function getbyid_community_products_pdf($table, $community_products_id)
    {
        $this->db->where('community_products_id', $community_products_id);
        $query = $this->db->get($table);
        return $query->result();
    }  

    public function getbyid_import_export_services_images($table, $import_export_services_id)
    {
        $this->db->where('import_export_services_id', $import_export_services_id);
        $query = $this->db->get($table);
        return $query->result();
    } 

      
    public function getbyid_import_export_services_pdf($table, $import_export_services_id)
    {
        $this->db->where('import_export_services_id', $import_export_services_id);
        $query = $this->db->get($table);
        return $query->result();
    }  


    public function getbyid_electrical_automation_images($table, $electrical_automation_id)
    {
        $this->db->where('electrical_automation_id', $electrical_automation_id);
        $query = $this->db->get($table);
        return $query->result();
    } 

      
    public function getbyid_electrical_automation_pdf($table, $electrical_automation_id)
    {
        $this->db->where('electrical_automation_id', $electrical_automation_id);
        $query = $this->db->get($table);
        return $query->result();
    }  


    public function getbyid_smart_home_appliance_images($table, $smart_home_appliance_id)
    {
        $this->db->where('smart_home_appliance_id', $smart_home_appliance_id);
        $query = $this->db->get($table);
        return $query->result();
    } 

      
    public function getbyid_smart_home_appliance_pdf($table, $smart_home_appliance_id)
    {
        $this->db->where('smart_home_appliance_id', $smart_home_appliance_id);
        $query = $this->db->get($table);
        return $query->result();
    }  



    public function getby_cat_id($table, $cat_id, $cat_colm)
    {
        $this->db->where($cat_colm, $cat_id);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($table);
        return $query->result();
    }   


    public function get_byid($table, $product_id, $limit)
    {
        $this->db->where('product_id', $product_id);
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit);
        $query = $this->db->get($table);
        return $query->result();
    }   




// public function get_photos($plink, $perpage = NULL) {



//        $limit = "";

    //        if (isset($_GET['page'])) {

    //            if ($_GET['page'] != '' || $_GET['page'] > 0) {

    //                $page = $_GET['page'];

    //            } else {

    //                $page = 1;

    //            }

    //            $this->db->limit($perpage, (($page - 1) * $perpage));

    //        } else {

    //            $page = 1;

    //            $this->db->limit($perpage, (($page - 1) * $perpage));

    //        }

    //        $this->db->where('gallery_plink', $plink);

    //        return $this->db->get("photos")->result();

    //    }



    public function get_gallery_viewpage($table, $gallery_id, $perpage = null)

    {



        $this->db->where('gallery_id', $gallery_id);



        $limit = "";

        if (isset($_GET['page'])) {

            if ($_GET['page'] != '' || $_GET['page'] > 0) {

                $page = $_GET['page'];

            } else {

                $page = 1;

            }

            $this->db->limit($perpage, (($page - 1) * $perpage));

        } else {

            $page = 1;

            $this->db->limit($perpage, (($page - 1) * $perpage));

        }
        // $this->db->limit($limit,$start);
        $query = $this->db->get($table);

        return $query->result();

    }



  public function get_by_limit($table,$limit)
    {
        $this->db->limit($limit);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get($table);
        return $query->result();
    }




  public function user_login($table,$email,$password)

    {

        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $query = $this->db->get($table);
        return $query->row();

    }


  public function get_by_data($table,$data,$coloum)

    {
        $this->db->where($coloum, $data);
        $query = $this->db->get($table);
        return $query->row();
    }






  public function get_by_data_where_not_id($table,$data,$coloum,$id)

    {
        $this->db->where('id !=', $id);
        $this->db->where($coloum, $data);
        $query = $this->db->get($table);
        return $query->row();
    }




  public function get_by_data_where_id($table,$data,$coloum,$id)

    {
        $this->db->where('id', $id);
        $this->db->where($coloum, $data);
        $query = $this->db->get($table);
        return $query->row();
        // echo $this->db->last_query(); exit();
    }


  public function get_where($table,$property_id,$user_id)

    {
        $this->db->where('property_listings_id', $property_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get($table);
        return $query->row();
        // echo $this->db->last_query(); exit();
    }


  public function get_by_userid($table,$user_id,$perpage = null)

    {
        $limit = "";
        if (isset($_GET['page'])) {
            if ($_GET['page'] != '' || $_GET['page'] > 0) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $this->db->limit($perpage, (($page - 1) * $perpage));
        } else {
            $page = 1;
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }

        $this->db->where('user_id', $user_id);
        $query = $this->db->get($table);
        return $query->result();
        // echo $this->db->last_query(); exit();
    }


  public function get_by_propertyid($table,$property_listings_id)

    {
        $this->db->where('id', $property_listings_id);
        $query = $this->db->get($table);
        return $query->result();
        // echo $this->db->last_query(); exit();
    }


    public function get_student_rent($table, $perpage = null)
    {
        $limit = "";
        if (isset($_GET['page'])) {
            if ($_GET['page'] != '' || $_GET['page'] > 0) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $this->db->limit($perpage, (($page - 1) * $perpage));
        } else {
            $page = 1;
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }


        $sort_by = $this->input->get('sort_by');

        if($sort_by == 'price_high'){
                $this->db->order_by('price', 'DESC');
            }elseif($sort_by == 'price_low'){
                $this->db->order_by('price', 'ASC');    
            }elseif($sort_by == 'most_recent'){
                $this->db->order_by('id', 'DESC');
            }

        $query = $this->db->get($table);
        return $query->result();
        // echo $this->db->last_query(); exit();
    }


    public function get_student_rent_search($table, $perpage = null)
    {
        $limit = "";
        if (isset($_GET['page'])) {
            if ($_GET['page'] != '' || $_GET['page'] > 0) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            $this->db->limit($perpage, (($page - 1) * $perpage));
        } else {
            $page = 1;
            $this->db->limit($perpage, (($page - 1) * $perpage));
        }


        $postcode = $this->input->get('postcode');
        $location = $this->input->get('location');
        $area = $this->input->get('area');
        $property_type = $this->input->get('property_type');
        $bed_rooms = $this->input->get('bed_rooms');
        // $max_beds = $this->input->get('max_beds');
        $minrange = $this->input->get('minrange');
        $maxrange = $this->input->get('maxrange');

        if($postcode != ""){
            $this->db->like('postcode', $postcode);
        }elseif($location != ""){
            $this->db->where('location', $location);
        }elseif($area != ""){
            $this->db->where('area', $area);
        }elseif($property_type != ""){
            $this->db->where('property_type', $property_type);
        }elseif($no_of_beds != ""){
                $this->db->where('bed_rooms', $bed_rooms);
        }elseif($minrange != "" && $maxrange != ""){
            $this->db->where('price >=', $minrange);
            $this->db->where('price <=', $maxrange);
        }

        $query = $this->db->get($table);
        return $query->result();
        // echo $this->db->last_query(); exit();
    }


  public function get_where_property_listings_id($table,$property_listings_id,$user_id)
    {

        $this->db->where('property_listings_id', $property_listings_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get($table);
        return $query->row();
        // echo $this->db->last_query(); exit;
    }
}