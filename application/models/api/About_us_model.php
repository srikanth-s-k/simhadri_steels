<?php

class About_us_model extends CI_Model {

    public function get_data($select, $table, $id) {

        $this->db->select($select);

        $this->db->where('id', $id);

        $query = $this->db->get($table);

        return $query->row();
    }

}
