<?php

class Products_title_model extends CI_Model {

    public function get_result($select, $table, $order_by_column = '', $order_by = '') {

        $this->db->select($select);
        $this->db->order_by($order_by_column, $order_by);
        $query = $this->db->get($table);
        $result = $query->result();

        if (count($result) > 0) {
            //echo $this->db->last_query();die;
            return $result;
        } else {
            return [];
        }
    }

    public function get_products($products_title_id) {
        $result = $this->db->where('products_title_id', $products_title_id)->get('products')->result();
//        echo $this->db->last_query();
//        die();

        if (count($result) > 0) {
//            echo $this->db->last_query();
//            die();
            return $result;
        } else {
            return [];
        }
    }

}
