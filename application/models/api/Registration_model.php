<?php

class Registration_model extends CI_Model {

    function insert($data) {

        $query = $this->db->insert('users', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function check_mobile_exists($mobile) {
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('mobile', $mobile);
        $rows = $this->db->count_all_results();
        if ($rows > 0) {
            return true;
        } else {
            return false;
        }
    }

}
