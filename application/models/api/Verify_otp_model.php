<?php

class Verify_otp_model extends CI_Model {

    function check_otp($otp, $user_id) {
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('otp', $otp);
        $this->db->where('id', $user_id);
        $rows = $this->db->count_all_results();
        if ($rows == 1) {

            return true;
        } else {
            return false;
        }
    }

}
