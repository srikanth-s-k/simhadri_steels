<?php

class Login_model extends CI_Model {

    function insert($data) {

        $query = $this->db->insert('users', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function check_mobile_exists($mobile) {
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('mobile', $mobile);
        $rows = $this->db->count_all_results();
        if ($rows == 1) {
            $user_details = $this->get_user_details($mobile);
            return $user_details;
        } else {
            return false;
        }
    }

    function get_user_details($mobile) {
        $this->db->select('id,password');
        $this->db->from('users');
        $this->db->where('mobile', $mobile);
        $row = $this->db->get()->row();
        return $row;
    }

}
