<?php

class City_wise_dealers_model extends CI_Model {

    public function get_result($select, $table, $coloum1, $coloum1_value, $order_by_column = '', $order_by = '') {
        $this->db->select($select);
        if ($order_by_column != '') {
            $this->db->order_by($order_by_column, $order_by);
        }

        $this->db->where($coloum1, $coloum1_value);
        $query = $this->db->get($table);

        // echo $this->db->last_query(); die();
        if ($query) {
            return $query->result();
        } else {
            return [];
        }
    }

}
