<?php

class Products_details_model extends CI_Model {

    public function get_products_details($select, $table, $coloum1, $coloum1_value, $order_by_column = '', $order_by = '') {
        $this->db->select($select);
        if ($order_by_column != '') {
            $this->db->order_by($order_by_column, $order_by);
        }

        $this->db->where($coloum1, $coloum1_value);
        $query = $this->db->get($table);

        // echo $this->db->last_query(); die();

        return $query->result();
    }

}
