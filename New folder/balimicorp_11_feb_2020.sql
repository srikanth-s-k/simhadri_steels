-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2020 at 01:54 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `balimicorp`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL,
  `heading1` varchar(250) DEFAULT NULL,
  `sub_heading1` varchar(250) DEFAULT NULL,
  `image1` varchar(250) DEFAULT NULL,
  `description1` text,
  `brochure` varchar(250) DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL,
  `heading2` varchar(250) DEFAULT NULL,
  `image2` varchar(250) DEFAULT NULL,
  `description2` text,
  `meta_keywords` text,
  `meta_description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `heading1`, `sub_heading1`, `image1`, `description1`, `brochure`, `link`, `heading2`, `image2`, `description2`, `meta_keywords`, `meta_description`) VALUES
(1, 'WE ARE BCORP', NULL, '20200211160943585_20.png', '<p>&nbsp;Stated in 2014 as a tech and consulting service company based in Singapore (Decimal CS), B?LIMICORP is a new gen integrated digital conglomerate enterprise, with core business model based on internet domain. The company incorporated under INDIA Companies Act, 1956 as B?LIMICORP ENTERPRISE PRIVATE LIMITED.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; It&#39;s business portfolio comprises: Digital Infotainment, Software development, Corporate Cowork spaces, Real Estate, Research &amp; Tech Incubation</p>\r\n\r\n<p><b>Spirit &amp; Soul :</b><br />\r\nDriven by innovation and ambition, we aspire to become a valuable pure play internet companies in the vertical.</p>\r\n\r\n<p>We envision a future where the best ideas flourish and provide great choices for customers to enrich their lives. enabling all companies to build their own digital DNA to create life-changing products, engage their customers and empower employees. We believe the best software will be built by those who are inspired by their work, can choose their work environment, and are rewarded for pure contribution&mdash;in the company of equally passionate colleagues.</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'BUSINESSESS ', NULL, '20200211144429852_m2.png', '<p>B?LIMICORP is involved in five broad verticals of businesses:.</p>\r\n									\r\n										<span class=\"mt-5 ml-2 text-light\"><i class=\"fal fa-map-pin fa-rotate-270 mr-lg-3 mr-0\"></i>Digital Infotainment</span>\r\n										<br>\r\n										<span class=\"ml-2 text-light\"><i class=\"fal fa-map-pin fa-rotate-270 mr-lg-3 mr-0\"></i> Software Consulting & Development</span>\r\n										<br>\r\n										<span class=\"ml-2 text-light\"><i class=\"fal fa-map-pin fa-rotate-270 mr-lg-3 mr-0\"></i> Corporate Coworking Spaces</span>\r\n										<br>\r\n										<span class=\"ml-2 text-light\"><i class=\"fal fa-map-pin fa-rotate-270 mr-lg-3 mr-0\"></i> Infra and Real Estate</span>\r\n										<br>\r\n										<span class=\"ml-2 text-light\"><i class=\"fal fa-map-pin fa-rotate-270 mr-lg-3 mr-0\"></i>Startup Incubation</span><br><br>\r\n\r\n										<p class=\"ml-2 text-light\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp These businesses solely owned and operated by B?LIMICORP Enterprise Private Limited under registered tradenames, all IP rights are reserved. This represents a tremendous scope of opportunities available for the company and to remain one step ahead in the ever changing market conditions the company’s focus on new collaborations and partners in the form of Investor and/or Accelerator. </p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'COLLABORATION', NULL, '20200211144518583_16.png', '<h6>The Power of Collaboration</h6>\r\n\r\n<p>Whether you&#39;re a budding entrepreneur trying to find a partner to work with, a company looking to explore and join our partner ecosystem, or a startup with a revolutinoary idea -you&#39;ve come to the right place!</p>\r\n\r\n<p>...we at B?LIMICORP believe in Collaboration rather than Competition, Your are always welcome to reach us at&nbsp;<a href=\"mailto:%20contact@balimicorp.com\">contact@balimicorp.com</a></p>\r\n', NULL, NULL, NULL, NULL, '<p>We partner with the best teams and companies to deliver what we promise</p>\r\n', NULL, NULL),
(4, ' IDEABOX ', NULL, '20200211150349815_m4.png', '<p>By checking this box I understand and agree that B?LIMICORP may contact me via the above email address to follow up on my submission.</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, ' REACH US ', NULL, '20200211150514512_m5.png', '<p>We&rsquo;re very approachable and would love to speak to you. Feel free to reach us at...</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `home_banners`
--

CREATE TABLE `home_banners` (
  `id` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `heading` varchar(200) DEFAULT NULL,
  `sub_heading` varchar(250) DEFAULT NULL,
  `image_alt` varchar(300) DEFAULT NULL,
  `link` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_banners`
--

INSERT INTO `home_banners` (`id`, `image`, `heading`, `sub_heading`, `image_alt`, `link`) VALUES
(1, '20200211143056760_s8.png', 'Digital Enterprise', NULL, 'Digital Enterprise', NULL),
(2, '20200211143159213_s11.png', 'Software Consulting & Development', NULL, 'Software Consulting & Development', NULL),
(3, '20200211143230976_s10.png', 'Corporate Coworking Spaces', NULL, 'Corporate Coworking Spaces', NULL),
(4, '202002111432543_s4.png', 'Infra and Real Estate', NULL, 'Infra and Real Estate', NULL),
(5, '20200211143331328_s12.png', 'Startup Incubation', NULL, 'Startup Incubation', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ip_conn`
--

CREATE TABLE `ip_conn` (
  `id` int(11) NOT NULL,
  `ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ip_conn`
--

INSERT INTO `ip_conn` (`id`, `ip`) VALUES
(1, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `user` varchar(100) NOT NULL,
  `ipaddr` varchar(100) NOT NULL,
  `login_time` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `user`, `ipaddr`, `login_time`) VALUES
(1, 'admin', '::1', '11-Feb-2020 01:56:24 PM');

-- --------------------------------------------------------

--
-- Table structure for table `log_in_cr`
--

CREATE TABLE `log_in_cr` (
  `id` int(11) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_psw` varchar(100) NOT NULL,
  `email_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_in_cr`
--

INSERT INTO `log_in_cr` (`id`, `admin_name`, `admin_psw`, `email_id`) VALUES
(1, 'admin', '34b329d9495816e93f9ff420d891b218676e8b60', 'kphanikumar.ben@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `manage_uploads`
--

CREATE TABLE `manage_uploads` (
  `id` int(11) NOT NULL,
  `heading` varchar(250) DEFAULT NULL,
  `designation` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `description` text,
  `p_link` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_uploads`
--

INSERT INTO `manage_uploads` (`id`, `heading`, `designation`, `image`, `description`, `p_link`) VALUES
(1, NULL, NULL, '2020021114273668_s4.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `heading` varchar(200) DEFAULT NULL,
  `sub_heading` varchar(250) DEFAULT NULL,
  `image_alt` varchar(300) DEFAULT NULL,
  `link` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `image`, `heading`, `sub_heading`, `image_alt`, `link`) VALUES
(1, '20200211145556531_p1.png', NULL, NULL, 'ROBOXA', 'http://roboxatech.com/'),
(2, '2020021114561826_p2.jpg', NULL, NULL, 'DECIMAL', 'http://decimalcs.com/'),
(3, '20200211150134135_p3.png', NULL, NULL, 'WORK CROP', 'http://thecorpwork.com/'),
(4, '20200211150206948_p4.png', NULL, NULL, 'START UP INDIA', 'https://www.startupindia.gov.in/');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL,
  `site_name` varchar(250) DEFAULT NULL,
  `site_tag_title` varchar(250) DEFAULT NULL,
  `phone_number` varchar(250) DEFAULT NULL,
  `contact_email` varchar(250) DEFAULT NULL,
  `from_email` varchar(250) DEFAULT NULL,
  `frogot_password_email` varchar(250) DEFAULT NULL,
  `site_logo` varchar(250) DEFAULT NULL,
  `site_logo_alt` varchar(250) DEFAULT NULL,
  `site_favicon` varchar(250) DEFAULT NULL,
  `footer_logo` varchar(250) DEFAULT NULL,
  `footer_heading` varchar(250) DEFAULT NULL,
  `footer_description` text,
  `address_1` varchar(250) DEFAULT NULL,
  `phone_1` varchar(250) DEFAULT NULL,
  `email_1` varchar(250) DEFAULT NULL,
  `address_2` varchar(250) DEFAULT NULL,
  `phone_2` varchar(250) DEFAULT NULL,
  `email_2` varchar(250) DEFAULT NULL,
  `website` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `site_name`, `site_tag_title`, `phone_number`, `contact_email`, `from_email`, `frogot_password_email`, `site_logo`, `site_logo_alt`, `site_favicon`, `footer_logo`, `footer_heading`, `footer_description`, `address_1`, `phone_1`, `email_1`, `address_2`, `phone_2`, `email_2`, `website`) VALUES
(1, 'BALIMICROP', 'We are Best in Town With 40 years of Experience.', '0000000000', 'contact@balimicorp.com', 'contact@balimicorp.com', 'contact@balimicorp.com', '20200211140534428_logo.png', 'BALIMICROP', '20200211140534895_favicon.png', '20190316063149614_logo_white.png', 'About aquasoftzone', 'The working principle of the water softener is based on a theory from 1930 which states that an electromagnetic or electric field causes small crystals of calcium carbonate in the water to join together to form larger crystals.', '7 St George\'s Square, Huddersfield', '01484 516385', 'info@aquasoftzone.com', 'Huddersfield', '(+91) 767-485-5486', 'vizag@aquasoftzone.com', 'voguelettings.com');

-- --------------------------------------------------------

--
-- Table structure for table `social_links`
--

CREATE TABLE `social_links` (
  `id` int(11) NOT NULL,
  `facebook` varchar(600) NOT NULL,
  `twitter` varchar(600) DEFAULT NULL,
  `linkedin` varchar(600) DEFAULT NULL,
  `pinterest` varchar(600) DEFAULT NULL,
  `google` varchar(600) DEFAULT NULL,
  `instagram` varchar(600) DEFAULT NULL,
  `youtube` varchar(600) DEFAULT NULL,
  `vimeo` varchar(900) DEFAULT NULL,
  `bribbble` varchar(600) DEFAULT NULL,
  `map` text,
  `meta_title` text,
  `meta_keywords` text,
  `meta_description` text,
  `og_title` text,
  `og_description` text,
  `twitter_title` text,
  `twitter_description` text,
  `twitter_card` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_links`
--

INSERT INTO `social_links` (`id`, `facebook`, `twitter`, `linkedin`, `pinterest`, `google`, `instagram`, `youtube`, `vimeo`, `bribbble`, `map`, `meta_title`, `meta_keywords`, `meta_description`, `og_title`, `og_description`, `twitter_title`, `twitter_description`, `twitter_card`) VALUES
(1, 'https://www.facebook.com/', 'https://twitter.com/login', 'https://www.linkedin.com/', 'https://in.pinterest.com/', 'https://plus.google.com', 'https://www.instagram.com/', 'https://www.youtube.com/?gl=IN', 'https://vimeo.com/', 'https://dribbble.com/', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2364.94935133638!2d-1.7861242846020708!3d53.64787525978429!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487bdc6df7b95555%3A0x3deaf92c0da22c05!2sVogue+Lettings+Ltd!5e0!3m2!1sen!2suk!4v1556973839111!5m2!1sen!2suk', 'BALIMICORP', 'BALIMICORP', 'BALIMICORP', 'BALIMICORP', 'BALIMICORP', 'BALIMICORP', 'BALIMICORP', 'BALIMICORP');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banners`
--
ALTER TABLE `home_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ip_conn`
--
ALTER TABLE `ip_conn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_in_cr`
--
ALTER TABLE `log_in_cr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manage_uploads`
--
ALTER TABLE `manage_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_links`
--
ALTER TABLE `social_links`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `home_banners`
--
ALTER TABLE `home_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ip_conn`
--
ALTER TABLE `ip_conn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `log_in_cr`
--
ALTER TABLE `log_in_cr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `manage_uploads`
--
ALTER TABLE `manage_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `social_links`
--
ALTER TABLE `social_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
